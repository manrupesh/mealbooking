<%@page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>

<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery.dataTables.css">
<style type="text/css">
</style>
<!-- <script
	src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.5/angular.min.js"></script> -->
<script type="text/javascript">
	var app = angular.module('myapp', []);
	app.controller('myappcontroller', function($scope, $http, $compile) {

		$http({
			method : 'POST',
			url : 'getCartCount'
		}).then(function successCallback(response) {
			$scope.size = response.data;
		}, function errorCallback(response) {
			console.log(response.statusText);
		});

		$scope.myVar = 'all';

		$scope.product_id = 0;
		$scope.addtocart = function(id) {
			var bvalue = id;
			$.ajax({
				type : 'POST',
				url : "addtomycart",
				data : {
					product_id : bvalue
				},
				success : function(data) {
					if (data == "userdashboard") {
						window.location.href = "login";
					} else {
						if (data != "Already in your cart!") {
							$http({
								method : 'POST',
								url : 'getCartCount'
							}).then(function successCallback(response) {
								$scope.size = response.data;
							}, function errorCallback(response) {
								console.log(response.statusText);
							});
						}
						alert(data);
					}
				}
			});
		}

	});
</script>
<meta charset="UTF-8">
<title>Lambert - Responsive Pub / Restaurant Template</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="robots" content="index, follow" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<!--=============== css  ===============-->
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/reset.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/plugins.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/style.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/color.css">
<!--=============== favicons ===============-->
<link rel="shortcut icon" href="resources/restorant/images/favicon.ico">
<!-- <script type="text/javascript" src="resources/restorant/js/map.js"></script> -->
</head>
<body ng-app="myapp" ng-controller="myappcontroller">
	<div class="loader">
		<img src="resources/restorant/images/loader.png" alt="">
	</div>
	<!--================= main start ================-->
	<div id="main">
		<!--=============== header ===============-->
		<header>
		<div class="header-inner">
			<div class="container">
				<!--navigation social links-->
				<div class="nav-social">
					<ul>
						<li><a href="" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-instagram"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-tumblr"></i></a></li>
					</ul>
				</div>
				<!--logo-->
				<div class="logo-holder">
					<a href="index.html"> <img
						src="resources/restorant/images/logo.png" class="respimg logo-vis"
						alt=""> <img src="resources/restorant/images/logo2.png"
						class="respimg logo-notvis" alt="">
					</a>
				</div>
				<!--Navigation -->
				<sec:authorize access="hasAuthority('AUTH_USER')">
					<div class="subnav">
						<a href="showcart"><i class="fa fa-shopping-cart"></i>( <span>{{size}}</span>
							)</a>
					</div>
				</sec:authorize>
				<div class="nav-holder">
					<nav>
					<ul>
						<li><a href="userdashboard">Home</a></li>
						<li><a href="viewmenulist">Menu</a></li>
						<li><a href="viewgallery">Gallery</a></li>
						<li><a href="getproductlist">Shop</a></li>
						<li><a href="viewcontact" class="act-link">Contact</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a href="viewmyorders">My Orders</a></li>
						</sec:authorize>
						<li class="hideli"><a href="userRegistration">Sign Up</a></li>
						<li class="hideli"><a href="login">Sign In</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a class="has-arrow waves-effect waves-dark"
								href="<c:url value="/logout"/>" aria-expanded="false"> <i
									class="mdi mdi-login-variant"></i> Logout
							</a></li>
							<script type="text/javascript">
								$(".hideli").hide();
							</script>
						</sec:authorize>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		</header>
		<!--header end-->
		<!--=============== wrapper ===============-->
		<div id="wrapper">
			<div class="content">
				<section class="parallax-section header-section">
				<div class="bg bg-parallax"
					style="background-image: url(resources/restorant/images/bg/32.jpg)"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);"></div>
				<div class="overlay"></div>
				<div class="container">
					<h2>Our contacts</h2>
					<h3>Were to find us</h3>
				</div>
				</section>
				<section>
				<div class="triangle-decor"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="contact-details">
								<h3>Contact info</h3>
								<p>Numerous commentators have also referred to the supposed
									restaurant owner's eccentric habit of touting for custom
									outside his establishment, dressed in aristocratic fashion and
									brandishing a sword</p>
								<p>Numerous commentators have also referred to the supposed
									restaurant owner's eccentric habit of touting for custom
									outside his establishment, dressed in aristocratic fashion and
									brandishing a sword</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="contact-details">
								<h4>Lambert - New York City</h4>
								<ul>
									<li><a href="">27th Brooklyn New York, NY 10065</a></li>
									<li><a href="">+7(111)123456789</a></li>
									<li><a href="">yourmail@domain.com</a></li>
								</ul>
							</div>
							<div class="contact-details">
								<h4>Lambert - Washington</h4>
								<ul>
									<li><a href="">27th Brooklyn New York, NY 10065</a></li>
									<li><a href="">+7(111)123456789</a></li>
									<li><a href="">yourmail@domain.com</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-3">
							<div class="contact-details">
								<h4>Lambert - Florida</h4>
								<ul>
									<li><a href="">27th Brooklyn New York, NY 10065</a></li>
									<li><a href="">+7(111)123456789</a></li>
									<li><a href="">yourmail@domain.com</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
					<!-- <div class="row">
						<div class="col-md-12">
							<div class="section-title">
								<h3>Our location</h3>
							</div>
						</div>
					</div> -->
				</div>
				</section>
				<!-- <section class="no-padding">
				<div class="map-box">
					<div class="map-holder"
						data-top-bottom="transform: translateY(300px);"
						data-bottom-top="transform: translateY(-300px);">
						<div id="map-canvas"></div>
					</div>
				</div>
				</section> -->
				<section>
				<div class="triangle-decor"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">
								<h3>Get in Touch</h3>
								<h4 class="decor-title">Write us</h4>
								<div class="separator color-separator"></div>
							</div>
							<div class="contact-form-holder">
								<div id="contact-form">
									<div id="message2"></div>
									<form:form class="form-horizontal form-material"
										id="reservation-form" name="signIn"
										action="savetablebookdetail" method="post"
										modelAttribute="tablebookdetail"
										onsubmit='return checkLoginForm(this);'>
										<form:input path="username" name="name" type="text"
											class="name" placeholder="Name" />
										<form:input path="emailID" name="email" type="text"
											class="email" placeholder="E-mail" />
										<form:input path="mobno" name="phone" type="text"
											class="phone" placeholder="Phone" />
										<form:textarea path="message" id="comments"
											placeholder="Message"></form:textarea>
										<form:button type="submit" id="submit">Send</form:button>
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
				</section>
			</div>
			<!--=============== footer ===============-->
			<footer>
			<div class="footer-inner">
				<div class="container">
					<div class="row">
						<!--tiwtter-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Our twitter</h4>
								<div class="twitter-holder">
									<div class="twitts">
										<div class="twitter-feed">
											<!-- Twitter Slider -->
											<div id="twitter-feed"></div>
										</div>
									</div>
									<div class="customNavigation">
										<a class="prev-slide transition"><i
											class="fa fa-long-arrow-left"></i></a> <a
											class="twit-link transition"
											href="https://twitter.com/katokli3mmm" target="_blank"><i
											class="fa fa-twitter"></i></a> <a class="next-slide transition"><i
											class="fa fa-long-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<!--footer social links-->
						<div class="col-md-4">
							<div class="footer-social">
								<h3>Find us</h3>
								<ul>
									<li><a href="" target="_blank"><i
											class="fa fa-facebook"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-twitter"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-instagram"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-pinterest"></i></a></li>
									<li><a href="" target="_blank"><i class="fa fa-tumblr"></i></a></li>
								</ul>
							</div>
						</div>
						<!--subscribe form-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Newsletter</h4>
								<div class="subcribe-form">
									<form id="subscribe">
										<input class="enteremail" name="email" id="subscribe-email"
											placeholder="Your email address.." spellcheck="false"
											type="text">
										<button type="submit" id="subscribe-button"
											class="subscribe-button">
											<i class="fa fa-envelope"></i>
										</button>
										<label for="subscribe-email" class="subscribe-message"></label>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
					<!--footer contacts links -->
					<ul class="footer-contacts">
						<li><a href="">+7(111)123456789</a></li>
						<li><a href="">27th Brooklyn New York, NY 10065</a></li>
						<li><a href="">yourmail@domain.com</a></li>
					</ul>
				</div>
			</div>
			<!--to top / privacy policy-->
			<div class="to-top-holder">
				<div class="container">
					<p>
						<span> &#169; Lambert 2015 . </span> All rights reserved.
					</p>
					<div class="to-top">
						<span>Back To Top </span><i class="fa fa-angle-double-up"></i>
					</div>
				</div>
			</div>
			</footer>
			<!--footer end -->
		</div>
		<!-- wrapper end -->
	</div>
	<!-- Main end -->
	<!--=============== google map ===============-->

	<!--=============== scripts  ===============-->
	<script type="text/javascript"
		src="resources/restorant/js/jquery.min.js"></script>
	<script type="text/javascript" src="resources/restorant/js/plugins.js"></script>
	<script type="text/javascript" src="resources/restorant/js/scripts.js"></script>
</body>

<!-- Mirrored from lambert.kwst.net/site/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 01 Feb 2019 10:47:50 GMT -->
</html>