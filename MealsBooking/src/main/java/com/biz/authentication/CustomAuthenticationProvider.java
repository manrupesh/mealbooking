package com.biz.authentication;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.biz.model.Login;
import com.biz.service.WalletService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	@Autowired
	private WalletService walletService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String userName = authentication.getName();
		String password = authentication.getCredentials().toString();

		if (authorizedUser(userName, password)) {
			List<GrantedAuthority> grantedAuths = new ArrayList<>();
			grantedAuths.add(() -> {
				return "AUTH_USER";
			});
			Authentication auth = new UsernamePasswordAuthenticationToken(userName, password, grantedAuths);
			return auth;
		} else {
			if (authorizedUser1(userName, password)) {
				List<GrantedAuthority> grantedAuths = new ArrayList<>();
				grantedAuths.add(() -> {
					return "AUTH_ADMIN";
				});
				Authentication auth = new UsernamePasswordAuthenticationToken(userName, password, grantedAuths);
				return auth;
			}
			else {
				throw new AuthenticationCredentialsNotFoundException("Invalid Credentials!");
			}
		}
	}

	private boolean authorizedUser(String userName, String password) {
		Login login = new Login();
		login.setEmailID(userName);
		login.setPassword(password);
		boolean flag = walletService.checkUser(login);
		return flag;
	}
	private boolean authorizedUser1(String userName, String password) {
		Login login = new Login();
		login.setEmailID(userName);
		login.setPassword(password);
		boolean flag = walletService.checkUser1(login);
		return flag;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}
}