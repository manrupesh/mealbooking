package com.biz.dao;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.biz.Mailling.UniqueKey;
import com.biz.adminentity.Admin;
import com.biz.adminentity.Product;
import com.biz.adminmodel.ProductInfo;
import com.biz.model.User;

@Repository
public class AdminDAOImpl implements AdminDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public int save(Product product) {
		Session session = sessionFactory.getCurrentSession();
		int flag = 0;
		if (product.getProduct_id() > 0) {
			Criteria criteria = session.createCriteria(Product.class);
			criteria.add(Restrictions.eq("product_id", product.getProduct_id()));
			Product users = (Product) criteria.uniqueResult();
			if (users != null) {
				users.setCreateDate(new Date());
				users.setName(product.getName());
				users.setDescription(product.getDescription());
				users.setCategory(product.getCategory());
				users.setProduct_type(product.getProduct_type());
				users.setAvailability(product.getAvailability());
				users.setProduct_size(product.getProduct_size());
				users.setDiscount_per(product.getDiscount_per());
				users.setStartDate(product.getStartDate());
				users.setEndDate(product.getEndDate());
				users.setPrice(product.getPrice());
				if (product.getImage().length > 0) {
					users.setImage(product.getImage());
				}
				session.saveOrUpdate(users);
			}
		} else {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			Criteria criteria = session.createCriteria(Admin.class);
			criteria.add(Restrictions.eq("emailID", name));
			Admin admins = (Admin) criteria.uniqueResult();
			product.setAdmin_id(admins);
			session.save(product);
			flag = 1;
		}

		return flag;
	}

	@Override
	public Admin addAdmin(Admin user) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Admin.class);
		criteria.add(Restrictions.eq("emailID", user.getEmailID()));
		Admin users = (Admin) criteria.uniqueResult();

		Criteria criteria11 = session.createCriteria(Admin.class);
		criteria11.add(Restrictions.eq("phone", user.getPhone()));
		Admin users11 = (Admin) criteria11.uniqueResult();
		Admin userdata = new Admin();
		userdata.setEmailID("setforcheck");// set for reference checking purpose

		if (users11 != null) {
			userdata.setEmailID("already exist");
		} else {
			if (users != null) {
				userdata.setEmailID("already exist");
			} else {

				// reference check if exists
				if (!user.getReferencecode().isEmpty()) {
					Criteria criteria1 = session.createCriteria(Admin.class);
					criteria1.add(Restrictions.eq("referencecode", user.getReferencecode()));
					Admin users1 = (Admin) criteria1.uniqueResult();
					if (users1 != null) {
						user.setReferencecodeuserid(users1.getAdmin_id());
					} else {

						userdata.setEmailID("wrong reference code");
					}

				}

				UniqueKey uniqueKey = new UniqueKey();
				String uuid = uniqueKey.uniquekey();
				user.setActive(false);
				user.setDelete(false);
				user.setUuid(uuid);

				String uniqueId = UUID.randomUUID().toString();

				user.setReferencecode(uniqueId);

				/*
				 * User_Addresses useraddress = new User_Addresses(); useraddress.setUid(user);
				 * 
				 * SendFund sendfund = new SendFund(); sendfund.setUid(user);
				 */

				if (userdata.getEmailID().equals("setforcheck")) {

					/*
					 * session.save(useraddress); session.save(sendfund);
					 */
					session.save(user);
					userdata = user;
				}

			}
		}

		return userdata;
	}

	@Override
	public List<Product> getproduct() {
		Session session = sessionFactory.getCurrentSession();
		return (List<Product>) session.createQuery("from Product").list();
	}

	@Override
	public Product findProduct(int product_id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Product.class);
		crit.add(Restrictions.eq("product_id", product_id));
		return (Product) crit.uniqueResult();
	}

	@Override
	public ProductInfo findProductInfo(int product_id) {
		Product productInfo = this.findProduct(product_id);
		ProductInfo product = new ProductInfo();
		if (productInfo != null) {
			product.setProduct_id(productInfo.getProduct_id());
			product.setName(productInfo.getName());
			product.setDescription(productInfo.getDescription());
			product.setCategory(productInfo.getCategory());
			product.setProduct_type(productInfo.getProduct_type());
			product.setAvailability(productInfo.getAvailability());
			product.setProduct_size(productInfo.getProduct_size());
			product.setDiscount_per(productInfo.getDiscount_per());
			product.setStartDate(productInfo.getStartDate());
			product.setEndDate(productInfo.getEndDate());
			product.setPrice(productInfo.getPrice());
			String base64Encoded = null;
			byte[] bytes = productInfo.getImage();
			byte[] encodeBase64 = Base64.encodeBase64(bytes);
			try {
				base64Encoded = new String(encodeBase64, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			product.setImg(base64Encoded);
		} else if (productInfo == null) {
			return null;
		}
		return product;
	}
}
