package com.biz.Mailling;

import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.Message;
//import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.Message.RecipientType;

public class Mailing {
	
	private String email;
	private String otp;
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public void sendMail() {


		String to = email;// target mail id change accordingly
		final String from = "info@oristocoin.com";// source mail id change accordingly
		final String password = "P@ssw0rd@123";// source mail password change accordingly

		// Get the session object
		Properties properties = System.getProperties();

		properties.setProperty("mail.smtp.host", "smtp.office365.com");// change accordingly
		properties.setProperty("mail.smtp.port", "587");
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.debug", "true");

		Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, password);
			}
		});

		// compose the message
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject("Oristo Wallet Mailing");
			message.setContent(otp, "text/html");

			// Send message
			Transport.send(message);
			System.out.println("message sent successfully....");

		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
		finally {
			System.out.println("pls check internet connection first");
		}
	}
}


