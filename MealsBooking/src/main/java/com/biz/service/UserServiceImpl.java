package com.biz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biz.adminentity.Order_Details;
import com.biz.adminentity.Orders;
import com.biz.adminentity.Product;
import com.biz.adminentity.Product_Reviews;
import com.biz.adminentity.Table_Book_Details;
import com.biz.adminmodel.GetProductCartTotal;
import com.biz.adminmodel.Reviews;
import com.biz.dao.UserDAO;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	public Product findProduct(int product_id) {
		return userDAO.findProduct(product_id);
	}

	@Override
	public String addtocart(int product_id) {
		return userDAO.addtocart(product_id);
	}

	@Override
	public List<Product> getcart() {
		return userDAO.getcart();
	}

	@Override
	public String rmvtocart(int product_id) {
		return userDAO.rmvtocart(product_id);
	}

	@Override
	public String ratting(int product_id, int star) {
		return userDAO.ratting(product_id, star);
	}

	@Override
	public void updateratting(int product_id) {
		userDAO.updateratting(product_id);
	}

	@Override
	public void savereview(Reviews review) {
		userDAO.savereview(review);
	}

	@Override
	public List<Product_Reviews> getreviewslist(int product_id) {
		return userDAO.getreviewslist(product_id);
	}

	@Override
	public String savefavourite(int product_id) {
		return userDAO.savefavourite(product_id);
	}

	@Override
	public String getfovourite(int product_id) {
		return userDAO.getfovourite(product_id);
	}

	@Override
	public String rmvqty(int product_id) {
		return userDAO.rmvqty(product_id);
	}

	@Override
	public String addqty(int product_id) {
		return userDAO.addqty(product_id);
	}

	@Override
	public GetProductCartTotal gettotal() {
		return userDAO.gettotal();
	}

	@Override
	public int savestudentdetails(Table_Book_Details tablebookdetail) {
		return userDAO.savestudentdetails(tablebookdetail);
	}

	@Override
	public boolean addorder(Orders orders) {
		return userDAO.addorder(orders);
	}

	@Override
	public List<Orders> getmyorders() {
		return userDAO.getmyorders();
	}

	@Override
	public List<Order_Details> getmyorderdetails(int order_id) {
		return userDAO.getmyorderdetails(order_id);
	}

}
