<%@page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
<script src="resources/myJS/bootstrap.min.js"></script>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>
<script src="resources/myJS/jquery-ui.js"></script>
<script src="resources/myJS/custom.js"></script>
<script src="resources/myJS/jquery.mark.min.js"></script>
<script src="resources/myJS/jquery.calculation.js"></script>
<script src="resources/myJS/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery.dataTables.css">
<!--=============== basic  ===============-->
<meta charset="UTF-8">
<title>Lambert - Responsive Pub / Restaurant Template</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="robots" content="index, follow" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<!--=============== css  ===============-->
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/reset.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/plugins.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/style.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/color.css">
<!--=============== favicons ===============-->
<link rel="shortcut icon" href="resources/restorant/images/favicon.ico">
<script type="text/javascript">
	var app = angular.module('myapp', []);
	app.controller('myappcontroller', function($scope, $http, $compile) {
		
		$http({
			method : 'POST',
			url : 'getCartCount'
		}).then(function successCallback(response) {
			$scope.size = response.data;
		}, function errorCallback(response) {
			console.log(response.statusText);
		});

	});
</script>
<style>
mark {
	padding: 0px;
	background-color: #f1c40f;
}

.quantity {
	float: left;
	padding: 20px 0;
	width: 100%;
}
</style>
</head>
<body ng-app="myapp" ng-controller="myappcontroller">
	<div class="loader">
		<img src="resources/restorant/images/loader.png" alt="">
	</div>
	<!--================= main start ================-->
	<div id="main">
		<!--=============== header ===============-->
		<header>
		<div class="header-inner">
			<div class="container">
				<!--navigation social links-->
				<div class="nav-social">
					<ul>
						<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" target="_blank"><i
								class="fa fa-instagram"></i></a></li>
						<li><a href="#" target="_blank"><i
								class="fa fa-pinterest"></i></a></li>
						<li><a href="#" target="_blank"><i class="fa fa-tumblr"></i></a></li>
					</ul>
				</div>
				<!--logo-->
				<div class="logo-holder">
					<a href="index.html"> <img
						src="resources/restorant/images/logo.png" class="respimg logo-vis"
						alt=""> <img src="resources/restorant/images/logo2.png"
						class="respimg logo-notvis" alt="">
					</a>
				</div>
				<!--Navigation -->
				<div class="subnav">
					<a href="showcart"><i class="fa fa-shopping-cart"></i>( <span>{{size}}</span>
						)</a>
				</div>
				<div class="nav-holder">
					<nav>
					<ul>
						<li><a href="userdashboard">Home</a></li>
						<li><a href="viewmenulist">Menu</a></li>
						<li><a href="viewgallery">Gallery</a></li>
						<!-- <li><a href="viewreservation">Reservation</a></li> -->
						<li><a href="getproductlist">Shop</a></li>
						<li><a href="viewcontact">Contact</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a href="viewmyorders">My Orders</a></li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a class="has-arrow waves-effect waves-dark"
								href="<c:url value="/logout"/>" aria-expanded="false"> <i
									class="mdi mdi-login-variant"></i> Logout
							</a></li>
						</sec:authorize>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		</header>
		<!--header end-->
		<!--=============== wrapper ===============-->
		<div id="wrapper">
			<div class="content">
				<section class="parallax-section header-section">
				<div class="bg bg-parallax"
					style="background-image: url(resources/restorant/images/bg/21.jpg)"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);"></div>
				<div class="overlay"></div>
				<div class="container">
					<h2>Your Orders</h2>
					<h3>Order online is easy</h3>
				</div>
				</section>
				<section>
				<div class="triangle-decor"></div>
				<div class="container">
					<!-- CHECKOUT TABLE -->
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-border checkout-table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Email-ID</th>
										<th>Phone No</th>
										<th>Address</th>
										<th>Order Date</th>
										<th>Status</th>
										<th>Total Product Amount</th>
										<th>Tax CGST</th>
										<th>Tax SGST</th>
										<th>Total Discount</th>
										<th>Final Paid Amount</th>
										<th>View My Order Details</th>
									</tr>
								</thead>
								<c:forEach var="orders" items="${orders}">
									<tbody>
										<tr>
											<%-- <td class="hidden-xs"><img
												src="data:image/png;base64,${prodInfo.img}" alt=""
												class="respimg"></td>--%>
											<td>
												<h5 class="product-name">${orders.customerName}</h5>
											</td>
											<td>
												<h5 class="product-name">${orders.customerEmail}</h5>
											</td>
											<td>
												<h5 class="product-name">${orders.customerPhone}</h5>
											</td>
											<td>
												<h5 class="product-name">${orders.customerAddress}</h5>
											</td>
											<td>
												<h5 class="product-name">${orders.orderDate}</h5>
											</td>
											<td>
												<h5 class="product-name">${orders.delevery_status}</h5>
											</td>
											<td>
												<h5 class="product-name">${orders.total_product_amount}</h5>
											</td>
											<td>
												<h5 class="product-name">${orders.tax_cgst}</h5>
											</td>
											<td>
												<h5 class="product-name">${orders.tax_sgst}</h5>
											</td>
											<td>
												<h5 class="product-name">${orders.total_discount_amount}</h5>
											</td>
											<td>
												<h5 class="product-name">${orders.final_paid_amount}</h5>
											</td>
											<td>
												<h5 class="product-name"><a href="viewmyorderdetails?order_id=${orders.order_id}">View</a></h5>
											</td>
										</tr>
									</tbody>
								</c:forEach>
							</table>
						</div>
					</div>
				</div>
				</section>
			</div>
			<!--=============== footer ===============-->
			<footer>
			<div class="footer-inner">
				<div class="container">
					<div class="row">
						<!--tiwtter-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Our twitter</h4>
								<div class="twitter-holder">
									<div class="twitts">
										<div class="twitter-feed">
											<!-- Twitter Slider -->
											<div id="twitter-feed"></div>
										</div>
									</div>
									<div class="customNavigation">
										<a class="prev-slide transition"><i
											class="fa fa-long-arrow-left"></i></a> <a
											class="twit-link transition"
											href="https://twitter.com/katokli3mmm" target="_blank"><i
											class="fa fa-twitter"></i></a> <a class="next-slide transition"><i
											class="fa fa-long-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<!--footer social links-->
						<div class="col-md-4">
							<div class="footer-social">
								<h3>Find us</h3>
								<ul>
									<li><a href="#" target="_blank"><i
											class="fa fa-facebook"></i></a></li>
									<li><a href="#" target="_blank"><i
											class="fa fa-twitter"></i></a></li>
									<li><a href="#" target="_blank"><i
											class="fa fa-instagram"></i></a></li>
									<li><a href="#" target="_blank"><i
											class="fa fa-pinterest"></i></a></li>
									<li><a href="#" target="_blank"><i
											class="fa fa-tumblr"></i></a></li>
								</ul>
							</div>
						</div>
						<!--subscribe form-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Newsletter</h4>
								<div class="subcribe-form">
									<form id="subscribe">
										<input class="enteremail" name="email" id="subscribe-email"
											placeholder="Your email address.." spellcheck="false"
											type="text">
										<button type="submit" id="subscribe-button"
											class="subscribe-button">
											<i class="fa fa-envelope"></i>
										</button>
										<label for="subscribe-email" class="subscribe-message"></label>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
					<!--footer contacts links -->
					<ul class="footer-contacts">
						<li><a href="#">+7(111)123456789</a></li>
						<li><a href="#">27th Brooklyn New York, NY 10065</a></li>
						<li><a href="#">yourmail@domain.com</a></li>
					</ul>
				</div>
			</div>
			<!--to top / privacy policy-->
			<div class="to-top-holder">
				<div class="container">
					<p>
						<span> &#169; Lambert 2015 . </span> All rights reserved.
					</p>
					<div class="to-top">
						<span>Back To Top </span><i class="fa fa-angle-double-up"></i>
					</div>
				</div>
			</div>
			</footer>
			<!--footer end -->
		</div>
		<!-- wrapper end -->
	</div>
	<!-- Main end -->
	<!--=============== google map ===============-->
	<script type="text/javascript"
		src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<!--=============== scripts  ===============-->
	<script type="text/javascript"
		src="resources/restorant/js/jquery.min.js"></script>
	<script type="text/javascript" src="resources/restorant/js/plugins.js"></script>
	<script type="text/javascript" src="resources/restorant/js/scripts.js"></script>
</body>

<!-- Mirrored from lambert.kwst.net/site/cart.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 01 Feb 2019 10:49:25 GMT -->
</html>