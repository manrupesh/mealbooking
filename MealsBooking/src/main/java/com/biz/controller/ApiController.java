package com.biz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.biz.model.Student;
import com.biz.service.ApiService;

@RestController
@RequestMapping("/api")
public class ApiController {
	
	@Autowired
	private ApiService apiService;
	
	@RequestMapping(value="/test",method=RequestMethod.GET)
	public String checktest(){
		return "Rupesh";
		
	}
	@RequestMapping(value="/postsave",method=RequestMethod.POST)
	public String postsave(@RequestBody Student student) {
		return apiService.postsave(student);
	}
}
