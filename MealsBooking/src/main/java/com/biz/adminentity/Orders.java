package com.biz.adminentity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biz.model.User;

@Entity
@Table(name = "orders_list")
public class Orders {

	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name = "order_id")
	private int order_id;
	private String customerName;
	private String customerAddress;
	private String customerEmail;
	private String customerPhone;
	private double total_product_amount;
	private double total_discount_amount;
	private double tax_cgst;
	private double tax_sgst;
	private double final_paid_amount;
	private Date orderDate;
	private double product_price;
	private int quantity;
	private String delevery_status;
	private String order_status;
	private Character carry;
	private Character deleted;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "order_id")
	Set<Order_Details> order_details;

	@ManyToOne
	@JoinColumn(name = "uid")
	User uid = new User();

	public int getOrder_id() {
		return order_id;
	}

	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public double getTotal_product_amount() {
		return total_product_amount;
	}

	public void setTotal_product_amount(double total_product_amount) {
		this.total_product_amount = total_product_amount;
	}

	public double getTotal_discount_amount() {
		return total_discount_amount;
	}

	public void setTotal_discount_amount(double total_discount_amount) {
		this.total_discount_amount = total_discount_amount;
	}

	public double getTax_cgst() {
		return tax_cgst;
	}

	public void setTax_cgst(double tax_cgst) {
		this.tax_cgst = tax_cgst;
	}

	public double getTax_sgst() {
		return tax_sgst;
	}

	public void setTax_sgst(double tax_sgst) {
		this.tax_sgst = tax_sgst;
	}

	public double getFinal_paid_amount() {
		return final_paid_amount;
	}

	public void setFinal_paid_amount(double final_paid_amount) {
		this.final_paid_amount = final_paid_amount;
	}

	public double getProduct_price() {
		return product_price;
	}

	public void setProduct_price(double product_price) {
		this.product_price = product_price;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDelevery_status() {
		return delevery_status;
	}

	public void setDelevery_status(String delevery_status) {
		this.delevery_status = delevery_status;
	}

	public Character getCarry() {
		return carry;
	}

	public void setCarry(Character carry) {
		this.carry = carry;
	}

	public Character getDeleted() {
		return deleted;
	}

	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}

	public Set<Order_Details> getOrder_details() {
		return order_details;
	}

	public void setOrder_details(Set<Order_Details> order_details) {
		this.order_details = order_details;
	}

	public User getUid() {
		return uid;
	}

	public void setUid(User uid) {
		this.uid = uid;
	}

	public String getOrder_status() {
		return order_status;
	}

	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}
}
