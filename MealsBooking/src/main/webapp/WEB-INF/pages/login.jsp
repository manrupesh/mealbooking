<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16"
	href="resources/restorant/images/favicon.ico">
<title></title>
<!-- Bootstrap Core CSS -->
<link
	href="resources/login/assets/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="resources/login/css/style.css" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="resources/login/css/colors/blue.css" id="theme"
	rel="stylesheet">
<!-- Tell the browser to be responsive to screen width -->
<script src="resources/myJS/bootstrap.min.js"></script>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>
<script src="resources/myJS/jquery-ui.js"></script>
<script src="resources/myJS/custom.js"></script>
<script src="resources/myJS/jquery.mark.min.js"></script>
<script src="resources/myJS/jquery.calculation.js"></script>
<script src="resources/myJS/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	var sheight = window.screen.availHeight - 183;
	var swidth = window.screen.availWidth - 800;
	$(document).ready(function() {

	});
	var app = angular.module('myApp', []);
	app.controller('myApp', function($scope, $http, $compile) {
		$scope.submitted = false;
		$scope.checkVal = function() {
			$scope.submitted = true;
			if ($scope.signForm.$valid) {

				$("#loginform").submit();
			}
		}
	});
</script>
</head>

<body>
	<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
		<font color="red"> Your login attempt was not successful due to
			<c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />.
		</font>
	</c:if>
	<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50"> <circle
			class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2"
			stroke-miterlimit="10" /> </svg>
	</div>

	<section id="wrapper">
	<div class="login-register"
		style="background-image: url(resources/login/assets/images/background/login.jpg);">
		<div class="offset-md-7 col-md-5">
			<div class="login-box">
				<img src="resources/login/assets/images/logo-icon.png"
					alt="homepage" class="dark-logo logoloin" />
			</div>
		</div>
		<div class="offset-md-7 col-md-4 card1">
			<div class="card-body">
				<form class="form-horizontal form-material" id="loginform"
					ng-model="loginform" action="authenticateUser" method="post">
					<h3 class="box-title m-b-20">Sign In</h3>
					<div class="form-group ">
						<div class="col-xs-12">
							<input id="email" class="form-control" type="text"
								name="username" required="" placeholder="Username" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input id="password-field" type="password" name="password"
								class="form-control" required="" placeholder="password">
							<span toggle="#password-field"
								class="fa fa-fw fa-eye field-icon toggle-password"></span> <input
								type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						</div>
						<div class="form-group row">
							<div class="col-md-12 font-14">
								<div class="checkbox checkbox-primary pull-left p-t-0">
									<input id="checkbox-signup" type="checkbox">
									<!--  <label for="checkbox-signup"> Remember me </label> -->
								</div>
								<a href="forgotpass" id="to-recover"
									class="text-light pull-right forget"><i
									class="fa fa-lock m-r-5"></i> Forgot pwd?</a>
							</div>
						</div>
						<div class="form-group text-center m-t-20">
							<div class="col-xs-12">
								<button
									class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
									type="submit" ng-click="checkVal()">Log In</button>
							</div>
						</div>

						<div class="form-group m-b-0">
							<div class="col-sm-12 text-center">
								<div>
									Don't have an account? <a href="userRegistration"
										class="text-info m-l-5"><b>Sign Up</b></a> <a
										href="adminRegistration" class="text-info m-l-5"><b>admin
											Sign Up</b></a>
								</div>
							</div>
						</div>
						<div class="form-group m-b-0">
							<div class="col-sm-12 text-center">
								<div>
									<a href="userdashboard" class="text-info m-l-5"><b>Home</b></a>
								</div>
							</div>
						</div>
				</form>

			</div>
		</div>
	</div>
	</section>

	<script src="resources/login/assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap tether Core JavaScript -->
	<script src="resources/login/assets/plugins/bootstrap/js/popper.min.js"></script>
	<script
		src="resources/login/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- slimscrollbar scrollbar JavaScript -->
	<script src="resources/login/js/jquery.slimscroll.js"></script>
	<!--Wave Effects -->
	<script src="resources/login/js/waves.js"></script>
	<!--Menu sidebar -->
	<script src="resources/login/js/sidebarmenu.js"></script>
	<!--stickey kit -->
	<script
		src="resources/login/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
	<script
		src="resources/login/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!--Custom JavaScript -->
	<script src="resources/login/js/custom.min.js"></script>
	<script
		src="resources/login/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
	<script>
		$(".toggle-password").click(function() {

			$(this).toggleClass("fa-eye fa-eye-slash");
			var input = $($(this).attr("toggle"));
			if (input.attr("type") == "password") {
				input.attr("type", "text");
			} else {
				input.attr("type", "password");
			}
		});
	</script>
</body>

</html>





<%-- 
</head>

<body>




	<img alt="" src="resources/theme1/assets/images/emailler/Logo.png">

	<form class="form-horizontal form-material" id="loginform"
		ng-model="loginform" action="authenticateUser" method="post">
		<h3 class="box-title m-b-20">Sign In</h3>
		<div class="form-group ">
			<div class="col-xs-12">
				<input id="email" class="form-control" type="text" name="username"
					required="" placeholder="Username" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<input id="password-field" type="password" name="password"
					class="form-control" required="" placeholder="password"> <span
					toggle="#password-field"
					class="fa fa-fw fa-eye field-icon toggle-password"></span> <input
					type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			</div>
			<div class="form-group row">
				<div class="col-md-12 font-14">
					<div class="checkbox checkbox-primary pull-left p-t-0">
						<input id="checkbox-signup" type="checkbox">
						<!--  <label for="checkbox-signup"> Remember me </label> -->
					</div>
					<a href="forgotpass" id="to-recover" class="text-dark pull-right">
						<!-- <i class="fa fa-lock m-r-5"></i> --> Forgot pwd?
					</a>
				</div>
			</div>
			<div class="form-group text-center m-t-20">
				<div class="col-xs-12">
					<button
						class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
						type="submit" ng-click="checkVal()">Log In</button>
				</div>
			</div>

			<div class="form-group m-b-0">
				<div class="col-sm-12 text-center">
					<div>
						Don't have an account? <a href="userRegistration"
							class="text-info m-l-5"><b>Sign Up</b></a> <a
							href="adminRegistration" class="text-info m-l-5"><b>admin
								Sign Up</b></a>
					</div>
				</div>
			</div>
			<div class="form-group m-b-0">
				<div class="col-sm-12 text-center">
					<div>
						<a href="https://oristocoin.com/" class="text-info m-l-5"><b>Home</b></a>
					</div>
				</div>
			</div>
	</form>


	<script>
		$(".toggle-password").click(function() {

			$(this).toggleClass("fa-eye fa-eye-slash");
			var input = $($(this).attr("toggle"));
			if (input.attr("type") == "password") {
				input.attr("type", "text");
			} else {
				input.attr("type", "password");
			}
		});
	</script>
</body>

</html> --%>