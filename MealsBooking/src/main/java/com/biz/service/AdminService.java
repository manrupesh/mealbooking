package com.biz.service;

import java.util.ArrayList;
import java.util.List;

import com.biz.adminentity.Admin;
import com.biz.adminentity.Product;
import com.biz.adminmodel.ProductInfo;

public interface AdminService {

	public int save(Product product);

	public String addadmin(Admin admin, String path);

	public List<Product> getproduct();

	public ProductInfo findProductInfo(int product_id);

}
