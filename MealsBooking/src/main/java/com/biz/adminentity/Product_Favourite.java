package com.biz.adminentity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Product_favourite_list")
public class Product_Favourite {
	
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name = "favourite_id")
	private int favourite_id;
	private int uid;
	private Date favourite_date;
	private Character status;
	private Character carry;
	private Character deleted;
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	Product product_id = new Product();

	public int getFavourite_id() {
		return favourite_id;
	}

	public void setFavourite_id(int favourite_id) {
		this.favourite_id = favourite_id;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public Date getFavourite_date() {
		return favourite_date;
	}

	public void setFavourite_date(Date favourite_date) {
		this.favourite_date = favourite_date;
	}

	public Product getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Product product_id) {
		this.product_id = product_id;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public Character getCarry() {
		return carry;
	}

	public void setCarry(Character carry) {
		this.carry = carry;
	}

	public Character getDeleted() {
		return deleted;
	}

	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}
	
}
