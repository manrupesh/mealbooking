package com.biz.controller;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.apache.commons.codec.binary.Base64;
import com.biz.adminentity.Product;
import com.biz.adminmodel.ProductInfo;
import com.biz.service.AdminService;
import com.biz.service.WalletService;

@Controller
@RequestMapping("/")
public class AdminController {

	@Autowired
	private AdminService adminService;
	String message = "";
	@Autowired
	private WalletService walletService;

	@RequestMapping(value = "/admindashboard", method = RequestMethod.GET)
	public ModelAndView opendashbord(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		System.out.println("hi hi    ");
		try {
			if (checkto == "done") {
				List<Product> product = adminService.getproduct();
				for (Product product2 : product) {
					String base64Encoded = null;
					byte[] bytes = product2.getImage();
					byte[] encodeBase64 = Base64.encodeBase64(bytes);
					try {
						base64Encoded = new String(encodeBase64, "UTF-8");
						System.out.println(base64Encoded);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					product2.setImg(base64Encoded);
				}
				model.addObject("product", product);
				model.setViewName("admindashboard");

			} else {
				model.setViewName("login");
			}
		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/viewuploadproduct", method = RequestMethod.GET)
	public ModelAndView viewuploadproduct(ModelAndView model, ModelMap model1,
			@RequestParam(value = "product_id", defaultValue = "") int product_id, HttpSession session1) {
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {
			if (checkto == "done") {
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				String name = auth.getName();
				ProductInfo product = null;
				if (product_id > 0) {
					product = adminService.findProductInfo(product_id);
				}
				if (product == null) {
					product = new ProductInfo();
					product.setNewProduct(true);
				}

				model.addObject("fileBucket", product);
				model.setViewName("viewuploadproduct");
			} else {
				model.setViewName("login");
			}
		} catch (Exception e) {
			model.setViewName("login");
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/admin/saveproduct", method = RequestMethod.POST)
	public ModelAndView saveproduct(@ModelAttribute ProductInfo productInfo, HttpSession session1) {

		try {
			Product product = new Product();
			if (productInfo.getProduct_id() > 0) {
				product.setProduct_id(productInfo.getProduct_id());
			}
			product.setCreateDate(new Date());
			product.setName(productInfo.getName());
			product.setDescription(productInfo.getDescription());
			product.setCategory(productInfo.getCategory());
			product.setProduct_type(productInfo.getProduct_type());
			product.setAvailability(productInfo.getAvailability());
			product.setProduct_size(productInfo.getProduct_size());
			product.setDiscount_per(productInfo.getDiscount_per());
			product.setStartDate(productInfo.getStartDate());
			product.setEndDate(productInfo.getEndDate());
			product.setPrice(productInfo.getPrice());
			MultipartFile file = productInfo.getFile();
			// MultipartFile file = product.getFile();
			byte[] bytes = file.getBytes();
			product.setImage(bytes);
			/*
			 * if(bytes.length>0) {
			 * 
			 * }
			 */
			int flag = adminService.save(product);
			return new ModelAndView("redirect:/admindashboard");

		} catch (Exception e) {
			return new ModelAndView("redirect:/admindashboard");
		}
	}

}
