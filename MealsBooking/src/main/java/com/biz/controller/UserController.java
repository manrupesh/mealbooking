package com.biz.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.biz.adminentity.Order_Details;
import com.biz.adminentity.Orders;
import com.biz.adminentity.Product;
import com.biz.adminentity.Product_Reviews;
import com.biz.adminentity.Table_Book_Details;
import com.biz.adminmodel.GetProductCartTotal;
import com.biz.adminmodel.Reviews;
import com.biz.model.User;
import com.biz.service.AdminService;
import com.biz.service.UserService;

@Controller
@RequestMapping("/")
public class UserController {

	@Autowired
	private UserService userService;
	String message = "";
	@Autowired
	private AdminService adminService;

	@RequestMapping(value = "/userdashboard", method = RequestMethod.GET)
	public ModelAndView userdashboard(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {
			List<Product> product1 = adminService.getproduct();
			for (Product product2 : product1) {
				userService.updateratting(product2.getProduct_id());
			}
			List<Product> product = adminService.getproduct();
			for (Product product2 : product) {
				String base64Encoded = null;
				byte[] bytes = product2.getImage();
				byte[] encodeBase64 = Base64.encodeBase64(bytes);
				try {
					base64Encoded = new String(encodeBase64, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				product2.setImg(base64Encoded);
				String favourit = null;
				if (checkto == "done") {
					favourit = userService.getfovourite(product2.getProduct_id());
					product2.setFavourit(favourit);
					/*
					 * List<Product> cartsize = userService.getcart(); model.addObject("cartsize",
					 * cartsize.size());
					 */
				}
			}
			model.addObject("product", product);
			model.setViewName("userdashboard");

		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/getproductlist", method = RequestMethod.GET)
	public ModelAndView getproductlist(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {

			List<Product> product1 = adminService.getproduct();
			for (Product product2 : product1) {
				userService.updateratting(product2.getProduct_id());
			}
			List<Product> product = adminService.getproduct();
			for (Product product2 : product) {
				String base64Encoded = null;
				byte[] bytes = product2.getImage();
				byte[] encodeBase64 = Base64.encodeBase64(bytes);
				try {
					base64Encoded = new String(encodeBase64, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				product2.setImg(base64Encoded);
				if (checkto == "done") {
					String favourit = userService.getfovourite(product2.getProduct_id());
					product2.setFavourit(favourit);
				}

			}
			model.addObject("product", product);
			model.setViewName("productList");

		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/review", method = RequestMethod.GET)
	public @ResponseBody ModelAndView review(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {
			String id = request.getParameter("product_id");
			int product_id = Integer.parseInt(id);
			Product product = userService.findProduct(product_id);
			userService.updateratting(product.getProduct_id());
			String base64Encoded = null;
			byte[] bytes = product.getImage();
			byte[] encodeBase64 = Base64.encodeBase64(bytes);
			try {
				base64Encoded = new String(encodeBase64, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			product.setImg(base64Encoded);
			if (checkto == "done") {
				String favourit = userService.getfovourite(product_id);
				product.setFavourit(favourit);
			}
			Reviews review = new Reviews();
			List<Product_Reviews> reviewslist = userService.getreviewslist(product_id);
			model.addObject("reviewslist", reviewslist);
			model.addObject("reviews", review);
			model.addObject("prodInfo", product);
			model.setViewName("product_review");

		} catch (Exception e) {
			model.setViewName("login");
			e.printStackTrace();
		}
		return model;
	}

	@RequestMapping(value = "/viewmenulist", method = RequestMethod.GET)
	public ModelAndView viewmenulist(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {

			List<Product> product1 = adminService.getproduct();
			for (Product product2 : product1) {
				userService.updateratting(product2.getProduct_id());
			}
			List<Product> product = adminService.getproduct();
			for (Product product2 : product) {

				String base64Encoded = null;
				byte[] bytes = product2.getImage();
				byte[] encodeBase64 = Base64.encodeBase64(bytes);
				try {
					base64Encoded = new String(encodeBase64, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				product2.setImg(base64Encoded);
				if (checkto == "done") {
					String favourit = userService.getfovourite(product2.getProduct_id());
					product2.setFavourit(favourit);
				}
			}
			model.addObject("product", product);
			model.setViewName("viewmenulist");

		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/viewgallery", method = RequestMethod.GET)
	public ModelAndView viewgallery(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {

			List<Product> product1 = adminService.getproduct();
			for (Product product2 : product1) {
				userService.updateratting(product2.getProduct_id());
			}
			List<Product> product = adminService.getproduct();
			for (Product product2 : product) {

				String base64Encoded = null;
				byte[] bytes = product2.getImage();
				byte[] encodeBase64 = Base64.encodeBase64(bytes);
				try {
					base64Encoded = new String(encodeBase64, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				product2.setImg(base64Encoded);
				if (checkto == "done") {
					List<Product> cartsize = userService.getcart();
					model.addObject("cartsize", cartsize.size());
					String favourit = userService.getfovourite(product2.getProduct_id());
					product2.setFavourit(favourit);
				}
			}
			model.addObject("product", product);
			model.setViewName("viewgallery");

		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/viewreservation", method = RequestMethod.GET)
	public ModelAndView viewreservation(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {
			if (checkto == "done") {
				List<Product> cartsize = userService.getcart();
				model.addObject("cartsize", cartsize.size());
			}
			Table_Book_Details tablebookdetail = new Table_Book_Details();
			model.addObject("tablebookdetail", tablebookdetail);
			model.setViewName("viewreservation");
		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		return model;
	}

	@RequestMapping(value = "/viewcontact", method = RequestMethod.GET)
	public ModelAndView viewcontact(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {
			if (checkto == "done") {
				List<Product> cartsize = userService.getcart();
				model.addObject("cartsize", cartsize.size());
			}
			Table_Book_Details tablebookdetail = new Table_Book_Details();
			model.addObject("tablebookdetail", tablebookdetail);
			model.setViewName("viewcontact");
		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		return model;
	}

	@RequestMapping(value = "/showcart", method = RequestMethod.GET)
	public ModelAndView showcart(ModelAndView model, ModelMap model1, HttpSession session1, HttpServletRequest request,
			HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {
			if (checkto == "done") {
				List<Product> product = userService.getcart();
				for (Product product2 : product) {
					userService.updateratting(product2.getProduct_id());
				}
				for (Product product2 : product) {
					String favourit = userService.getfovourite(product2.getProduct_id());
					String base64Encoded = null;
					byte[] bytes = product2.getImage();
					byte[] encodeBase64 = Base64.encodeBase64(bytes);
					try {
						base64Encoded = new String(encodeBase64, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					product2.setImg(base64Encoded);
					product2.setFavourit(favourit);
				}
				model.addObject("product", product);
				model.setViewName("shoppingCart");

			} else {
				model.setViewName("login");
			}
		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/proceedtocheckout", method = RequestMethod.GET)
	public ModelAndView proceedtocheckout(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {
			if (checkto == "done") {
				Orders orders = new Orders();
				model.addObject("orders", orders);
				model.setViewName("viewproceedtocheckout");

			} else {
				model.setViewName("login");
			}
		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/viewmyorders", method = RequestMethod.GET)
	public ModelAndView viewmyorders(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {
			if (checkto == "done") {
				List<Orders> orders = userService.getmyorders();
				model.addObject("orders", orders);
				model.setViewName("viewmyorders");

			} else {
				model.setViewName("login");
			}
		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/viewmyorderdetails", method = RequestMethod.GET)
	public @ResponseBody ModelAndView viewmyorderdetails(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {
			String id = request.getParameter("order_id");
			int product_id = Integer.parseInt(id);
			List<Order_Details> order_details = userService.getmyorderdetails(product_id);
			for (Order_Details order_Details2 : order_details) {
				String base64Encoded = null;
				byte[] bytes = order_Details2.getProduct_id().getImage();
				byte[] encodeBase64 = Base64.encodeBase64(bytes);
				try {
					base64Encoded = new String(encodeBase64, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				order_Details2.getProduct_id().setImg(base64Encoded);
			}
			model.addObject("order_details", order_details);
			model.setViewName("viewmyorderdetails");

		} catch (Exception e) {
			model.setViewName("login");
			e.printStackTrace();
		}
		return model;
	}

	@RequestMapping(value = "/getCartProduct", method = RequestMethod.POST)
	public @ResponseBody ArrayList getCartProduct(HttpServletRequest request, HttpServletResponse response,
			HttpSession session1) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		List<Product> product = null;
		try {
			if (checkto == "done") {
				product = userService.getcart();
				for (Product product2 : product) {
					userService.updateratting(product2.getProduct_id());
				}
				for (Product product2 : product) {
					String favourit = userService.getfovourite(product2.getProduct_id());
					String base64Encoded = null;
					byte[] bytes = product2.getImage();
					byte[] encodeBase64 = Base64.encodeBase64(bytes);
					try {
						base64Encoded = new String(encodeBase64, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					product2.setImg(base64Encoded);
					product2.setFavourit(favourit);
				}

			}
		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			e.printStackTrace();
		}
		message = "";
		return (ArrayList) product;
	}

	@RequestMapping(value = "/getCartTotal", method = RequestMethod.POST)
	public @ResponseBody GetProductCartTotal getCartTotal(HttpServletRequest request, HttpServletResponse response,
			HttpSession session1) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		GetProductCartTotal gettotal = new GetProductCartTotal();
		try {
			if (checkto == "done") {
				gettotal = userService.gettotal();
			}
		} catch (Exception e) {
			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			e.printStackTrace();
		}
		message = "";
		return gettotal;
	}

	@RequestMapping(value = "/getCartCount", method = RequestMethod.POST)
	public @ResponseBody int getCartCount(HttpServletRequest request, HttpServletResponse response,
			HttpSession session1) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		int countsize = 0;
		try {
			if (checkto == "done") {
				List<Product> cartsize = userService.getcart();
				countsize = cartsize.size();
			}
		} catch (Exception e) {
			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			e.printStackTrace();
		}
		message = "";
		return countsize;
	}

	@RequestMapping(value = "/addtomycart", method = RequestMethod.POST)
	public @ResponseBody String add(HttpServletRequest request, HttpServletResponse response, HttpSession session1) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		String result = null;
		if (checkto == "done") {
			try {
				String id = request.getParameter("product_id");
				int product_id = Integer.parseInt(id);
				result = userService.addtocart(product_id);
			} catch (Exception e) {
				result = "Something is wrong";
			}
		} else {
			result = "userdashboard";
		}
		return result;
	}

	@RequestMapping(value = "/rmvtocart", method = RequestMethod.POST)
	public @ResponseBody String rmvtocart(HttpServletRequest request, HttpServletResponse response,
			HttpSession session1) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		String result = null;
		if (checkto == "done") {
			try {
				String id = request.getParameter("product_id");
				int product_id = Integer.parseInt(id);
				result = userService.rmvtocart(product_id);
			} catch (Exception e) {
				result = "Something is wrong";
			}
		} else {
			result = "userdashboard";
		}
		return result;
	}

	@RequestMapping(value = "/ratting", method = RequestMethod.POST)
	public @ResponseBody String ratting(HttpServletRequest request, HttpServletResponse response,
			HttpSession session1) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		String result = null;
		if (checkto == "done") {
			try {
				String id = request.getParameter("product_id");
				String st = request.getParameter("star");
				int product_id = Integer.parseInt(id);
				int star = Integer.parseInt(st);
				result = userService.ratting(product_id, star);
			} catch (Exception e) {
				result = "Something is wrong";
			}
		} else {
			result = "userdashboard";
		}
		return result;
	}

	@RequestMapping(value = "/savereview", method = RequestMethod.POST)
	public ModelAndView savereview(@ModelAttribute Reviews review, HttpServletRequest request, HttpSession session1) {
		String direct = "";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		String result = null;
		if (checkto == "done") {
			try {
				userService.savereview(review);
				direct = "redirect:/review?product_id=" + review.getProduct_id();
			} catch (Exception e) {
				direct = "redirect:/login";
			}
		}
		return new ModelAndView(direct);
	}

	@RequestMapping(value = "/savefavourite", method = RequestMethod.POST)
	public @ResponseBody String savefavourite(HttpServletRequest request, HttpServletResponse response,
			HttpSession session1) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		String result = null;
		if (checkto == "done") {
			try {
				String id = request.getParameter("product_id");
				int product_id = Integer.parseInt(id);
				result = userService.savefavourite(product_id);
			} catch (Exception e) {
				result = "Something is wrong";
			}
		} else {
			result = "userdashboard";
		}
		return result;
	}

	@RequestMapping(value = "/rmvqty", method = RequestMethod.POST)
	public @ResponseBody String rmvqty(HttpServletRequest request, HttpServletResponse response) {
		String result = null;
		try {
			String id = request.getParameter("product_id");

			int product_id = Integer.parseInt(id);
			result = userService.rmvqty(product_id);

		} catch (Exception e) {
			result = "Something is wrong";
		}
		return result;
	}

	@RequestMapping(value = "/addqty", method = RequestMethod.POST)
	public @ResponseBody String addqty(HttpServletRequest request, HttpServletResponse response) {
		String result = null;
		try {
			String id = request.getParameter("product_id");

			int product_id = Integer.parseInt(id);
			result = userService.addqty(product_id);

		} catch (Exception e) {
			result = "Something is wrong";
		}
		return result;
	}

	@RequestMapping(value = "/saveorder", method = RequestMethod.POST)
	public ModelAndView saveorder(@ModelAttribute Orders orders, HttpServletRequest request) {
		message = "";
		String direct = "";
		try {
			String path = request.getRequestURL().toString();
			boolean flag = userService.addorder(orders);
			// walletService.updateipdetails("registration", user.getEmailID());
			direct = "redirect:/userdashboard";
		} catch (Exception e) {
			direct = "redirect:/login";
		}

		return new ModelAndView(direct);
	}

	@RequestMapping(value = "/savetablebookdetail", method = RequestMethod.POST)
	public ModelAndView savetablebookdetail(@ModelAttribute Table_Book_Details tablebookdetail) {
		int flag = 0;
		String direct = "";
		try {
			flag = userService.savestudentdetails(tablebookdetail);
			direct = "redirect:/viewcontact";
		} catch (Exception e) {
			direct = "redirect:/login";
		}

		return new ModelAndView(direct);
	}
}
