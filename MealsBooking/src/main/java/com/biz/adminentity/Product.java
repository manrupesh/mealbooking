package com.biz.adminentity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "admin_products_list")
public class Product {

	// private static final long serialVersionUID = -1000119078147252957L;

	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name = "product_id")
	private int product_id;
	private String name;
	private String description;
	private String category;
	private String product_type;
	private String availability;
	private String product_size;
	private double discount_per;
	private String startDate;
	private String endDate;
	private double price;
	private String img;
	private int quantity;
	private int total_star;
	private String favourit;
	@Lob
	@Column(name = "product_image", nullable = false, columnDefinition = "mediumblob")
	private byte[] image;
	private Date createDate;
	private Character status;
	private Character carry;
	private Character deleted;

	@ManyToOne
	@JoinColumn(name = "admin_id")
	Admin admin_id = new Admin();
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "product_id")
	private Set<Product_Ratting> ratting;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "product_id")
	private Set<Product_Reviews> reviews;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "product_id")
	private Set<Product_Favourite> favourite;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "product_id")
	Set<Order_Details> order_details;

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getProduct_size() {
		return product_size;
	}

	public void setProduct_size(String product_size) {
		this.product_size = product_size;
	}

	public double getDiscount_per() {
		return discount_per;
	}

	public void setDiscount_per(double discount_per) {
		this.discount_per = discount_per;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Lob
	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Admin getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(Admin admin_id) {
		this.admin_id = admin_id;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getTotal_star() {
		return total_star;
	}

	public void setTotal_star(int total_star) {
		this.total_star = total_star;
	}

	public Set<Product_Ratting> getRatting() {
		return ratting;
	}

	public void setRatting(Set<Product_Ratting> ratting) {
		this.ratting = ratting;
	}

	public Set<Product_Reviews> getReviews() {
		return reviews;
	}

	public void setReviews(Set<Product_Reviews> reviews) {
		this.reviews = reviews;
	}

	public Set<Product_Favourite> getFavourite() {
		return favourite;
	}

	public void setFavourite(Set<Product_Favourite> favourite) {
		this.favourite = favourite;
	}

	public String getFavourit() {
		return favourit;
	}

	public void setFavourit(String favourit) {
		this.favourit = favourit;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public Character getCarry() {
		return carry;
	}

	public void setCarry(Character carry) {
		this.carry = carry;
	}

	public Character getDeleted() {
		return deleted;
	}

	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}

	public String getProduct_type() {
		return product_type;
	}

	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}

	public Set<Order_Details> getOrder_details() {
		return order_details;
	}

	public void setOrder_details(Set<Order_Details> order_details) {
		this.order_details = order_details;
	}

}
