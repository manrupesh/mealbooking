
package com.biz.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.biz.Mailling.Authenticate;
import com.biz.adminentity.Admin;
import com.biz.adminentity.Product;
import com.biz.adminentity.Product_Reviews;
import com.biz.adminentity.Table_Book_Details;
import com.biz.adminmodel.GetProductCartTotal;
import com.biz.adminmodel.Reviews;
import com.biz.model.ChangePassword;
import com.biz.model.Login;
import com.biz.model.User;
import com.biz.service.AdminService;
import com.biz.service.UserService;
import com.biz.service.WalletService;

@Controller
@RequestMapping("/")
public class WalletController {

	private static final Logger logger = Logger.getLogger(WalletController.class);
	private String uuid = null;

	@Autowired
	private WalletService walletService;
	@Autowired
	private AdminService adminService;
	@Autowired
	private UserService userService;
	String message = "";

	public WalletController() {
		System.out.println("WalletController()");
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView homepage(ModelAndView model, ModelMap model1, HttpSession session1, HttpServletRequest request,
			HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		try {
			List<Product> product1 = adminService.getproduct();
			for (Product product2 : product1) {
				userService.updateratting(product2.getProduct_id());
			}
			List<Product> product = adminService.getproduct();
			for (Product product2 : product) {
				String base64Encoded = null;
				byte[] bytes = product2.getImage();
				byte[] encodeBase64 = Base64.encodeBase64(bytes);
				byte[] bytes1=Base64.decodeBase64(encodeBase64);
				byte[] encodeBase641 = Base64.encodeBase64(bytes1);
				System.out.println("bytes1   "+bytes1);
				try {
					base64Encoded = new String(encodeBase641, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				product2.setImg(base64Encoded);
				String favourit = null;
				if (checkto == "done") {
					favourit = userService.getfovourite(product2.getProduct_id());
				}
				product2.setFavourit(favourit);
			}
			List<Product> cartsize = userService.getcart();
			model.addObject("cartsize", cartsize.size());
			model.addObject("product", product);
			model.setViewName("userdashboard");

		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/*")
	public ModelAndView welcomeUser1() {
		return new ModelAndView("404");
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String testlogin(@RequestParam(value = "uuid", required = false) String uuid, ModelMap model) {
		if (uuid != null) {
			walletService.checkUserActive(uuid);
		}
		message = "";
		return "login";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response, HttpSession session1) {
		session1.removeAttribute("MY_MESSAGES");

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		message = "";
		return "redirect:/welcome";
	}

	// open registration page
	@RequestMapping(value = "/userRegistration", method = RequestMethod.GET)
	public ModelAndView userRegistration(ModelAndView model, ModelMap model1) {
		try {
			User user = new User();
			model.addObject("user", user);
			model1.addAttribute("status", message);
			model.setViewName("Registration");
		} catch (Exception e) {
			model.setViewName("login");
		}
		message = "";
		return model;
	}

	// save user at registration time
	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute User user, HttpServletRequest request) {
		message = "";
		String direct = "";
		try {
			String path = request.getRequestURL().toString();
			message = walletService.addUser(user, path);
			// walletService.updateipdetails("registration", user.getEmailID());
			direct = "redirect:/userRegistration";
		} catch (Exception e) {
			direct = "redirect:/userRegistration";
		}

		return new ModelAndView(direct);
	}

	// after verify login email and password then open otp page
	@RequestMapping(value = "/authocode", method = RequestMethod.GET)
	public ModelAndView authocode(ModelAndView model, ModelMap model1) {
		try {

			Authenticate authenticate = new Authenticate();
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName(); // get logged in username
			model1.addAttribute("username", name);
			model.addObject("authenticate", authenticate);
			model.setViewName("checkCode");
		} catch (Exception e) {
			model.setViewName("login");
		}
		message = "";
		return model;
	}

	// after submit otp page check otp is corect or not
	@RequestMapping(value = "/checkcode", method = RequestMethod.POST)
	public ModelAndView checkcode(@ModelAttribute Authenticate authenticate, HttpServletRequest request1) {
		message = "";
		String direct = "";
		try {
			int flag = walletService.checkcode(authenticate);
			int flag1 = 0;
			if (flag == 0) {
				flag1 = walletService.checkcode1(authenticate);
			}
			// String direct = "";
			if (flag == 1) {
				User user = new User();
				String name = "done";
				request1.getSession().setAttribute("MY_MESSAGES", name);
				direct = "redirect:/userdashboard";
			} else if (flag1 == 1) {
				Admin admin = new Admin();
				String name = "done";
				request1.getSession().setAttribute("MY_MESSAGES", name);
				direct = "redirect:/admindashboard";
			} else {
				message = "You entered an old password or email address. Please try again with updated details.";
				direct = "redirect:/login";
			}

		} catch (Exception e) {
			direct = "redirect:/login";
		}

		return new ModelAndView(direct);
	}

	// open dashbord
	@RequestMapping(value = "/opendashboard", method = RequestMethod.GET)
	public ModelAndView opendashbord(ModelAndView model, ModelMap model1, HttpSession session1,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");

		try {
			if (checkto == "done") {
				model.setViewName("userdashboard");
			} else {
				model.setViewName("login");
			}
		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
			if (auth1 != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth1);
			}
			model.setViewName("login");
			e.printStackTrace();
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/forgotpass", method = RequestMethod.GET)
	public ModelAndView forgotpass(ModelAndView model) {
		Login login = new Login();
		model.addObject("login", login);
		model.setViewName("forgotpassword");
		message = "";
		return model;
	}

	// forgot password send password to user mail
	@RequestMapping(value = "/editpassword", method = RequestMethod.POST)
	public ModelAndView editpassword(@ModelAttribute User user, HttpSession session1) {

		String direct = "";

		try {

			message = "";
			int flag = walletService.editpassword(user);

			if (flag == 1) {
				direct = "redirect:/login";
			} else {
				message = "A server error occurred. Please try again in sometime.";
				direct = "redirect:/userRegistration";
			}

		} catch (Exception e) {

			session1.removeAttribute("MY_MESSAGES");
			e.printStackTrace();
			direct = "redirect:/login";
		}
		return new ModelAndView(direct);
	}

	// view change password page
	@RequestMapping(value = "/newpassword", method = RequestMethod.GET)
	public ModelAndView newpassword(ModelAndView model, HttpSession session1) {
		ChangePassword changepassword = new ChangePassword();
		String checkto = (String) session1.getAttribute("MY_MESSAGES");
		if (checkto == "done") {

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			User u = new User();
			u.setEmailID(name);
			User user = walletService.getUserdata(u);
			// model1.addAttribute("name", name);
			model.addObject("firstname", user.getFirstName());
			model.addObject("lastname", user.getLastName());
			model.addObject("emailid", user.getEmailID());
			model.addObject("phone", user.getPhone());
			model.addObject("name", name);
			model.addObject("changepassword", changepassword);
			model.setViewName("editpassword");

		} else {
			model.setViewName("login");
		}
		message = "";
		return model;
	}

	// change password using old password
	@RequestMapping(value = "/passwordchange", method = RequestMethod.POST)
	public ModelAndView passwordchange(@ModelAttribute ChangePassword changepassword, HttpSession session1) {
		message = "";
		String direct = "";
		try {
			int flag = walletService.passwordchange(changepassword);

			if (flag == 1) {
				direct = "redirect:/login";
			} else {
				message = "You entered an old password or email address. Please try again with updated details.";
				direct = "redirect:/userRegistration";
			}
		} catch (Exception e) {
			direct = "redirect:/login";
			session1.removeAttribute("MY_MESSAGES");
			e.printStackTrace();
		}
		return new ModelAndView(direct);
	}

	@RequestMapping(value = "/adminRegistration", method = RequestMethod.GET)
	public ModelAndView adminRegistration(ModelAndView model, ModelMap model1) {
		try {
			Admin admin = new Admin();
			model.addObject("user", admin);
			model1.addAttribute("status", message);
			model.setViewName("adminRegistration");
		} catch (Exception e) {
			model.setViewName("login");
		}
		message = "";
		return model;
	}

	@RequestMapping(value = "/saveAdmin", method = RequestMethod.POST)
	public ModelAndView saveAdmin(@ModelAttribute Admin admin, HttpServletRequest request) {
		message = "";
		String direct = "";
		try {
			String path = request.getRequestURL().toString();
			message = adminService.addadmin(admin, path);
			// walletService.updateipdetails("registration", user.getEmailID());
			direct = "redirect:/adminRegistration";
		} catch (Exception e) {
			direct = "redirect:/adminRegistration";
		}

		return new ModelAndView(direct);
	}
	/*
	 * @RequestMapping(value = "/saveproduct", method = RequestMethod.POST) public
	 * ModelAndView saveproduct(@ModelAttribute TestProject product, HttpSession
	 * session1) {
	 * 
	 * try { Product product1 = new Product();
	 * product1.setProductName(product.getProductName());
	 * product1.setProductType(product.getProductType());
	 * product1.setAvailability(product.getAvailability());
	 * product1.setHalfFull(product.getHalfFull());
	 * product1.setProductDescription(product.getProductDescription());
	 * MultipartFile file = product.getFile(); byte[] bytes = file.getBytes();
	 * product1.setImage(bytes); int flag = walletService.saveproduct(product1);
	 * return new ModelAndView("redirect:/opendashboard");
	 * 
	 * } catch (Exception e) { return new ModelAndView("redirect:/opendashboard"); }
	 * }
	 * 
	 * @RequestMapping(value = "/getstudentform", method = RequestMethod.GET) public
	 * String index() { return "studentform"; }
	 * 
	 * @RequestMapping(value = "/savestudentdetails", consumes =
	 * MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST) public String
	 * savestudentdetails(@RequestBody Student student) { int flag =
	 * walletService.savestudentdetails(student); return "admindashboard"; }
	 * 
	 * @RequestMapping(value = "/getstudentlist", method = RequestMethod.POST)
	 * public @ResponseBody ArrayList<Student> add(HttpServletRequest request,
	 * HttpServletResponse response) {
	 * 
	 * ArrayList<Student> student = new ArrayList<Student>(); student =
	 * walletService.getstudent(); return student; }
	 * 
	 * @RequestMapping(value = "/getproductlist", method = RequestMethod.POST)
	 * public @ResponseBody ArrayList<Product> addimage(HttpServletRequest request,
	 * HttpServletResponse response) {
	 * 
	 * ArrayList<Product> product = new ArrayList<Product>();
	 * 
	 * product = walletService.getproduct();
	 * 
	 * for (Product product2 : product) { String base64Encoded = null; byte[] bytes
	 * = product2.getImage(); byte[] encodeBase64 = Base64.encodeBase64(bytes); try
	 * { base64Encoded = new String(encodeBase64, "UTF-8"); } catch
	 * (UnsupportedEncodingException e) { e.printStackTrace(); }
	 * product2.setImg(base64Encoded); } return product; }
	 */

}
