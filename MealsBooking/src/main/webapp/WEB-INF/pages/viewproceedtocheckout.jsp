<%@page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
<script src="resources/myJS/bootstrap.min.js"></script>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>
<script src="resources/myJS/jquery-ui.js"></script>
<script src="resources/myJS/custom.js"></script>
<script src="resources/myJS/jquery.mark.min.js"></script>
<script src="resources/myJS/jquery.calculation.js"></script>
<script src="resources/myJS/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery.dataTables.css">
<!--=============== basic  ===============-->
<meta charset="UTF-8">
<title>Lambert - Responsive Pub / Restaurant Template</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="robots" content="index, follow" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<!--=============== css  ===============-->
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/reset.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/plugins.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/style.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/color.css">
<!--=============== favicons ===============-->
<link rel="shortcut icon" href="resources/restorant/images/favicon.ico">
<script type="text/javascript">
	var app = angular.module('myapp', []);
	app.controller('myappcontroller', function($scope, $http, $compile) {
		$http({
			method : 'POST',
			url : 'getCartTotal'
		}).then(function successCallback(response) {
			$scope.total = response.data.total;
			$scope.discounttotal = response.data.discounttotal;
			$scope.cgst = response.data.cgst;
			$scope.sgst = response.data.sgst;
			$scope.gstfinaltotal = response.data.gstfinaltotal;
		}, function errorCallback(response) {
			console.log(response.statusText);
		});

		/* $http({
			method : 'POST',
			url : 'getCartProduct'
		}).then(function successCallback(response) {
			$scope.product = response.data;
			
		}, function errorCallback(response) {
			console.log(response.statusText);
		}); */

		$scope.product_id = 0;
		$scope.rmvtocart = function(id) {
			var bvalue = id;
			$.ajax({
				type : 'POST',
				url : "rmvtocart",
				data : {
					product_id : bvalue
				},
				success : function(data) {
					location.reload(true);
					alert(data);

				}
			});
		}
		$scope.rmvqty = function(id) {
			var bvalue = id;
			var quantity = $(".product-qty-" + id).text();
			var price = $(".price-" + id).text();
			var total = price * (parseInt(quantity) - 1);
			$.ajax({
				type : 'POST',
				url : "rmvqty",
				data : {
					product_id : bvalue
				},
				success : function(data) {
					if (quantity > 1) {
						$(".product-qty-" + id).text((parseInt(quantity) - 1))
						$(".total-" + id).text("Rs. " + total)
						//$scope.check=(parseInt(quantity) - 1);
						$http({
							method : 'POST',
							url : 'getCartTotal'
						}).then(function successCallback(response) {
							$scope.total = response.data.total;
							$scope.discounttotal = response.data.discounttotal;
							$scope.cgst = response.data.cgst;
							$scope.sgst = response.data.sgst;
							$scope.gstfinaltotal = response.data.gstfinaltotal;
						}, function errorCallback(response) {
							console.log(response.statusText);
						});
					}

				}
			});
		}
		$scope.addqty = function(id) {
			var bvalue = id;
			var quantity = $(".product-qty-" + id).text();
			var price = $(".price-" + id).text();
			var total = price * (parseInt(quantity) + 1);
			$.ajax({
				type : 'POST',
				url : "addqty",
				data : {
					product_id : bvalue
				},
				success : function(data) {
					if (quantity < 11) {
						$(".product-qty-" + id).text((parseInt(quantity) + 1))
						$(".total-" + id).text("Rs. " + total)
						//$scope.check=(parseInt(quantity) + 1);
						$http({
							method : 'POST',
							url : 'getCartTotal'
						}).then(function successCallback(response) {
							$scope.total = response.data.total;
							$scope.discounttotal = response.data.discounttotal;
							$scope.cgst = response.data.cgst;
							$scope.sgst = response.data.sgst;
							$scope.gstfinaltotal = response.data.gstfinaltotal;
						}, function errorCallback(response) {
							console.log(response.statusText);
						});
					}
					//location.reload(true);
					//alert(data);

				}
			});
		}
	});
</script>
<style>
mark {
	padding: 0px;
	background-color: #f1c40f;
}

.quantity {
	float: left;
	padding: 20px 0;
	width: 100%;
}
</style>
</head>
<body ng-app="myapp" ng-controller="myappcontroller">
	<div class="loader">
		<img src="resources/restorant/images/loader.png" alt="">
	</div>
	<!--================= main start ================-->
	<div id="main">
		<!--=============== header ===============-->
		<header>
		<div class="header-inner">
			<div class="container">
				<!--navigation social links-->
				<div class="nav-social">
					<ul>
						<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" target="_blank"><i
								class="fa fa-instagram"></i></a></li>
						<li><a href="#" target="_blank"><i
								class="fa fa-pinterest"></i></a></li>
						<li><a href="#" target="_blank"><i class="fa fa-tumblr"></i></a></li>
					</ul>
				</div>
				<!--logo-->
				<div class="logo-holder">
					<a href="index.html"> <img
						src="resources/restorant/images/logo.png" class="respimg logo-vis"
						alt=""> <img src="resources/restorant/images/logo2.png"
						class="respimg logo-notvis" alt="">
					</a>
				</div>
				<!--Navigation -->
				<c:set var="total" value="${0}" />
				<c:forEach var="prodInfo" items="${product}">
					<c:set var="total" value="${total+1}" />
				</c:forEach>
				<div class="subnav">
					<a href="showcart"><i class="fa fa-shopping-cart"></i>( <span>${total}</span>
						)</a>
				</div>
				<div class="nav-holder">
					<nav>
					<ul>
						<li><a href="userdashboard">Home</a></li>
						<li><a href="viewmenulist">Menu</a></li>
						<li><a href="viewgallery">Gallery</a></li>
						<li><a href="getproductlist">Shop</a></li>
						<li><a href="viewcontact">Contact</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a href="viewmyorders">My Orders</a></li>
						</sec:authorize>
						<li class="hideli"><a href="userRegistration">Sign Up</a></li>
						<li class="hideli"><a href="login">Sign In</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a class="has-arrow waves-effect waves-dark"
								href="<c:url value="/logout"/>" aria-expanded="false"> <i
									class="mdi mdi-login-variant"></i> Logout
							</a></li>
							<script type="text/javascript">
								$(".hideli").hide();
							</script>
						</sec:authorize>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		</header>
		<!--header end-->
		<!--=============== wrapper ===============-->
		<div id="wrapper">
			<div class="content">
				<section class="parallax-section header-section">
				<div class="bg bg-parallax"
					style="background-image: url(resources/restorant/images/bg/21.jpg)"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);"></div>
				<div class="overlay"></div>
				<div class="container">
					<h2>Proceed TO CheckOut</h2>
					<h3>Order online is easy</h3>
				</div>
				</section>
				<section>
				<div class="triangle-decor"></div>
				<div class="container">
					<!-- CHECKOUT TABLE -->
					<div class="row">
						<div class="col-sm-12">
							<form:form class="form-horizontal form-material" id="loginform"
								name="signIn" action="saveorder" method="post"
								modelAttribute="orders" onsubmit='return checkLoginForm(this);'>
								<h3 class="box-title m-b-20">Sign Up</h3>
								<div class="form-group ">
									<div class="col-xs-12">
										<form:input class="form-control lab" path="customerName"
											type="text" id="firstName" name="customerName"
											placeholder="Name" required="" />
									</div>
								</div>
								<div class="form-group ">
									<div class="col-xs-12">
										<form:input class="form-control lab" path="customerAddress"
											type="text" id="lastName" name="lastName"
											placeholder="Delivery Address" required="" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-12">
										<form:input class="form-control lab" required=""
											placeholder="EmailID" path="customerEmail" type="email"
											id="email" name="email" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-12">
										<form:input class="form-control lab" required="" id="phone"
											path="customerPhone" placeholder="Contact No" type="text"
											name="phone" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-12">
										<form:input required="" path="total_product_amount"
											value="{{total}}" type="text" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-12">
										<form:input required="" path="total_discount_amount"
											value="{{discounttotal}}" type="text" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-12">
										<form:input required="" path="tax_cgst" value="{{cgst}}"
											type="text" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-12">
										<form:input required="" path="tax_sgst" value="{{sgst}}"
											type="text" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-12">
										<form:input required="" path="final_paid_amount"
											value="{{gstfinaltotal}}" type="text" />
									</div>
								</div>
								<div class="form-group text-center m-t-20">
									<div class="col-xs-12">
										<button type="submit"
											class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
											style="padding-bottom: 41px;">Order Now</button><a href="showcart">Edit Cart</a>
									</div>
								</div>
							</form:form>
						</div>
					</div>
				</div>
				</section>
			</div>
			<!--=============== footer ===============-->
			<footer>
			<div class="footer-inner">
				<div class="container">
					<div class="row">
						<!--tiwtter-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Our twitter</h4>
								<div class="twitter-holder">
									<div class="twitts">
										<div class="twitter-feed">
											<!-- Twitter Slider -->
											<div id="twitter-feed"></div>
										</div>
									</div>
									<div class="customNavigation">
										<a class="prev-slide transition"><i
											class="fa fa-long-arrow-left"></i></a> <a
											class="twit-link transition"
											href="https://twitter.com/katokli3mmm" target="_blank"><i
											class="fa fa-twitter"></i></a> <a class="next-slide transition"><i
											class="fa fa-long-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<!--footer social links-->
						<div class="col-md-4">
							<div class="footer-social">
								<h3>Find us</h3>
								<ul>
									<li><a href="#" target="_blank"><i
											class="fa fa-facebook"></i></a></li>
									<li><a href="#" target="_blank"><i
											class="fa fa-twitter"></i></a></li>
									<li><a href="#" target="_blank"><i
											class="fa fa-instagram"></i></a></li>
									<li><a href="#" target="_blank"><i
											class="fa fa-pinterest"></i></a></li>
									<li><a href="#" target="_blank"><i
											class="fa fa-tumblr"></i></a></li>
								</ul>
							</div>
						</div>
						<!--subscribe form-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Newsletter</h4>
								<div class="subcribe-form">
									<form id="subscribe">
										<input class="enteremail" name="email" id="subscribe-email"
											placeholder="Your email address.." spellcheck="false"
											type="text">
										<button type="submit" id="subscribe-button"
											class="subscribe-button">
											<i class="fa fa-envelope"></i>
										</button>
										<label for="subscribe-email" class="subscribe-message"></label>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
					<!--footer contacts links -->
					<ul class="footer-contacts">
						<li><a href="#">+7(111)123456789</a></li>
						<li><a href="#">27th Brooklyn New York, NY 10065</a></li>
						<li><a href="#">yourmail@domain.com</a></li>
					</ul>
				</div>
			</div>
			<!--to top / privacy policy-->
			<div class="to-top-holder">
				<div class="container">
					<p>
						<span> &#169; Lambert 2015 . </span> All rights reserved.
					</p>
					<div class="to-top">
						<span>Back To Top </span><i class="fa fa-angle-double-up"></i>
					</div>
				</div>
			</div>
			</footer>
			<!--footer end -->
		</div>
		<!-- wrapper end -->
	</div>
	<!-- Main end -->
	<!--=============== google map ===============-->
	<script type="text/javascript"
		src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<!--=============== scripts  ===============-->
	<script type="text/javascript"
		src="resources/restorant/js/jquery.min.js"></script>
	<script type="text/javascript" src="resources/restorant/js/plugins.js"></script>
	<script type="text/javascript" src="resources/restorant/js/scripts.js"></script>
</body>

<!-- Mirrored from lambert.kwst.net/site/cart.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 01 Feb 2019 10:49:25 GMT -->
</html>