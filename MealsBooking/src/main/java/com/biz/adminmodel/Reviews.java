package com.biz.adminmodel;

import java.util.Date;

public class Reviews {

	private int reviews_id;
	private int uid;
	private String firstName;
	private String lastName;
	private Date review_date;
	private String review;
	private int product_id;
	public int getReviews_id() {
		return reviews_id;
	}
	public void setReviews_id(int reviews_id) {
		this.reviews_id = reviews_id;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getReview_date() {
		return review_date;
	}
	public void setReview_date(Date review_date) {
		this.review_date = review_date;
	}
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	
	

}
