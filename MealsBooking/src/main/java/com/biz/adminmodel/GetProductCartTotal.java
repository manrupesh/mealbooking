package com.biz.adminmodel;

public class GetProductCartTotal {
	private double total;
	private double discounttotal;
	private double gstfinaltotal;
	private double cgst;
	private double sgst;

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getDiscounttotal() {
		return discounttotal;
	}

	public void setDiscounttotal(double discounttotal) {
		this.discounttotal = discounttotal;
	}

	public double getGstfinaltotal() {
		return gstfinaltotal;
	}

	public void setGstfinaltotal(double gstfinaltotal) {
		this.gstfinaltotal = gstfinaltotal;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

}
