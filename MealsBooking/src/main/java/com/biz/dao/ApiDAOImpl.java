package com.biz.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biz.model.Student;

@Repository
public class ApiDAOImpl implements ApiDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public String postsave(Student student) {
		String result="error";
		Session session=sessionFactory.getCurrentSession();
		session.save(student);
		result="success";
		return result;
	}

}
