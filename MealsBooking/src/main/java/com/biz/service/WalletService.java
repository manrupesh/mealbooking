package com.biz.service;


import com.biz.Mailling.Authenticate;
import com.biz.model.ChangePassword;
import com.biz.model.Login;
import com.biz.model.User;

public interface WalletService {

	// save user registration time and encript password and send link verification
	// mail
	public String addUser(User user, String path);

	// check user correct or not login time
	public boolean checkUser(Login login);

	// check otp mail time correct or not
	public int checkcode(Authenticate authenticate);

	// forgot password to send on user mail new password
	public int editpassword(User user);

	// change password using old password
	public int passwordchange(ChangePassword changepassword);

	// check user active or not registration login time
	public void checkUserActive(String uuid);

	public User getUserdata(User u);

	public User getUserdata1(User u);

	//public void updateipdetails(String logs, String emailid);

	
	//  public int saveproduct(Product product);
	 /* 
	 * public int savestudentdetails(Student student);
	 * 
	 * public ArrayList<Student> getstudent();
	 * 
	 * public ArrayList<Product> getproduct();
	 */

	public boolean checkUser1(Login login);

	public int checkcode1(Authenticate authenticate);

}
