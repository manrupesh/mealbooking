package com.biz.dao;

import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biz.Mailling.Mailing;
import com.biz.Mailling.NewOTP;
import com.biz.Mailling.RandomPassword;
import com.biz.Mailling.UniqueKey;
import com.biz.adminentity.Admin;
import com.biz.Mailling.AesCrypto;
import com.biz.Mailling.Encryption;
import com.biz.model.ChangePassword;
import com.biz.model.Login;
import com.biz.model.User;

@Repository
public class WalletDAOImpl implements WalletDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public User addUser(User user) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", user.getEmailID()));
		User users = (User) criteria.uniqueResult();

		Criteria criteria11 = session.createCriteria(User.class);
		criteria11.add(Restrictions.eq("phone", user.getPhone()));
		User users11 = (User) criteria11.uniqueResult();
		User userdata = new User();
		userdata.setEmailID("setforcheck");// set for reference checking purpose

		if (users11 != null) {
			userdata.setEmailID("already exist");
		} else {
			if (users != null) {
				userdata.setEmailID("already exist");
			} else {

				// reference check if exists
				if (!user.getReferencecode().isEmpty()) {
					Criteria criteria1 = session.createCriteria(User.class);
					criteria1.add(Restrictions.eq("referencecode", user.getReferencecode()));
					User users1 = (User) criteria1.uniqueResult();
					if (users1 != null) {
						user.setReferencecodeuserid(users1.getUid());
					} else {

						userdata.setEmailID("wrong reference code");
					}

				}

				UniqueKey uniqueKey = new UniqueKey();
				String uuid = uniqueKey.uniquekey();
				user.setActive(false);
				user.setDelete(false);
				user.setUuid(uuid);

				String uniqueId = UUID.randomUUID().toString();

				user.setReferencecode(uniqueId);

				/*
				 * User_Addresses useraddress = new User_Addresses(); useraddress.setUid(user);
				 * 
				 * SendFund sendfund = new SendFund(); sendfund.setUid(user);
				 */

				if (userdata.getEmailID().equals("setforcheck")) {

					/*
					 * session.save(useraddress); session.save(sendfund);
					 */
					session.save(user);
					userdata = user;
				}

			}
		}

		return userdata;
	}

	// get user data
	@Override
	public User getUserdata(User user) {
		Session session = sessionFactory.getCurrentSession();
		User userdata = new User();

		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", user.getEmailID()));
		criteria.add(Restrictions.eq("isActive", true));
		User users = (User) criteria.uniqueResult();
		if (users != null) {
			userdata = users;
		} else {
			userdata = users;
		}
		return userdata;
	}

	// get user data
	@Override
	public User getUserdata1(User user) {
		Session session = sessionFactory.getCurrentSession();
		User userdata = new User();

		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("phone", user.getEmailID()));
		criteria.add(Restrictions.eq("isActive", true));
		User users = (User) criteria.uniqueResult();
		if (users != null) {
			userdata = users;
		} else {
			userdata = users;
		}
		return userdata;
	}

	// forgot password and password send on mail
	@Override
	public int editpassword(User user) {
		Session session = sessionFactory.getCurrentSession();
		NewOTP newotp = new NewOTP();
		String otp = newotp.generateOTP1();
		String mail = user.getEmailID();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", mail));
		User users = (User) criteria.uniqueResult();
		int flag = 0;
		if (users != null) {
			// .String s = mail.substring(0, 3);
			RandomPassword r = new RandomPassword();
			String newpassword = r.generateRandomPassword();
			// System.out.println("fist_____)))))))))))))))__________"+newpassword);
			Mailing m = new Mailing();
			m.setOtp(
					"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional //EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><!--[if IE]><html xmlns=\"http://www.w3.org/1999/xhtml\" class=\"ie\"><![endif]--><!--[if !IE]><!--><html style=\"margin: 0;padding: 0;\" xmlns=\"http://www.w3.org/1999/xhtml\"><!--<![endif]--><head>\r\n"
							+ "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n"
							+ "    <title></title>\r\n"
							+ "    <!--[if !mso]><!--><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /><!--<![endif]-->\r\n"
							+ "    <meta name=\"viewport\" content=\"width=device-width\" /><style type=\"text/css\">\r\n"
							+ "@media only screen and (min-width: 620px){.wrapper{min-width:600px !important}.wrapper h1{}.wrapper h1{font-size:64px !important;line-height:63px !important}.wrapper h2{}.wrapper h2{font-size:30px !important;line-height:38px !important}.wrapper h3{}.wrapper h3{font-size:22px !important;line-height:31px !important}.column{}.wrapper .size-8{font-size:8px !important;line-height:14px !important}.wrapper .size-9{font-size:9px !important;line-height:16px !important}.wrapper .size-10{font-size:10px !important;line-height:18px !important}.wrapper .size-11{font-size:11px !important;line-height:19px !important}.wrapper .size-12{font-size:12px !important;line-height:19px !important}.wrapper .size-13{font-size:13px !important;line-height:21px !important}.wrapper .size-14{font-size:14px !important;line-height:21px !important}.wrapper .size-15{font-size:15px !important;line-height:23px \r\n"
							+ "!important}.wrapper .size-16{font-size:16px !important;line-height:24px !important}.wrapper .size-17{font-size:17px !important;line-height:26px !important}.wrapper .size-18{font-size:18px !important;line-height:26px !important}.wrapper .size-20{font-size:20px !important;line-height:28px !important}.wrapper .size-22{font-size:22px !important;line-height:31px !important}.wrapper .size-24{font-size:24px !important;line-height:32px !important}.wrapper .size-26{font-size:26px !important;line-height:34px !important}.wrapper .size-28{font-size:28px !important;line-height:36px !important}.wrapper .size-30{font-size:30px !important;line-height:38px !important}.wrapper .size-32{font-size:32px !important;line-height:40px !important}.wrapper .size-34{font-size:34px !important;line-height:43px !important}.wrapper .size-36{font-size:36px !important;line-height:43px !important}.wrapper \r\n"
							+ ".size-40{font-size:40px !important;line-height:47px !important}.wrapper .size-44{font-size:44px !important;line-height:50px !important}.wrapper .size-48{font-size:48px !important;line-height:54px !important}.wrapper .size-56{font-size:56px !important;line-height:60px !important}.wrapper .size-64{font-size:64px !important;line-height:63px !important}}\r\n"
							+ "</style>\r\n" + "    <style type=\"text/css\">\r\n" + "body {\r\n" + "  margin: 0;\r\n"
							+ "  padding: 0;\r\n" + "}\r\n" + "table {\r\n" + "  border-collapse: collapse;\r\n"
							+ "  table-layout: fixed;\r\n" + "}\r\n" + "* {\r\n" + "  line-height: inherit;\r\n"
							+ "}\r\n" + "[x-apple-data-detectors],\r\n" + "[href^=\"tel\"],\r\n"
							+ "[href^=\"sms\"] {\r\n" + "  color: inherit !important;\r\n"
							+ "  text-decoration: none !important;\r\n" + "}\r\n"
							+ ".wrapper .footer__share-button a:hover,\r\n"
							+ ".wrapper .footer__share-button a:focus {\r\n" + "  color: #ffffff !important;\r\n"
							+ "}\r\n" + ".btn a:hover,\r\n" + ".btn a:focus,\r\n" + ".footer__share-button a:hover,\r\n"
							+ ".footer__share-button a:focus,\r\n" + ".email-footer__links a:hover,\r\n"
							+ ".email-footer__links a:focus {\r\n" + "  opacity: 0.8;\r\n" + "}\r\n" + ".preheader,\r\n"
							+ ".header,\r\n" + ".layout,\r\n" + ".column {\r\n"
							+ "  transition: width 0.25s ease-in-out, max-width 0.25s ease-in-out;\r\n" + "}\r\n"
							+ ".preheader td {\r\n" + "  padding-bottom: 8px;\r\n" + "}\r\n" + ".layout,\r\n"
							+ "div.header {\r\n" + "  max-width: 400px !important;\r\n"
							+ "  -fallback-width: 95% !important;\r\n" + "  width: calc(100% - 20px) !important;\r\n"
							+ "}\r\n" + "div.preheader {\r\n" + "  max-width: 360px !important;\r\n"
							+ "  -fallback-width: 90% !important;\r\n" + "  width: calc(100% - 60px) !important;\r\n"
							+ "}\r\n" + ".snippet,\r\n" + ".webversion {\r\n" + "  Float: none !important;\r\n"
							+ "}\r\n" + ".column {\r\n" + "  max-width: 400px !important;\r\n"
							+ "  width: 100% !important;\r\n" + "}\r\n" + ".fixed-width.has-border {\r\n"
							+ "  max-width: 402px !important;\r\n" + "}\r\n"
							+ ".fixed-width.has-border .layout__inner {\r\n" + "  box-sizing: border-box;\r\n" + "}\r\n"
							+ ".snippet,\r\n" + ".webversion {\r\n" + "  width: 50% !important;\r\n" + "}\r\n"
							+ ".ie .btn {\r\n" + "  width: 100%;\r\n" + "}\r\n" + "[owa] .column div,\r\n"
							+ "[owa] .column button {\r\n" + "  display: block !important;\r\n" + "}\r\n"
							+ ".ie .column,\r\n" + "[owa] .column,\r\n" + ".ie .gutter,\r\n" + "[owa] .gutter {\r\n"
							+ "  display: table-cell;\r\n" + "  float: none !important;\r\n"
							+ "  vertical-align: top;\r\n" + "}\r\n" + ".ie div.preheader,\r\n"
							+ "[owa] div.preheader,\r\n" + ".ie .email-footer,\r\n" + "[owa] .email-footer {\r\n"
							+ "  max-width: 560px !important;\r\n" + "  width: 560px !important;\r\n" + "}\r\n"
							+ ".ie .snippet,\r\n" + "[owa] .snippet,\r\n" + ".ie .webversion,\r\n"
							+ "[owa] .webversion {\r\n" + "  width: 280px !important;\r\n" + "}\r\n"
							+ ".ie div.header,\r\n" + "[owa] div.header,\r\n" + ".ie .layout,\r\n"
							+ "[owa] .layout,\r\n" + ".ie .one-col .column,\r\n" + "[owa] .one-col .column {\r\n"
							+ "  max-width: 600px !important;\r\n" + "  width: 600px !important;\r\n" + "}\r\n"
							+ ".ie .fixed-width.has-border,\r\n" + "[owa] .fixed-width.has-border,\r\n"
							+ ".ie .has-gutter.has-border,\r\n" + "[owa] .has-gutter.has-border {\r\n"
							+ "  max-width: 602px !important;\r\n" + "  width: 602px !important;\r\n" + "}\r\n"
							+ ".ie .two-col .column,\r\n" + "[owa] .two-col .column {\r\n"
							+ "  max-width: 300px !important;\r\n" + "  width: 300px !important;\r\n" + "}\r\n"
							+ ".ie .three-col .column,\r\n" + "[owa] .three-col .column,\r\n" + ".ie .narrow,\r\n"
							+ "[owa] .narrow {\r\n" + "  max-width: 200px !important;\r\n"
							+ "  width: 200px !important;\r\n" + "}\r\n" + ".ie .wide,\r\n" + "[owa] .wide {\r\n"
							+ "  width: 400px !important;\r\n" + "}\r\n" + ".ie .two-col.has-gutter .column,\r\n"
							+ "[owa] .two-col.x_has-gutter .column {\r\n" + "  max-width: 290px !important;\r\n"
							+ "  width: 290px !important;\r\n" + "}\r\n" + ".ie .three-col.has-gutter .column,\r\n"
							+ "[owa] .three-col.x_has-gutter .column,\r\n" + ".ie .has-gutter .narrow,\r\n"
							+ "[owa] .has-gutter .narrow {\r\n" + "  max-width: 188px !important;\r\n"
							+ "  width: 188px !important;\r\n" + "}\r\n" + ".ie .has-gutter .wide,\r\n"
							+ "[owa] .has-gutter .wide {\r\n" + "  max-width: 394px !important;\r\n"
							+ "  width: 394px !important;\r\n" + "}\r\n"
							+ ".ie .two-col.has-gutter.has-border .column,\r\n"
							+ "[owa] .two-col.x_has-gutter.x_has-border .column {\r\n"
							+ "  max-width: 292px !important;\r\n" + "  width: 292px !important;\r\n" + "}\r\n"
							+ ".ie .three-col.has-gutter.has-border .column,\r\n"
							+ "[owa] .three-col.x_has-gutter.x_has-border .column,\r\n"
							+ ".ie .has-gutter.has-border .narrow,\r\n" + "[owa] .has-gutter.x_has-border .narrow {\r\n"
							+ "  max-width: 190px !important;\r\n" + "  width: 190px !important;\r\n" + "}\r\n"
							+ ".ie .has-gutter.has-border .wide,\r\n" + "[owa] .has-gutter.x_has-border .wide {\r\n"
							+ "  max-width: 396px !important;\r\n" + "  width: 396px !important;\r\n" + "}\r\n"
							+ ".ie .fixed-width .layout__inner {\r\n" + "  border-left: 0 none white !important;\r\n"
							+ "  border-right: 0 none white !important;\r\n" + "}\r\n" + ".ie .layout__edges {\r\n"
							+ "  display: none;\r\n" + "}\r\n" + ".mso .layout__edges {\r\n" + "  font-size: 0;\r\n"
							+ "}\r\n" + ".layout-fixed-width,\r\n" + ".mso .layout-full-width {\r\n"
							+ "  background-color: #ffffff;\r\n" + "}\r\n"
							+ "@media only screen and (min-width: 620px) {\r\n" + "  .column,\r\n" + "  .gutter {\r\n"
							+ "    display: table-cell;\r\n" + "    Float: none !important;\r\n"
							+ "    vertical-align: top;\r\n" + "  }\r\n" + "  div.preheader,\r\n"
							+ "  .email-footer {\r\n" + "    max-width: 560px !important;\r\n"
							+ "    width: 560px !important;\r\n" + "  }\r\n" + "  .snippet,\r\n" + "  .webversion {\r\n"
							+ "    width: 280px !important;\r\n" + "  }\r\n" + "  div.header,\r\n" + "  .layout,\r\n"
							+ "  .one-col .column {\r\n" + "    max-width: 600px !important;\r\n"
							+ "    width: 600px !important;\r\n" + "  }\r\n" + "  .fixed-width.has-border,\r\n"
							+ "  .fixed-width.ecxhas-border,\r\n" + "  .has-gutter.has-border,\r\n"
							+ "  .has-gutter.ecxhas-border {\r\n" + "    max-width: 602px !important;\r\n"
							+ "    width: 602px !important;\r\n" + "  }\r\n" + "  .two-col .column {\r\n"
							+ "    max-width: 300px !important;\r\n" + "    width: 300px !important;\r\n" + "  }\r\n"
							+ "  .three-col .column,\r\n" + "  .column.narrow {\r\n"
							+ "    max-width: 200px !important;\r\n" + "    width: 200px !important;\r\n" + "  }\r\n"
							+ "  .column.wide {\r\n" + "    width: 400px !important;\r\n" + "  }\r\n"
							+ "  .two-col.has-gutter .column,\r\n" + "  .two-col.ecxhas-gutter .column {\r\n"
							+ "    max-width: 290px !important;\r\n" + "    width: 290px !important;\r\n" + "  }\r\n"
							+ "  .three-col.has-gutter .column,\r\n" + "  .three-col.ecxhas-gutter .column,\r\n"
							+ "  .has-gutter .narrow {\r\n" + "    max-width: 188px !important;\r\n"
							+ "    width: 188px !important;\r\n" + "  }\r\n" + "  .has-gutter .wide {\r\n"
							+ "    max-width: 394px !important;\r\n" + "    width: 394px !important;\r\n" + "  }\r\n"
							+ "  .two-col.has-gutter.has-border .column,\r\n"
							+ "  .two-col.ecxhas-gutter.ecxhas-border .column {\r\n"
							+ "    max-width: 292px !important;\r\n" + "    width: 292px !important;\r\n" + "  }\r\n"
							+ "  .three-col.has-gutter.has-border .column,\r\n"
							+ "  .three-col.ecxhas-gutter.ecxhas-border .column,\r\n"
							+ "  .has-gutter.has-border .narrow,\r\n" + "  .has-gutter.ecxhas-border .narrow {\r\n"
							+ "    max-width: 190px !important;\r\n" + "    width: 190px !important;\r\n" + "  }\r\n"
							+ "  .has-gutter.has-border .wide,\r\n" + "  .has-gutter.ecxhas-border .wide {\r\n"
							+ "    max-width: 396px !important;\r\n" + "    width: 396px !important;\r\n" + "  }\r\n"
							+ "}\r\n"
							+ "@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {\r\n"
							+ "  .fblike {\r\n"
							+ "    background-image: url(https://i10.createsend1.com/static/eb/master/13-the-blueprint-3/images/fblike@2x.png) !important;\r\n"
							+ "  }\r\n" + "  .tweet {\r\n"
							+ "    background-image: url(https://i7.createsend1.com/static/eb/master/13-the-blueprint-3/images/tweet@2x.png) !important;\r\n"
							+ "  }\r\n" + "  .linkedinshare {\r\n"
							+ "    background-image: url(https://i8.createsend1.com/static/eb/master/13-the-blueprint-3/images/lishare@2x.png) !important;\r\n"
							+ "  }\r\n" + "  .forwardtoafriend {\r\n"
							+ "    background-image: url(https://i9.createsend1.com/static/eb/master/13-the-blueprint-3/images/forward@2x.png) !important;\r\n"
							+ "  }\r\n" + "}\r\n" + "@media (max-width: 321px) {\r\n"
							+ "  .fixed-width.has-border .layout__inner {\r\n"
							+ "    border-width: 1px 0 !important;\r\n" + "  }\r\n" + "  .layout,\r\n"
							+ "  .column {\r\n" + "    min-width: 320px !important;\r\n"
							+ "    width: 320px !important;\r\n" + "  }\r\n" + "  .border {\r\n"
							+ "    display: none;\r\n" + "  }\r\n" + "}\r\n" + ".mso div {\r\n"
							+ "  border: 0 none white !important;\r\n" + "}\r\n" + ".mso .w560 .divider {\r\n"
							+ "  Margin-left: 260px !important;\r\n" + "  Margin-right: 260px !important;\r\n" + "}\r\n"
							+ ".mso .w360 .divider {\r\n" + "  Margin-left: 160px !important;\r\n"
							+ "  Margin-right: 160px !important;\r\n" + "}\r\n" + ".mso .w260 .divider {\r\n"
							+ "  Margin-left: 110px !important;\r\n" + "  Margin-right: 110px !important;\r\n" + "}\r\n"
							+ ".mso .w160 .divider {\r\n" + "  Margin-left: 60px !important;\r\n"
							+ "  Margin-right: 60px !important;\r\n" + "}\r\n" + ".mso .w354 .divider {\r\n"
							+ "  Margin-left: 157px !important;\r\n" + "  Margin-right: 157px !important;\r\n" + "}\r\n"
							+ ".mso .w250 .divider {\r\n" + "  Margin-left: 105px !important;\r\n"
							+ "  Margin-right: 105px !important;\r\n" + "}\r\n" + ".mso .w148 .divider {\r\n"
							+ "  Margin-left: 54px !important;\r\n" + "  Margin-right: 54px !important;\r\n" + "}\r\n"
							+ ".mso .size-8,\r\n" + ".ie .size-8 {\r\n" + "  font-size: 8px !important;\r\n"
							+ "  line-height: 14px !important;\r\n" + "}\r\n" + ".mso .size-9,\r\n"
							+ ".ie .size-9 {\r\n" + "  font-size: 9px !important;\r\n"
							+ "  line-height: 16px !important;\r\n" + "}\r\n" + ".mso .size-10,\r\n"
							+ ".ie .size-10 {\r\n" + "  font-size: 10px !important;\r\n"
							+ "  line-height: 18px !important;\r\n" + "}\r\n" + ".mso .size-11,\r\n"
							+ ".ie .size-11 {\r\n" + "  font-size: 11px !important;\r\n"
							+ "  line-height: 19px !important;\r\n" + "}\r\n" + ".mso .size-12,\r\n"
							+ ".ie .size-12 {\r\n" + "  font-size: 12px !important;\r\n"
							+ "  line-height: 19px !important;\r\n" + "}\r\n" + ".mso .size-13,\r\n"
							+ ".ie .size-13 {\r\n" + "  font-size: 13px !important;\r\n"
							+ "  line-height: 21px !important;\r\n" + "}\r\n" + ".mso .size-14,\r\n"
							+ ".ie .size-14 {\r\n" + "  font-size: 14px !important;\r\n"
							+ "  line-height: 21px !important;\r\n" + "}\r\n" + ".mso .size-15,\r\n"
							+ ".ie .size-15 {\r\n" + "  font-size: 15px !important;\r\n"
							+ "  line-height: 23px !important;\r\n" + "}\r\n" + ".mso .size-16,\r\n"
							+ ".ie .size-16 {\r\n" + "  font-size: 16px !important;\r\n"
							+ "  line-height: 24px !important;\r\n" + "}\r\n" + ".mso .size-17,\r\n"
							+ ".ie .size-17 {\r\n" + "  font-size: 17px !important;\r\n"
							+ "  line-height: 26px !important;\r\n" + "}\r\n" + ".mso .size-18,\r\n"
							+ ".ie .size-18 {\r\n" + "  font-size: 18px !important;\r\n"
							+ "  line-height: 26px !important;\r\n" + "}\r\n" + ".mso .size-20,\r\n"
							+ ".ie .size-20 {\r\n" + "  font-size: 20px !important;\r\n"
							+ "  line-height: 28px !important;\r\n" + "}\r\n" + ".mso .size-22,\r\n"
							+ ".ie .size-22 {\r\n" + "  font-size: 22px !important;\r\n"
							+ "  line-height: 31px !important;\r\n" + "}\r\n" + ".mso .size-24,\r\n"
							+ ".ie .size-24 {\r\n" + "  font-size: 24px !important;\r\n"
							+ "  line-height: 32px !important;\r\n" + "}\r\n" + ".mso .size-26,\r\n"
							+ ".ie .size-26 {\r\n" + "  font-size: 26px !important;\r\n"
							+ "  line-height: 34px !important;\r\n" + "}\r\n" + ".mso .size-28,\r\n"
							+ ".ie .size-28 {\r\n" + "  font-size: 28px !important;\r\n"
							+ "  line-height: 36px !important;\r\n" + "}\r\n" + ".mso .size-30,\r\n"
							+ ".ie .size-30 {\r\n" + "  font-size: 30px !important;\r\n"
							+ "  line-height: 38px !important;\r\n" + "}\r\n" + ".mso .size-32,\r\n"
							+ ".ie .size-32 {\r\n" + "  font-size: 32px !important;\r\n"
							+ "  line-height: 40px !important;\r\n" + "}\r\n" + ".mso .size-34,\r\n"
							+ ".ie .size-34 {\r\n" + "  font-size: 34px !important;\r\n"
							+ "  line-height: 43px !important;\r\n" + "}\r\n" + ".mso .size-36,\r\n"
							+ ".ie .size-36 {\r\n" + "  font-size: 36px !important;\r\n"
							+ "  line-height: 43px !important;\r\n" + "}\r\n" + ".mso .size-40,\r\n"
							+ ".ie .size-40 {\r\n" + "  font-size: 40px !important;\r\n"
							+ "  line-height: 47px !important;\r\n" + "}\r\n" + ".mso .size-44,\r\n"
							+ ".ie .size-44 {\r\n" + "  font-size: 44px !important;\r\n"
							+ "  line-height: 50px !important;\r\n" + "}\r\n" + ".mso .size-48,\r\n"
							+ ".ie .size-48 {\r\n" + "  font-size: 48px !important;\r\n"
							+ "  line-height: 54px !important;\r\n" + "}\r\n" + ".mso .size-56,\r\n"
							+ ".ie .size-56 {\r\n" + "  font-size: 56px !important;\r\n"
							+ "  line-height: 60px !important;\r\n" + "}\r\n" + ".mso .size-64,\r\n"
							+ ".ie .size-64 {\r\n" + "  font-size: 64px !important;\r\n"
							+ "  line-height: 63px !important;\r\n" + "}\r\n" + "</style>\r\n" + "    \r\n"
							+ "  <style type=\"text/css\">\r\n"
							+ "body{background-color:#180b38}.logo a:hover,.logo a:focus{color:#fff !important}.mso .layout-has-border{border-top:1px solid #000;border-bottom:1px solid #000}.mso .layout-has-bottom-border{border-bottom:1px solid #000}.mso .border,.ie .border{background-color:#000}.mso h1,.ie h1{}.mso h1,.ie h1{font-size:64px !important;line-height:63px !important}.mso h2,.ie h2{}.mso h2,.ie h2{font-size:30px !important;line-height:38px !important}.mso h3,.ie h3{}.mso h3,.ie h3{font-size:22px !important;line-height:31px !important}.mso .layout__inner,.ie .layout__inner{}.mso .footer__share-button p{}.mso .footer__share-button p{font-family:sans-serif}\r\n"
							+ "</style><meta name=\"robots\" content=\"noindex,nofollow\" />\r\n"
							+ "<meta property=\"og:title\" content=\"My First Campaign\" />\r\n" + "</head>\r\n"
							+ "<!--[if mso]>\r\n" + "  <body class=\"mso\">\r\n" + "<![endif]-->\r\n"
							+ "<!--[if !mso]><!-->\r\n"
							+ "  <body class=\"no-padding\" style=\"margin: 0;padding: 0;-webkit-text-size-adjust: 100%;\">\r\n"
							+ "<!--<![endif]-->\r\n"
							+ "    <table class=\"wrapper\" style=\"border-collapse: collapse;table-layout: fixed;min-width: 320px;width: 100%;background-color: #180b38;\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"><tbody><tr><td>\r\n"
							+ "      <div role=\"banner\">\r\n"
							+ "        <div class=\"preheader\" style=\"Margin: 0 auto;max-width: 560px;min-width: 280px; width: 280px;width: calc(28000% - 167440px);\">\r\n"
							+ "          <div style=\"border-collapse: collapse;display: table;width: 100%;\">\r\n"
							+ "          <!--[if (mso)|(IE)]><table align=\"center\" class=\"preheader\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"><tr><td style=\"width: 280px\" valign=\"top\"><![endif]-->\r\n"
							+ "            <div class=\"snippet\" style=\"display: table-cell;Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 140px; width: 140px;width: calc(14000% - 78120px);padding: 10px 0 5px 0;color: #adb3b9;font-family: sans-serif;\">\r\n"
							+ "              \r\n" + "            </div>\r\n"
							+ "          <!--[if (mso)|(IE)]></td><td style=\"width: 280px\" valign=\"top\"><![endif]-->\r\n"
							+ "            <div class=\"webversion\" style=\"display: table-cell;Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 139px; width: 139px;width: calc(14100% - 78680px);padding: 10px 0 5px 0;text-align: right;color: #adb3b9;font-family: sans-serif;\">\r\n"
							+ "              \r\n" + "            </div>\r\n"
							+ "          <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\r\n"
							+ "          </div>\r\n" + "        </div>\r\n" + "        \r\n" + "      </div>\r\n"
							+ "      <div role=\"section\">\r\n"
							+ "      <div class=\"layout one-col fixed-width\" style=\"Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;\">\r\n"
							+ "        <div class=\"layout__inner\" style=\"border-collapse: collapse;display: table;width: 100%;background-color: #261454;background-position: 0px 0px;background-image: url(https://i1.createsend1.com/ei/j/A0/CDF/E74/212726/csfinal/3.jpg);background-repeat: no-repeat;\">\r\n"
							+ "        <!--[if (mso)|(IE)]><table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"><tr class=\"layout-fixed-width\" style=\"background-color: #261454;background-position: 0px 0px;background-image: url(https://i1.createsend1.com/ei/j/A0/CDF/E74/212726/csfinal/3.jpg);background-repeat: no-repeat;\"><td style=\"width: 600px\" class=\"w560\"><![endif]-->\r\n"
							+ "          <div class=\"column\" style=\"text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);\">\r\n"
							+ "        \r\n" + "            <div style=\"Margin-left: 20px;Margin-right: 20px;\">\r\n"
							+ "      <div style=\"mso-line-height-rule: exactly;line-height: 50px;font-size: 1px;\">&nbsp;</div>\r\n"
							+ "    </div>\r\n" + "        \r\n"
							+ "            <div style=\"Margin-left: 20px;Margin-right: 20px;\">\r\n"
							+ "        <div style=\"font-size: 12px;font-style: normal;font-weight: normal;line-height: 19px;Margin-bottom: 20px;\" align=\"center\">\r\n"
							+ "          <img style=\"border: 0;display: block;height: auto;width: 100%;max-width: 326px;\" alt=\"\" width=\"326\" src=\"http://oristocoin.com.com/Oristo_Wallet/resources/theme1/assets/images/emailler/Logo.png\" />\r\n"
							+ "        </div>\r\n" + "      </div>\r\n" + "        \r\n"
							+ "            <div style=\"Margin-left: 20px;Margin-right: 20px;\">\r\n"
							+ "      <div style=\"mso-line-height-rule: exactly;mso-text-raise: 4px;\">\r\n"
							+ "        <p class=\"size-26\" style=\"Margin-top: 0;Margin-bottom: 0;font-size: 22px;line-height: 31px;text-align: center;\" lang=\"x-size-26\">&nbsp;</p><p class=\"size-26\" style=\"Margin-top: 20px;Margin-bottom: 20px;font-size: 22px;line-height: 31px;text-align: center;\" lang=\"x-size-26\"><span style=\"color:#fcbe03\">WELCOME TO&nbsp;ORISTO</span></p>\r\n"
							+ "      </div>\r\n" + "    </div>\r\n" + "        \r\n"
							+ "            <div style=\"Margin-left: 20px;Margin-right: 20px;\">\r\n"
							+ "      <div style=\"mso-line-height-rule: exactly;line-height: 20px;font-size: 1px;\">&nbsp;</div>\r\n"
							+ "    </div>\r\n" + "        \r\n"
							+ "            <div style=\"Margin-left: 20px;Margin-right: 20px;\">\r\n"
							+ "      <div style=\"mso-line-height-rule: exactly;mso-text-raise: 4px;\">\r\n"
							+ "        <p class=\"size-18\" style=\"Margin-top: 0;Margin-bottom: 0;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-18\"><span class=\"font-century-gothic\"><span style=\"color:#ffffff\">Dear User,</span></span></p><p class=\"size-18\" style=\"Margin-top: 20px;Margin-bottom: 20px;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-18\"><span class=\"font-century-gothic\"><span style=\"color:#ffffff\">Please find below the required verification OTP!&nbsp;</span></span></p>\r\n"
							+ "      </div>\r\n" + "    </div>\r\n" + "        \r\n"
							+ "            <div style=\"Margin-left: 20px;Margin-right: 20px;\">\r\n"
							+ "      <div style=\"mso-line-height-rule: exactly;mso-text-raise: 4px;\">\r\n"
							+ "        <p class=\"size-18\" style=\"Margin-top: 0;Margin-bottom: 0;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-18\"><span class=\"font-century-gothic\">&nbsp;</span></p>\r\n"
							+ "		<p class=\"size-18\" style=\"Margin-top: 20px;Margin-bottom: 20px;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-18\"><span class=\"font-century-gothic\"><span style=\"color:#ffffff\">We have received a request from you to change password.</span></span></p>\r\n"
							+ "		<p class=\"size-18\" style=\"Margin-top: 20px;Margin-bottom: 20px;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-18\"><span class=\"font-century-gothic\"><span style=\"color:#ffffff\">You can securely login with your registered email id using your NEW password.</span></span></p>\r\n"
							+ "		<p class=\"size-18\" style=\"Margin-top: 20px;Margin-bottom: 20px;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-18\"><span class=\"font-century-gothic\"><span style=\"color:#fcbe03\">Password:"
							+ newpassword + "</span></span></p>\r\n"
							+ "		<p class=\"size-18\" style=\"Margin-top: 20px;Margin-bottom: 20px;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-18\"><span class=\"font-century-gothic\"><span style=\"color:#ffffff\">If you haven't initiated this request, kindly ignore the mail.</span></span></p>\r\n"
							+ "      </div>\r\n" + "    </div>\r\n" + "        \r\n"
							+ "            <div style=\"Margin-left: 20px;Margin-right: 20px;\">\r\n"
							+ "      <div style=\"mso-line-height-rule: exactly;mso-text-raise: 4px;\">\r\n"
							+ "        <p class=\"size-18\" style=\"Margin-top: 0;Margin-bottom: 0;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-18\"><span class=\"font-century-gothic\">&nbsp;</span></p><p class=\"size-18\" style=\"Margin-top: 20px;Margin-bottom: 20px;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-18\"><span class=\"font-century-gothic\"><span style=\"color:#ffffff\">Warm Regards,&nbsp;<br />\r\n"
							+ "Team </span><span style=\"color:#fcbe03\">ORISTO</span></span></p>\r\n"
							+ "      </div>\r\n" + "    </div>\r\n" + "        \r\n"
							+ "            <div style=\"Margin-left: 20px;Margin-right: 20px;\">\r\n"
							+ "      <div style=\"mso-line-height-rule: exactly;mso-text-raise: 4px;\">\r\n"
							+ "        <p class=\"size-14\" style=\"Margin-top: 0;Margin-bottom: 0;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 14px;line-height: 21px;\" lang=\"x-size-14\"><span class=\"font-century-gothic\">&nbsp;</span></p><p class=\"size-14\" style=\"Margin-top: 20px;Margin-bottom: 0;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 14px;line-height: 21px;\" lang=\"x-size-14\"><span class=\"font-century-gothic\"><span style=\"color:#fcbe03\">Let&#8217;s keep this safe between you and us, do not share your password with anyone.</span></span></p><p class=\"size-14\" style=\"Margin-top: 20px;Margin-bottom: 0;font-family: century gothic,heiti sc,stheiti,avenir,trebuchet ms,arial,sans-serif;font-size: 14px;line-height: 21px;\" lang=\"x-size-14\"><span class=\"font-century-gothic\">&nbsp;</span></p>\r\n"
							+ "      </div>\r\n" + "    </div>\r\n" + "        \r\n" + "          </div>\r\n"
							+ "        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\r\n" + "        </div>\r\n"
							+ "      </div>\r\n" + "  \r\n"
							+ "      <div style=\"mso-line-height-rule: exactly;line-height: 20px;font-size: 20px;\">&nbsp;</div>\r\n"
							+ "  \r\n" + "      \r\n"
							+ "      <div style=\"mso-line-height-rule: exactly;\" role=\"contentinfo\">\r\n"
							+ "        <div class=\"layout email-footer\" style=\"Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;\">\r\n"
							+ "          <div class=\"layout__inner\" style=\"border-collapse: collapse;display: table;width: 100%;\">\r\n"
							+ "          <!--[if (mso)|(IE)]><table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"><tr class=\"layout-email-footer\"><td style=\"width: 400px;\" valign=\"top\" class=\"w360\"><![endif]-->\r\n"
							+ "            <div class=\"column wide\" style=\"text-align: left;font-size: 12px;line-height: 19px;color: #adb3b9;font-family: sans-serif;Float: left;max-width: 400px;min-width: 320px; width: 320px;width: calc(8000% - 47600px);\">\r\n"
							+ "              <div style=\"Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;\">\r\n"
							+ "                \r\n"
							+ "                <div style=\"font-size: 12px;line-height: 19px;\">\r\n"
							+ "                  \r\n" + "                </div>\r\n"
							+ "                <div style=\"font-size: 12px;line-height: 19px;Margin-top: 18px;\">\r\n"
							+ "                  \r\n" + "                </div>\r\n"
							+ "                <!--[if mso]>&nbsp;<![endif]-->\r\n" + "              </div>\r\n"
							+ "            </div>\r\n"
							+ "          <!--[if (mso)|(IE)]></td><td style=\"width: 200px;\" valign=\"top\" class=\"w160\"><![endif]-->\r\n"
							+ "            <div class=\"column narrow\" style=\"text-align: left;font-size: 12px;line-height: 19px;color: #adb3b9;font-family: sans-serif;Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);\">\r\n"
							+ "              <div style=\"Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;\">\r\n"
							+ "                \r\n" + "              </div>\r\n" + "            </div>\r\n"
							+ "          <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\r\n"
							+ "          </div>\r\n" + "        </div>\r\n"
							+ "        <div class=\"layout one-col email-footer\" style=\"Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;\">\r\n"
							+ "          <div class=\"layout__inner\" style=\"border-collapse: collapse;display: table;width: 100%;\">\r\n"
							+ "          <!--[if (mso)|(IE)]><table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"><tr class=\"layout-email-footer\"><td style=\"width: 600px;\" class=\"w560\"><![endif]-->\r\n"
							+ "            <div class=\"column\" style=\"text-align: left;font-size: 12px;line-height: 19px;color: #adb3b9;font-family: sans-serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);\">\r\n"
							+ "              <div style=\"Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;\">\r\n"
							+ "                <!-- <div style=\"font-size: 12px;line-height: 19px;\"> -->\r\n"
							+ "                  <!-- <span><preferences style=\"text-decoration: underline;\" lang=\"en\">Preferences</preferences>&nbsp;&nbsp;|&nbsp;&nbsp;</span><unsubscribe style=\"text-decoration: underline;\">Unsubscribe</unsubscribe> -->\r\n"
							+ "                <!-- </div> -->\r\n" + "              </div>\r\n"
							+ "            </div>\r\n"
							+ "          <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\r\n"
							+ "          </div>\r\n" + "        </div>\r\n" + "      </div>\r\n"
							+ "      <div style=\"mso-line-height-rule: exactly;line-height: 40px;font-size: 40px;\">&nbsp;</div>\r\n"
							+ "    </div></td></tr></tbody></table>\r\n" + "  \r\n" + "</body></html>\r\n" + "");
			m.setEmail(mail);
			m.sendMail();

			User userdata1 = new User();
			userdata1 = users;
			String keygenerate = userdata1.getFirstName() + userdata1.getLastName() + userdata1.getEmailID()
					+ newpassword;
			// System.out.println("second_____)))))))))))))))__________"+newpassword);
			Encryption encryption = new Encryption(keygenerate);
			String password = encryption.encrypt(newpassword);

			String qryString3 = "update User u set u.password=:pass where u.emailID=:EId";
			Query query3 = session.createQuery(qryString3);
			query3.setParameter("EId", mail);
			query3.setParameter("pass", password);
			query3.executeUpdate();

			flag = 1;
		}
		return flag;
	}

	// change password using old password and email id
	@Override
	public int passwordchange(ChangePassword changepassword) {

		Session session = sessionFactory.getCurrentSession();
		int flag = 0;
		User user1 = new User();
		user1.setEmailID(changepassword.getEmailID());
		User userdata2 = new User();
		userdata2 = getUserdata(user1);
		String keygenerate1 = userdata2.getFirstName() + userdata2.getLastName() + userdata2.getEmailID()
				+ changepassword.getOld_password();
		Encryption encryption1 = new Encryption(keygenerate1);
		String password1 = encryption1.encrypt(changepassword.getOld_password());
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", changepassword.getEmailID()));
		criteria.add(Restrictions.eq("password", password1));
		User users = (User) criteria.uniqueResult();
		if (users != null) {
			String keygenerate2 = userdata2.getFirstName() + userdata2.getLastName() + userdata2.getEmailID()
					+ changepassword.getNew_password();
			Encryption encryption2 = new Encryption(keygenerate2);
			String password2 = encryption2.encrypt(changepassword.getNew_password());

			String qryString3 = "update User u set u.password=:pass where u.emailID=:EId";
			Query query3 = session.createQuery(qryString3);
			query3.setParameter("EId", changepassword.getEmailID());
			query3.setParameter("pass", password2);
			query3.executeUpdate();
			flag = 1;
		} else {
			System.out.println("old password is wrong plz try to forgot password");
		}
		return flag;
	}

	@Override
	public boolean checkUser(Login login, String uuid) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void updateUser(String emailID, String otp) {
		Session session = sessionFactory.getCurrentSession();
		String qryString3 = "update User u set u.otpcode=:pass where u.emailID=:EId";
		Query query3 = session.createQuery(qryString3);
		query3.setParameter("EId", emailID);

		AesCrypto aescry = new AesCrypto();
		String encryptotp = aescry.encrypt(otp);
		query3.setParameter("pass", encryptotp);

		query3.executeUpdate();

	}

	@Override
	public void updateAdmin(String emailID, String otp) {
		Session session = sessionFactory.getCurrentSession();
		String qryString3 = "update Admin u set u.otpcode=:pass where u.emailID=:EId";
		Query query3 = session.createQuery(qryString3);
		query3.setParameter("EId", emailID);

		AesCrypto aescry = new AesCrypto();
		String encryptotp = aescry.encrypt(otp);
		query3.setParameter("pass", encryptotp);

		query3.executeUpdate();

	}

	@Override
	public void checkUserActive(String uuid) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("uuid", uuid));
		User users = (User) criteria.uniqueResult();
		if (users != null) {
			users.setActive(true);
			users.setUuid("null");
			session.saveOrUpdate(users);
		} else {
			Criteria criteria1 = session.createCriteria(Admin.class);
			criteria1.add(Restrictions.eq("uuid", uuid));
			Admin admin = (Admin) criteria1.uniqueResult();
			if (admin != null) {
				admin.setActive(true);
				admin.setUuid("null");
				session.saveOrUpdate(admin);
			}

		}
	}

	/*
	 * @Override public int saveproduct(Product product) { Session session =
	 * sessionFactory.getCurrentSession(); session.save(product); return 1; }
	 */

	/*
	 * @Override public int savestudentdetails(Student student) { int flag = 0;
	 * Session session = sessionFactory.getCurrentSession(); session.save(student);
	 * flag = 1; return flag; }
	 * 
	 * @Override public ArrayList<Student> getstudent() { Session session =
	 * sessionFactory.getCurrentSession(); return (ArrayList<Student>)
	 * session.createQuery("from Student").list(); }
	 * 
	 * @Override public ArrayList<Product> getproduct() { Session session =
	 * sessionFactory.getCurrentSession(); return (ArrayList<Product>)
	 * session.createQuery("from Product").list(); }
	 */

	@Override
	public Admin getUserdata2(Admin admin) {
		Session session = sessionFactory.getCurrentSession();
		Admin admindata = new Admin();

		Criteria criteria = session.createCriteria(Admin.class);
		criteria.add(Restrictions.eq("emailID", admin.getEmailID()));
		criteria.add(Restrictions.eq("isActive", true));
		Admin admins = (Admin) criteria.uniqueResult();
		if (admin != null) {
			admindata = admins;
		} else {
			admindata = admins;
		}
		return admindata;
	}

	@Override
	public Admin getUserdata12(Admin admin) {
		Session session = sessionFactory.getCurrentSession();
		Admin admindata = new Admin();

		Criteria criteria = session.createCriteria(Admin.class);
		criteria.add(Restrictions.eq("phone", admin.getEmailID()));
		criteria.add(Restrictions.eq("isActive", true));
		Admin admins = (Admin) criteria.uniqueResult();
		if (admin != null) {
			admindata = admins;
		} else {
			admindata = admins;
		}
		return admindata;
	}

}
