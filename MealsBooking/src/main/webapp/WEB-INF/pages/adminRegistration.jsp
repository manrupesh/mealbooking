<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16"
	href="resources/restorant/images/favicon.ico">
<title></title>
<!-- Bootstrap Core CSS -->
<link
	href="resources/login/assets/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="resources/login/css/style.css" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="resources/login/css/colors/blue.css" id="theme"
	rel="stylesheet">
<script src="resources/myJS/bootstrap.min.js"></script>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>
<script src="resources/myJS/jquery-ui.js"></script>
<script src="resources/myJS/custom.js"></script>
<script src="resources/myJS/jquery.mark.min.js"></script>
<script src="resources/myJS/jquery.calculation.js"></script>
<script src="resources/myJS/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	var sheight = window.screen.availHeight - 183;
	var swidth = window.screen.availWidth - 800;

	// var cookieValue = document.getElementById("selected-dial-code").value;    
	// alert(cookieValue);
	//alert(swidth);

	$(document).ready(function() {
		/* var slf=$(".selected-dial-code").text();
		alert(slf)
		$("#phone").keyup(
								function() {
		var slf=$(".selected-dial-code").text();
		alert(slf)
		});
		$(".selected-dial-code").keyup(
				function() {
		var slf=$(".selected-dial-code").text();
		alert(slf)
		}); */

	});
	var app = angular.module('myapp', []);
	app.controller('mycontroller', function($scope, $http, $window) {
	});

	function checkLoginForm(form) {
		var countrycode = $(".selected-dial-code").text();
		var mobnumber = $("#phone").val();
		var codewithphone = countrycode.concat(mobnumber);
		$("#phone").val(codewithphone);
		//alert(codewithphone)

		usn = document.getElementById("firstName");
		lastName = document.getElementById("lastName");
		email = document.getElementById("email");
		pwd = document.getElementById("password-field1");
		cpwd = document.getElementById("password-field2");
		var terms = document.getElementById("checkbox-signup");

		if (pwd.value != cpwd.value) {
			$("#phone").val(mobnumber);
			alert("Enter Password and conform password is same");
			cpwd.focus();
			return false;
		}
		if (usn.value == "") {
			$("#phone").val(mobnumber);
			alert("Enter First Name");
			usn.focus();
			return false;
		}
		if (lastName.value == "") {
			$("#phone").val(mobnumber);
			alert("Enter Last Name");
			lastName.focus();
			return false;
		}
		if (email.value == "") {
			$("#phone").val(mobnumber);
			alert("Enter Email Name");
			email.focus();
			return false;
		}

		var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
		if (!re.test(pwd.value)) {
			$("#phone").val(mobnumber);
			alert("Password > 8 characters it must contain at least one number (0-9),at least one lowercase letter (a-z),at least one special character, at least one uppercase letter (A-Z)");
			pwd.focus();
			return false;
		}
		if (!terms.checked) {
			$("#phone").val(mobnumber);
			alert('Please signify your agreement with our terms.');
			terms.focus();
			return false;
		}
		var phoneno = /^\d{10}$/;
		if (mobnumber.match(phoneno)) {
			return true;
		} else {
			$("#phone").val(mobnumber);
			alert("check your mob m=number");
			return false;
		}
		return true;
	}
</script>
</head>

<body ng-app="myapp" ng-controller="mycontroller">

	<font color="red" size="4">${status}</font>
	<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50"> <circle
			class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2"
			stroke-miterlimit="10" /> </svg>
	</div>

	<section id="wrapper">
	<div class="login-register"
		style="background-image: url(resources/login/assets/images/background/login.jpg);">
		<div class="offset-md-7 col-md-5">
			<div class="login-box">
				<img src="resources/login/assets/images/logo-icon.png"
					alt="homepage" class="dark-logo logoloin" />
			</div>
		</div>
		<div class="offset-md-7 col-md-4 card1">
			<div class="card-body">
				<form:form class="form-horizontal form-material" id="loginform"
					name="signIn" action="saveAdmin" method="post"
					modelAttribute="user" onsubmit='return checkLoginForm(this);'>
					<h3 class="box-title m-b-20">Sign Up</h3>
					<div class="form-group ">
						<div class="col-xs-12">
							<form:input class="form-control lab" path="firstName" type="text"
								id="firstName" name="firstName" placeholder="First Name"
								required="" />
						</div>
					</div>
					<div class="form-group ">
						<div class="col-xs-12">
							<form:input class="form-control lab" path="lastName" type="text"
								id="lastName" name="lastName" placeholder="Last Name"
								required="" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-12">
							<form:input class="form-control lab" required=""
								placeholder="Email-ID" path="emailID" type="email" id="email"
								name="email" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<form:input class="form-control lab" required="" id="phone"
								path="phone" placeholder="Moble No" type="text" name="phone" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<form:input id="password-field1" type="password"
								class="form-control lab" name="password" path="password"
								placeholder="New Password" />
							<span toggle="#password-field1"
								class="fa fa-fw fa-eye field-icon toggle-password"></span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input id="password-field2" type="password"
								class="form-control lab" name="conformpassword"
								placeholder="Confirm Password"> <span
								toggle="#password-field2"
								class="fa fa-fw fa-eye field-icon toggle-password"></span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<form:input class="form-control lab" path="referencecode"
								type="text" id="referencecode" name="referencecode"
								placeholder="Referral code" required=""
								value='<%=request.getParameter("referencecode")%>' />
						</div>
					</div>
					<div class="form-group row">

						<div class="col-md-12 font-14">
							<div class="checkbox checkbox-primary pull-left p-t-40">
								<input id="checkbox-signup" type="checkbox" class="pull-left">
								<label for="checkbox-signup" required> I accept the
									privacy policy,terms & conditions</label><br>
							</div>
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button type="submit"
								class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
								style="padding-bottom: 41px;">Sign Me Up</button>
						</div>
					</div>
					<div class="form-group m-b-0">
						<div class="col-sm-12 text-center">
							<div>
								Already have an account? <a href="login"
									class="text-info m-l-5"><b>Login</b></a>
							</div>
						</div>

					</div>
				</form:form>
			</div>
		</div>
	</div>
	</section>

	<script src="resources/login/assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap tether Core JavaScript -->
	<script src="resources/login/assets/plugins/bootstrap/js/popper.min.js"></script>
	<script
		src="resources/login/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- slimscrollbar scrollbar JavaScript -->
	<script src="resources/login/js/jquery.slimscroll.js"></script>
	<!--Wave Effects -->
	<script src="resources/login/js/waves.js"></script>
	<!--Menu sidebar -->
	<script src="resources/login/js/sidebarmenu.js"></script>
	<!--stickey kit -->
	<script
		src="resources/login/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
	<script
		src="resources/login/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!--Custom JavaScript -->
	<script src="resources/login/js/custom.min.js"></script>
	<script
		src="resources/login/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
	<script>
		$(".toggle-password").click(function() {

			$(this).toggleClass("fa-eye fa-eye-slash");
			var input = $($(this).attr("toggle"));
			if (input.attr("type") == "password") {
				input.attr("type", "text");
			} else {
				input.attr("type", "password");
			}
		});
	</script>
</body>

</html>