package com.biz.adminmodel;

import org.springframework.web.multipart.MultipartFile;

import com.biz.adminentity.Admin;
import com.biz.adminentity.Product;

public class ProductInfo {
	private int product_id;
	private String name;
	private String description;
	private String category;
	private String product_type;
	private String availability;
	private String product_size;
	private double discount_per;
	private String startDate;
	private String endDate;
	private double price;
	private boolean newProduct = false;
	private MultipartFile file;
	private int quantity;
	private String img;
	private int total_star;
	private String favourit;

	public ProductInfo() {
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getProduct_size() {
		return product_size;
	}

	public void setProduct_size(String product_size) {
		this.product_size = product_size;
	}

	public double getDiscount_per() {
		return discount_per;
	}

	public void setDiscount_per(double discount_per) {
		this.discount_per = discount_per;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isNewProduct() {
		return newProduct;
	}

	public void setNewProduct(boolean newProduct) {
		this.newProduct = newProduct;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getTotal_star() {
		return total_star;
	}

	public void setTotal_star(int total_star) {
		this.total_star = total_star;
	}

	public String getFavourit() {
		return favourit;
	}

	public void setFavourit(String favourit) {
		this.favourit = favourit;
	}

	public String getProduct_type() {
		return product_type;
	}

	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}
	
}