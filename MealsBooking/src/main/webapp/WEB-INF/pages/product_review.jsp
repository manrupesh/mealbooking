<%@page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
<script src="resources/myJS/bootstrap.min.js"></script>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>
<script src="resources/myJS/jquery-ui.js"></script>
<script src="resources/myJS/custom.js"></script>
<script src="resources/myJS/jquery.mark.min.js"></script>
<script src="resources/myJS/jquery.calculation.js"></script>
<script src="resources/myJS/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery.dataTables.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- <script
	src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.5/angular.min.js"></script> -->
<script type="text/javascript">
	var app = angular.module('myapp', []);
	app.controller('myappcontroller', function($scope, $http, $compile) {
		
		$http({
			method : 'POST',
			url : 'getCartCount'
		}).then(function successCallback(response) {
			$scope.size = response.data;
		}, function errorCallback(response) {
			console.log(response.statusText);
		});

		$scope.myVar = 'all';

		$scope.product_id = 0;
		$scope.addtocart = function(id) {
			var bvalue = id;
			$.ajax({
				type : 'POST',
				url : "addtomycart",
				data : {
					product_id : bvalue
				},
				success : function(data) {
					if (data == "userdashboard") {
						window.location.href = "login";
					} else {
						if (data != "Already in your cart!") {
							$http({
								method : 'POST',
								url : 'getCartCount'
							}).then(function successCallback(response) {
								$scope.size = response.data;
							}, function errorCallback(response) {
								console.log(response.statusText);
							});
						}
						alert(data);
					}
				}
			});
		}
		$scope.ratting = function(star, product_id) {
			var product_id = product_id;
			var star = star;
			$.ajax({
				type : 'POST',
				url : "ratting",
				data : {
					product_id : product_id,
					star : star
				},
				success : function(data) {
					if (data == "userdashboard") {
						window.location.href = "login";
					} else {
						if (data != "Already in your cart!") {
							location.reload(true);
						}
						alert(data);
					}

				}
			});
		}

		$scope.favourite = function(product_id) {
			var product_id = product_id;
			$.ajax({
				type : 'POST',
				url : "savefavourite",
				data : {
					product_id : product_id,
				},
				success : function(data) {
					if (data == "userdashboard") {
						window.location.href = "login";
					} else {
						if (data != "Already in your cart!") {
							location.reload(true);
						}
						alert(data);
					}
				}
			});
		}

	});
</script>
<style>
mark {
	padding: 0px;
	background-color: #f1c40f;
}

.checked {
	color: orange;
}
</style>

<meta charset="UTF-8">
<title>Lambert - Responsive Pub / Restaurant Template</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="robots" content="index, follow" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<!--=============== css  ===============-->
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/reset.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/plugins.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/style.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/color.css">
<!--=============== favicons ===============-->
<link rel="shortcut icon" href="resources/restorant/images/favicon.ico">
</head>
<body ng-app="myapp" ng-controller="myappcontroller">
	<div class="loader">
		<img src="resources/restorant/images/loader.png" alt="">
	</div>
	<!--================= main start ================-->
	<div id="main">
		<!--=============== header ===============-->
		<header class="flat-header">
		<div class="header-inner">
			<div class="container">
				<!--navigation social links-->
				<div class="nav-social">
					<ul>
						<li><a href="" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-instagram"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-tumblr"></i></a></li>
					</ul>
				</div>
				<!--logo-->
				<div class="logo-holder">
					<a href="index.html"> <img
						src="resources/restorant/images/logo2.png" class="respimg" alt="">
					</a>
				</div>
				<!--Navigation -->
				<sec:authorize access="hasAuthority('AUTH_USER')">
					<div class="subnav">
						<a href="showcart"><i class="fa fa-shopping-cart"></i>( <span>{{size}}</span>
							)</a>
					</div>
				</sec:authorize>
				<div class="nav-holder">
					<nav>
					<ul>
						<li><a href="userdashboard">Home</a></li>
						<li><a href="viewmenulist">Menu</a></li>
						<li><a href="viewgallery">Gallery</a></li>
						<li><a href="getproductlist">Shop</a></li>
						<li><a href="viewcontact">Contact</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a href="viewmyorders">My Orders</a></li>
						</sec:authorize>
						<li class="hideli"><a href="userRegistration">Sign Up</a></li>
						<li class="hideli"><a href="login">Sign In</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a class="has-arrow waves-effect waves-dark"
								href="<c:url value="/logout"/>" aria-expanded="false"> <i
									class="mdi mdi-login-variant"></i> Logout
							</a></li>
							<script type="text/javascript">
								$(".hideli").hide();
							</script>
						</sec:authorize>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		</header>
		<!--header end-->
		<!--=============== wrapper ===============-->
		<div id="wrapper">
			<div class="content">
				<section>
				<div class="container">
					<div class="product-meta">
						<span class="posted_in">Category : <a href="">Other</a> , <a
							href="">Sale</a></span>
					</div>
					<div class="product-item-holder">
						<div class="row">
							<div class="col-md-6">
								<div class="product-image">
									<div class="owl-carousel product-slider">
										<div>
											<img src="data:image/png;base64,${prodInfo.img}" alt=""
												class="respimg">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="product-item">
									<h2 class="product_title">${prodInfo.name}</h2>
									<div class="pr-opt">
										<div class="product-item-price">
											<c:if
												test="${prodInfo.price>(prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100)}">
												<span>Rs.&nbsp;${prodInfo.price}</span>
											</c:if>
											Rs.&nbsp;${prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100}
										</div>
										<div class="product-rating1">
											<ul>
												<c:if test="${prodInfo.total_star < 1}">
													<li><span ng-click="ratting(1,${prodInfo.product_id})"
														class="fa fa-star"></span> <span
														ng-click="ratting(2,${prodInfo.product_id})"
														class="fa fa-star"></span> <span
														ng-click="ratting(3,${prodInfo.product_id})"
														class="fa fa-star"></span> <span
														ng-click="ratting(4,${prodInfo.product_id})"
														class="fa fa-star"></span> <span
														ng-click="ratting(5,${prodInfo.product_id})"
														class="fa fa-star"></span></li>
												</c:if>
												<c:if test="${prodInfo.total_star == 1}">
													<li><span ng-click="ratting(1,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(2,${prodInfo.product_id})"
														class="fa fa-star"></span> <span
														ng-click="ratting(3,${prodInfo.product_id})"
														class="fa fa-star"></span> <span
														ng-click="ratting(4,${prodInfo.product_id})"
														class="fa fa-star"></span> <span
														ng-click="ratting(5,${prodInfo.product_id})"
														class="fa fa-star"></span></li>
												</c:if>
												<c:if test="${prodInfo.total_star == 2}">
													<li><span ng-click="ratting(1,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(2,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(3,${prodInfo.product_id})"
														class="fa fa-star"></span> <span
														ng-click="ratting(4,${prodInfo.product_id})"
														class="fa fa-star"></span> <span
														ng-click="ratting(5,${prodInfo.product_id})"
														class="fa fa-star"></span></li>
												</c:if>
												<c:if test="${prodInfo.total_star == 3}">
													<li><span ng-click="ratting(1,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(2,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(3,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(4,${prodInfo.product_id})"
														class="fa fa-star"></span> <span
														ng-click="ratting(5,${prodInfo.product_id})"
														class="fa fa-star"></span></li>
												</c:if>
												<c:if test="${prodInfo.total_star == 4}">
													<li><span ng-click="ratting(1,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(2,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(3,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(4,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(5,${prodInfo.product_id})"
														class="fa fa-star"></span></li>
												</c:if>
												<c:if test="${prodInfo.total_star == 5}">
													<li><span ng-click="ratting(1,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(2,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(3,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(4,${prodInfo.product_id})"
														class="fa fa-star checked"></span> <span
														ng-click="ratting(5,${prodInfo.product_id})"
														class="fa fa-star checked"></span></li>
												</c:if>
												<c:if test="${prodInfo.favourit =='false'}">
													<li><button
															ng-click="favourite(${prodInfo.product_id})">
															<i class="heart fa fa-heart-o"></i>
														</button></li>
												</c:if>
												<c:if test="${prodInfo.favourit =='true'}">
													<li><button
															ng-click="favourite(${prodInfo.product_id})">
															<i class="heart fa fa-heart checked"></i>
														</button></li>
												</c:if>
											</ul>
										</div>
									</div>
									<div id="tabs-container">
										<ul class="tabs-menu">
											<li class="current"><a href="#tab-1">Description</a></li>
											<c:set var="total" value="${0}" />
											<c:forEach var="prodInfo" items="${reviewslist}">
												<c:set var="total" value="${total+1}" />
											</c:forEach>
											<li><a href="#tab-2">Reviews (${total})</a></li>
										</ul>
										<div class="tab">
											<div id="tab-1" class="tab-content">
												<p>${prodInfo.description}</p>
											</div>
											<div id="tab-2" class="tab-content">
												<div id="comments">
													<!--title-->
													<h6 id="comments-title">
														Comments <span></span>
													</h6>
													<ul class="commentlist clearafix">
														<c:forEach var="reviewslist" items="${reviewslist}">
															<li class="comment">
																<div class="comment-body">
																	<div class="comment-author">
																		<img alt=''
																			src='resources/restorant/images/blog/users/1.jpg'
																			width="50" height="50">
																	</div>
																	<cite class="fn"><a href="">${reviewslist.firstName}&nbsp;&nbsp;${reviewslist.lastName}</a></cite>
																	<div class="comment-meta">
																		<h6>
																			<a href="">${reviewslist.review_date}atam</a> / <a
																				class='comment-reply-link' href="#">Reply</a>
																		</h6>
																	</div>
																	<p>${reviewslist.review}</p>
																</div>
															</li>
														</c:forEach>
													</ul>
													<div class="clearfix"></div>
													<sec:authorize access="hasAuthority('AUTH_USER')">
														<div id="respond" class="clearafix">
															<h6 id="reply-title">Leave A Comment</h6>
															<div class="comment-reply-form clearfix">
																<form:form action="savereview" method="post"
																	modelAttribute="reviews"
																	onsubmit='return checkLoginForm(this);'>
																	<form:input path="product_id" type="hidden"
																		value="${prodInfo.product_id}" required="" />
																	<form:textarea path="review" rows="4" cols="50"
																		placeholder="enter your review"></form:textarea>
																	<button type="submit">Submit Review</button>
																</form:form>
																<%-- <form action="#" method="post" id="commentform"
																class="form-horizontal" name="commentform">
																<div class="comment-form-comment control-group">
																	<div class="controls">
																		<textarea id="comment" name="comment" cols="50"
																			rows="8" aria-required="true"
																			placeholder="Your comment here..">
                                                                                </textarea>
																	</div>
																</div>
																<div class="form-submit">
																	<div class="controls">
																		<button class="transition button" type="submit">Post
																			Comment</button>
																		<input type='hidden' name='comment_post_ID' value='30'
																			id='comment_post_ID'> <input type='hidden'
																			name='comment_parent' id='comment_parent' value='0' />
																	</div>
																</div>
															</form> --%>
															</div>
														</div>
													</sec:authorize>
													<!--end respond-->
												</div>
											</div>
										</div>
									</div>
									<div class="quantity">
										<a href="" ng-click="addtocart(${prodInfo.product_id})">Add
											to Cart</a>
									</div>
								</div>
							</div>
							<div class="bold-separator">
								<span></span>
							</div>


						</div>
					</div>
				</div>
				</section>
			</div>
			<!--=============== footer ===============-->
			<footer>
			<div class="footer-inner">
				<div class="container">
					<div class="row">
						<!--tiwtter-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Our twitter</h4>
								<div class="twitter-holder">
									<div class="twitts">
										<div class="twitter-feed">
											<!-- Twitter Slider -->
											<div id="twitter-feed"></div>
										</div>
									</div>
									<div class="customNavigation">
										<a class="prev-slide transition"><i
											class="fa fa-long-arrow-left"></i></a> <a
											class="twit-link transition"
											href="https://twitter.com/katokli3mmm" target="_blank"><i
											class="fa fa-twitter"></i></a> <a class="next-slide transition"><i
											class="fa fa-long-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<!--footer social links-->
						<div class="col-md-4">
							<div class="footer-social">
								<h3>Find us</h3>
								<ul>
									<li><a href="" target="_blank"><i
											class="fa fa-facebook"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-twitter"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-instagram"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-pinterest"></i></a></li>
									<li><a href="" target="_blank"><i class="fa fa-tumblr"></i></a></li>
								</ul>
							</div>
						</div>
						<!--subscribe form-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Newsletter</h4>
								<div class="subcribe-form">
									<form id="subscribe">
										<input class="enteremail" name="email" id="subscribe-email"
											placeholder="Your email address.." spellcheck="false"
											type="text">
										<button type="submit" id="subscribe-button"
											class="subscribe-button">
											<i class="fa fa-envelope"></i>
										</button>
										<label for="subscribe-email" class="subscribe-message"></label>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
					<!--footer contacts links -->
					<ul class="footer-contacts">
						<li><a href="">+7(111)123456789</a></li>
						<li><a href="">27th Brooklyn New York, NY 10065</a></li>
						<li><a href="">yourmail@domain.com</a></li>
					</ul>
				</div>
			</div>
			<!--to top / privacy policy-->
			<div class="to-top-holder">
				<div class="container">
					<p>
						<span> &#169; Lambert 2015 . </span> All rights reserved.
					</p>
					<div class="to-top">
						<span>Back To Top </span><i class="fa fa-angle-double-up"></i>
					</div>
				</div>
			</div>
			</footer>
			<!--footer end -->
		</div>
		<!-- wrapper end -->
	</div>
	<!-- Main end -->
	<!--=============== google map ===============-->
	<script type="text/javascript"
		src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<!--=============== scripts  ===============-->
	<script type="text/javascript"
		src="resources/restorant/js/jquery.min.js"></script>
	<script type="text/javascript" src="resources/restorant/js/plugins.js"></script>
	<script type="text/javascript" src="resources/restorant/js/scripts.js"></script>
</body>

<!-- Mirrored from lambert.kwst.net/site/product.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 01 Feb 2019 10:49:25 GMT -->
</html>