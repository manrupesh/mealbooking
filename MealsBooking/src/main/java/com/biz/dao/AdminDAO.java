package com.biz.dao;

import java.util.ArrayList;
import java.util.List;

import com.biz.adminentity.Admin;
import com.biz.adminentity.Product;
import com.biz.adminmodel.ProductInfo;

public interface AdminDAO {

	int save(Product product);

	Admin addAdmin(Admin admin);

	List<Product> getproduct();

	ProductInfo findProductInfo(int product_id);

	Product findProduct(int product_id);

}
