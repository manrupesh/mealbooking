<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="icon" type="image/png" sizes="16x16"
	href="resources/horizontal/assets/images/favicon.png">
<title>Page Not Found</title>
</head>
<body>
	<img alt="" src="resources/theme1/assets/images/404.jpg">
</body>
</html>