package com.biz.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import com.biz.adminentity.MyCard;
import com.biz.adminentity.Order_Details;
import com.biz.adminentity.Orders;

@Entity
@Table(name = "userlist")
public class User {

	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name = "uid")
	private int uid;
	private String firstName;
	private String lastName;
	private String emailID;
	private String password;
	private boolean isActive;
	private boolean isDelete;
	private String uuid;
	private String otpcode;
	private String referencecode;
	private int referencecodeuserid;
	private String phone;
	private double referral_income;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "uid")
	Set<Orders> orders;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "uid")
	Set<MyCard> mycard;

	/*
	 * @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy =
	 * "uid") Set<Orders> order;
	 */

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getOtpcode() {
		return otpcode;
	}

	public void setOtpcode(String otpcode) {
		this.otpcode = otpcode;
	}

	public String getReferencecode() {
		return referencecode;
	}

	public void setReferencecode(String referencecode) {
		this.referencecode = referencecode;
	}

	public int getReferencecodeuserid() {
		return referencecodeuserid;
	}

	public void setReferencecodeuserid(int referencecodeuserid) {
		this.referencecodeuserid = referencecodeuserid;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public double getReferral_income() {
		return referral_income;
	}

	public void setReferral_income(double referral_income) {
		this.referral_income = referral_income;
	}

	public Set<MyCard> getMycard() {
		return mycard;
	}

	public void setMycard(Set<MyCard> mycard) {
		this.mycard = mycard;
	}

	public Set<Orders> getOrders() {
		return orders;
	}

	public void setOrders(Set<Orders> orders) {
		this.orders = orders;
	}

}
