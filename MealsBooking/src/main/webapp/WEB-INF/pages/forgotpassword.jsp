<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16"
	href="resources/restorant/images/favicon.ico">
<title></title>
<!-- Bootstrap Core CSS -->
<link
	href="resources/login/assets/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="resources/login/css/style.css" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="resources/login/css/colors/blue.css" id="theme"
	rel="stylesheet">
<!-- Tell the browser to be responsive to screen width -->
<script src="resources/myJS/bootstrap.min.js"></script>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>
<script src="resources/myJS/jquery-ui.js"></script>
<script src="resources/myJS/custom.js"></script>
<script src="resources/myJS/jquery.mark.min.js"></script>
<script src="resources/myJS/jquery.calculation.js"></script>
<script src="resources/myJS/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	var sheight = window.screen.availHeight - 183;
	var swidth = window.screen.availWidth - 800;
	$(document).ready(function() {

	});
	var app = angular.module('myapp', []);
	app.controller('mycontroller', function($scope, $http, $compile) {
		angular.element(document).ready(function() {
		});

	});

	function checkLoginForm(form) {
		email = document.getElementById("email");

		if (email.value == "") {
			alert("Enter UserID");
			email.focus();
			return false;
		}
		return true;
	}
</script>
</head>

<body>
	<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50"> <circle
			class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2"
			stroke-miterlimit="10" /> </svg>
	</div>

	<section id="wrapper">
	<div class="login-register"
		style="background-image: url(resources/login/assets/images/background/login.jpg);">
		<div class="offset-md-6 col-md-6">
			<div class="login-box text-center">
				<img src="resources/login/assets/images/logo-icon.png" alt="homepage"
					class="dark-logo logoloin" />
			</div>
		</div>
		<div class="offset-md-7 col-md-4 card1">
			<div class="card-body">
				<form:form class="form-horizontal" name="reset"
					action="editpassword" method="post" modelAttribute="login"
					onsubmit='return checkLoginForm(this);'>
					<h3 class="box-title m-b-20">Forgot Password</h3>
					<h5>Enter your Email and instructions will be sent to you!</h5>
					<!-- <div class="form-group ">
						<div class="col-xs-12">
							<p class="text-light">Enter your Email and instructions will
								be sent to you!</p>
						</div>
					</div> -->
					<div class="form-group ">
						<div class="col-xs-12">
							<form:input class="form-control" type="text" required=""
								placeholder="Email" name="email" id="email" path="emailID" />
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button type="submit"
								class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light">Reset</button>
						</div>
					</div>
				</form:form>

			</div>
		</div>
	</div>
	</section>

	<script src="resources/login/assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap tether Core JavaScript -->
	<script src="resources/login/assets/plugins/bootstrap/js/popper.min.js"></script>
	<script src="resources/login/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- slimscrollbar scrollbar JavaScript -->
	<script src="resources/login/js/jquery.slimscroll.js"></script>
	<!--Wave Effects -->
	<script src="resources/login/js/waves.js"></script>
	<!--Menu sidebar -->
	<script src="resources/login/js/sidebarmenu.js"></script>
	<!--stickey kit -->
	<script src="resources/login/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
	<script src="resources/login/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!--Custom JavaScript -->
	<script src="resources/login/js/custom.min.js"></script>
	<!-- ============================================================== -->
	<!-- Style switcher -->
	<!-- ============================================================== -->
	<script src="resources/login/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>