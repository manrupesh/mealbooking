package com.biz.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import com.biz.Mailling.Authenticate;
import com.biz.Mailling.Mailing;
import com.biz.adminentity.MyCard;
import com.biz.adminentity.Order_Details;
import com.biz.adminentity.Orders;
import com.biz.adminentity.Product;
import com.biz.adminentity.Product_Favourite;
import com.biz.adminentity.Product_Ratting;
import com.biz.adminentity.Product_Reviews;
import com.biz.adminentity.Table_Book_Details;
import com.biz.adminmodel.GetProductCartTotal;
import com.biz.adminmodel.Reviews;
import com.biz.model.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Product findProduct(int product_id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Product.class);
		crit.add(Restrictions.eq("product_id", product_id));
		return (Product) crit.uniqueResult();
	}

	@Override
	public String addtocart(int product_id) {
		Session session = sessionFactory.getCurrentSession();
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String result = null;
		Criteria criteria1 = session.createCriteria(User.class);
		criteria1.add(Restrictions.eq("emailID", name));
		User users = (User) criteria1.uniqueResult();
		if (users != null) {
			Criteria criteria = session.createCriteria(MyCard.class);
			criteria.add(Restrictions.eq("product_id", product_id));
			criteria.add(Restrictions.eq("uid", users));
			MyCard mycart = (MyCard) criteria.uniqueResult();
			if (mycart == null) {
				Criteria crit = session.createCriteria(Product.class);
				crit.add(Restrictions.eq("product_id", product_id));
				Product product = (Product) crit.uniqueResult();
				if (product != null) {
					MyCard addtocart = new MyCard();
					addtocart.setProduct_id(product_id);
					addtocart.setQuantity(1);
					addtocart.setUid(users);
					session.save(addtocart);
					result = "Successfully addd in your cart!";
				} else {
					result = "temparary this product is not available!";
				}
			} else {
				result = "Already in your cart!";
			}
		} else {
			result = "You are not user!";
		}
		return result;
	}

	@Override
	public List<Product> getcart() {
		Session session = sessionFactory.getCurrentSession();
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();
		List<Product> pro = new ArrayList<Product>();
		if (users != null) {
			if (users != null) {
				User user = new User();
				user.setUid(users.getUid());
				Criteria crit = session.createCriteria(MyCard.class);
				crit.add(Restrictions.eq("uid", user));
				List<MyCard> mycart = crit.list();
				for (MyCard myCard : mycart) {
					int product_id = myCard.getProduct_id();
					Criteria crite = session.createCriteria(Product.class);
					crite.add(Restrictions.eq("product_id", product_id));
					Product product1 = (Product) crite.uniqueResult();
					product1.setQuantity(myCard.getQuantity());
					pro.add(product1);
				}
			}
		}
		return pro;
	}

	@Override
	public String rmvtocart(int product_id) {
		Session session = sessionFactory.getCurrentSession();
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String result = null;
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();
		if (users != null) {
			User user = new User();
			user.setUid(users.getUid());
			if (users != null) {
				Criteria criter = session.createCriteria(MyCard.class);
				criter.add(Restrictions.eq("uid", user));
				criter.add(Restrictions.eq("product_id", product_id));
				MyCard mycart = (MyCard) criter.uniqueResult();
				if (mycart != null) {
					session.delete(mycart);
					result = "Successfully removed";
				} else {
					result = "Somthing is wrong";
				}
			}
		} else {
			result = "you are not user";
		}

		return result;
	}

	@Override
	public String ratting(int product_id, int star) {
		Session session = sessionFactory.getCurrentSession();
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String result = null;
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();
		User user = new User();
		user.setUid(users.getUid());
		if (users != null) {
			Criteria criter = session.createCriteria(Product.class);
			criter.add(Restrictions.eq("product_id", product_id));
			Product product = (Product) criter.uniqueResult();
			Product pdt = new Product();
			pdt.setProduct_id(product_id);
			if (product != null) {
				Criteria crit = session.createCriteria(Product_Ratting.class);
				crit.add(Restrictions.eq("product_id", pdt));
				crit.add(Restrictions.eq("uid", users.getUid()));
				Product_Ratting pratting = (Product_Ratting) crit.uniqueResult();
				if (pratting == null) {
					Product_Ratting ratting = new Product_Ratting();
					ratting.setProduct_id(pdt);
					ratting.setStar(star);
					ratting.setUid(users.getUid());
					session.save(ratting);
					result = "Successfully";
				} else {
					pratting.setStar(star);
					session.saveOrUpdate(pratting);
					result = "Successfully Update";
				}
			}
		}
		return result;
	}

	@Override
	public void updateratting(int product_id) {
		Session session = sessionFactory.getCurrentSession();
		Product product = new Product();
		product.setProduct_id(product_id);
		Criteria crit = session.createCriteria(Product_Ratting.class);
		crit.add(Restrictions.eq("product_id", product));
		Product_Ratting pratting = (Product_Ratting) crit.uniqueResult();
		if (pratting != null) {
			Criteria crit1 = session.createCriteria(Product_Ratting.class);
			crit1.add(Restrictions.eq("product_id", product));
			crit1.setProjection(Projections.avg("star"));
			List ratting = crit1.list();
			double rat = (Double) ratting.get(0);
			int avgRatting = (int) rat;
			Criteria criter = session.createCriteria(Product.class);
			criter.add(Restrictions.eq("product_id", product_id));
			Product productupdaterate = (Product) criter.uniqueResult();
			productupdaterate.setTotal_star(avgRatting);
			session.saveOrUpdate(productupdaterate);
		}

	}

	@Override
	public void savereview(Reviews review) {
		Session session = sessionFactory.getCurrentSession();
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();
		if (users != null) {
			Product_Reviews product_reviews = new Product_Reviews();
			product_reviews.setFirstName(users.getFirstName());
			product_reviews.setLastName(users.getLastName());
			product_reviews.setReview(review.getReview());
			Date date = new Date();
			product_reviews.setReview_date(date);
			product_reviews.setUid(users.getUid());
			Product product = new Product();
			product.setProduct_id(review.getProduct_id());
			product_reviews.setProduct_id(product);
			session.save(product_reviews);
		}
	}

	@Override
	public List<Product_Reviews> getreviewslist(int product_id) {
		Session session = sessionFactory.getCurrentSession();
		Product product = new Product();
		product.setProduct_id(product_id);
		Criteria crit = session.createCriteria(Product_Reviews.class);
		crit.add(Restrictions.eq("product_id", product));
		List<Product_Reviews> reviewlist = crit.list();
		return reviewlist;
	}

	@Override
	public String savefavourite(int product_id) {
		Session session = sessionFactory.getCurrentSession();
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String result = null;
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();
		Product product = new Product();
		product.setProduct_id(product_id);
		if (users != null) {
			Criteria crit = session.createCriteria(Product_Favourite.class);
			crit.add(Restrictions.eq("product_id", product));
			crit.add(Restrictions.eq("uid", users.getUid()));
			Product_Favourite pratting = (Product_Favourite) crit.uniqueResult();
			if (pratting == null) {
				Product_Favourite productfavourite = new Product_Favourite();
				productfavourite.setUid(users.getUid());
				productfavourite.setProduct_id(product);
				Date date = new Date();
				productfavourite.setFavourite_date(date);
				session.save(productfavourite);
				result = "Successfully Add!";
			} else {
				session.delete(pratting);
				result = "Successfully Removed!";
			}
		} else {
			result = "Somthing is wrong";
		}
		return result;
	}

	@Override
	public String getfovourite(int product_id) {
		Session session = sessionFactory.getCurrentSession();
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		String result = null;
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();
		if (users != null) {
			Product product = new Product();
			product.setProduct_id(product_id);
			Criteria crit = session.createCriteria(Product_Favourite.class);
			crit.add(Restrictions.eq("product_id", product));
			crit.add(Restrictions.eq("uid", users.getUid()));
			Product_Favourite pratting = (Product_Favourite) crit.uniqueResult();
			if (pratting != null) {
				result = "true";
			} else {
				result = "false";
			}
		} else {
			result = "false";
		}

		return result;
	}

	@Override
	public String rmvqty(int product_id) {
		Session session = sessionFactory.getCurrentSession();
		String result = null;
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();

		User user = new User();
		user.setUid(users.getUid());

		Criteria crite = session.createCriteria(MyCard.class);
		crite.add(Restrictions.eq("uid", user));
		crite.add(Restrictions.eq("product_id", product_id));
		MyCard mycart = (MyCard) crite.uniqueResult();
		if (mycart.getQuantity() > 1) {
			mycart.setQuantity(mycart.getQuantity() - 1);
			session.saveOrUpdate(mycart);
			result = "successful";
		} else {
			result = "check product max quantity 1";
		}
		return result;
	}

	@Override
	public String addqty(int product_id) {
		Session session = sessionFactory.getCurrentSession();
		String result = null;
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();

		User user = new User();
		user.setUid(users.getUid());

		Criteria crite = session.createCriteria(MyCard.class);
		crite.add(Restrictions.eq("uid", user));
		crite.add(Restrictions.eq("product_id", product_id));
		MyCard mycart = (MyCard) crite.uniqueResult();
		if (mycart.getQuantity() >= 1 && mycart.getQuantity() <= 10) {
			mycart.setQuantity(mycart.getQuantity() + 1);
			session.saveOrUpdate(mycart);
			result = "successful";
		} else {
			result = "check product max quantity 1";
		}
		return result;
	}

	@Override
	public GetProductCartTotal gettotal() {
		Session session = sessionFactory.getCurrentSession();
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();
		List<Product> pro = new ArrayList<Product>();
		double total = 0;
		double discounttotal = 0;
		double gstfinaltotal;
		GetProductCartTotal gpc = new GetProductCartTotal();
		if (users != null) {
			if (users != null) {
				User user = new User();
				user.setUid(users.getUid());
				Criteria crit = session.createCriteria(MyCard.class);
				crit.add(Restrictions.eq("uid", user));
				List<MyCard> mycart = crit.list();
				for (MyCard myCard : mycart) {
					int product_id = myCard.getProduct_id();
					Criteria crite = session.createCriteria(Product.class);
					crite.add(Restrictions.eq("product_id", product_id));
					Product product1 = (Product) crite.uniqueResult();
					product1.setQuantity(myCard.getQuantity());
					double price = (product1.getPrice() * myCard.getQuantity());
					total += price;
					discounttotal += ((price * product1.getDiscount_per()) / 100);
				}
				gpc.setTotal(total);
				gpc.setDiscounttotal(discounttotal);
				gpc.setCgst((total * 2.50) / 100);
				gpc.setSgst((total * 2.50) / 100);
				gpc.setGstfinaltotal((total + (total * 2.50) / 100 + (total * 2.50) / 100) - discounttotal);
			}
		}
		return gpc;
	}

	@Override
	public int savestudentdetails(Table_Book_Details tablebookdetail) {
		Session session = sessionFactory.getCurrentSession();
		int flag = 0;
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();
		if (users != null) {
			session.save(tablebookdetail);
			Mailing m = new Mailing();
			m.setEmail(name);
			m.setOtp("hi i am rupesh");
			m.sendMail();
			flag = 1;
		} else {
			session.save(tablebookdetail);
		}
		return flag;
	}

	@Override
	public boolean addorder(Orders orders) {
		Session session = sessionFactory.getCurrentSession();
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();
		double total = 0;
		double discounttotal = 0;
		double gstfinaltotal;
		orders.setOrderDate(new Date());
		orders.setDelevery_status("pending");
		orders.setUid(users);
		session.save(orders);
		boolean flag = false;
		if (users != null) {
			User user = new User();
			user.setUid(users.getUid());
			Criteria crit = session.createCriteria(MyCard.class);
			crit.add(Restrictions.eq("uid", user));
			List<MyCard> mycart = crit.list();
			for (MyCard myCard : mycart) {
				int product_id = myCard.getProduct_id();
				Criteria crite = session.createCriteria(Product.class);
				crite.add(Restrictions.eq("product_id", product_id));
				Product product1 = (Product) crite.uniqueResult();
				Order_Details order_details = new Order_Details();
				order_details.setOrder_id(orders);
				order_details.setQuantity(product1.getQuantity());
				order_details.setProduct_price(product1.getPrice());
				double total_amount = (product1.getPrice() * myCard.getQuantity());
				order_details.setTotal_amount(total_amount);
				order_details.setProduct_id(product1);
				session.save(order_details);
				session.delete(myCard);
				flag = true;
			}
		}

		return flag;
	}

	@Override
	public List<Orders> getmyorders() {
		Session session = sessionFactory.getCurrentSession();
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();
		List<Orders> orders = null;
		List<Order_Details> order_details;
		List<Product> product;
		if (users != null) {
			User user = new User();
			user.setUid(users.getUid());
			Criteria crit = session.createCriteria(Orders.class);
			crit.add(Restrictions.eq("uid", user));
			crit.add(Restrictions.eq("delevery_status", "pending"));
			orders = crit.list();

			for (Orders order : orders) {
				Orders ord = new Orders();
				ord.setOrder_id(order.getOrder_id());
				Criteria crit1 = session.createCriteria(Order_Details.class);
				crit1.add(Restrictions.eq("order_id", ord));
				order_details = crit1.list();

				for (Order_Details orderdetail : order_details) {
					/*
					 * Criteria crit2 = session.createCriteria(Product.class);
					 * crit2.add(Restrictions.eq("product_id", orderdetail.getProduct_id()));
					 * product = crit2.list();
					 */
				}
			}
		}

		return orders;
	}

	@Override
	public List<Order_Details> getmyorderdetails(int order_id) {
		Session session = sessionFactory.getCurrentSession();
		Authenticate authenticate = new Authenticate();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("emailID", name));
		User users = (User) criteria.uniqueResult();
		List<Order_Details> order_details = null;
		if (users != null) {
			Orders ord = new Orders();
			ord.setOrder_id(order_id);
			Criteria crit1 = session.createCriteria(Order_Details.class);
			crit1.add(Restrictions.eq("order_id", ord));
			order_details = crit1.list();

		}
		return order_details;
	}

}
