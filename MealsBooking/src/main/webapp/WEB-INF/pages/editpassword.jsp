<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16"
	href="resources/horizontal/assets/images/favicon.png">
<title>Admin Press Admin Template - The Ultimate Bootstrap 4
	Admin Template</title>
<!-- Bootstrap Core CSS -->
<link
	href="resources/horizontal/assets/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="resources/horizontal/css/style.css" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="resources/horizontal/css/colors/blue.css" id="theme"
	rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
	var sheight = window.screen.availHeight - 183;
	var swidth = window.screen.availWidth - 800;
	$(document).ready(function() {

	});
	var app = angular.module('myapp', []);
	app.controller('mycontroller', function($scope, $http, $compile) {
		angular.element(document).ready(function() {
		});

	});

	function checkLoginForm(form) {
		email = document.getElementById("email");
		oldpwd = document.getElementById("old_password");
		newpwd = document.getElementById("password-field");
		conpwd = document.getElementById("password-field1");
		if (email.value == "") {
			alert("Enter UserID");
			email.focus();
			return false;
		}
		if (oldpwd.value == "") {
			alert("Enter old password!");
			oldpwd.focus();
			return false;
		}
		if (newpwd.value == "") {
			alert("Enter new pasasword!");
			newpwd.focus();
			return false;
		}
		if (newpwd.value != conpwd.value) {
			alert("Enter new password and conform password is same!");
			conpwd.focus();
			return false;
		}
		return true;
	}
</script>
</head>

<body class="fix-header card-no-border logo-center">
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinners.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50"> <circle
			class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2"
			stroke-miterlimit="10" /> </svg>
	</div>
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper">
		<!-- ============================================================== -->
		<!-- Topbar header - style you can find in pages.scss -->
		<!-- ============================================================== -->
		<header class="topbar"> <nav
			class="navbar top-navbar navbar-expand-md navbar-light"> <!-- ============================================================== -->
		<!-- Logo --> <!-- ============================================================== -->
		<div class="navbar-header">
			<a class="navbar-brand" href="opendashboard"> <!-- Logo icon --> <b>
					<!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
					<!-- Dark Logo icon --> <img
					src="resources/horizontal/assets/images/logo-icon.png"
					alt="homepage" class="dark-logo" /> <!-- Light Logo icon --> <img
					src="resources/horizontal/assets/images/logo-light-icon.png"
					alt="homepage" class="light-logo" />
			</b> <!--End Logo icon --> <!-- Logo text --> <span> <!-- dark Logo text -->
					<img src="resources/horizontal/assets/images/logo-text.png"
					alt="homepage" class="dark-logo" /> <!-- Light Logo text --> <img
					src="resources/horizontal/assets/images/logo-light-text.png"
					class="light-logo" alt="homepage" /></span>
			</a>
		</div>
		<!-- ============================================================== -->
		<!-- End Logo --> <!-- ============================================================== -->
		<div class="navbar-collapse">
			<!-- ============================================================== -->
			<!-- toggle and nav items -->
			<!-- ============================================================== -->
			<ul class="navbar-nav mr-auto mt-md-0">
				<!-- This is  -->
				<li class="nav-item"><a
					class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark"
					href="javascript:void(0)"><i class="mdi mdi-menu"></i></a></li>
				<!-- ============================================================== -->
				<!-- Comment -->
				<!-- ============================================================== -->
				<!--  <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu mailbox scale-up-left">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            Message
                                            <a href="#">
                                                <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span> </div>
                                            </a>
                                            Message
                                            <a href="#">
                                                <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Event today</h5> <span class="mail-desc">Just a reminder that you have event</span> <span class="time">9:10 AM</span> </div>
                                            </a>
                                            Message
                                            <a href="#">
                                                <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Settings</h5> <span class="mail-desc">You can customize this template as you want</span> <span class="time">9:08 AM</span> </div>
                                            </a>
                                            Message
                                            <a href="#">
                                                <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li> -->
				<!-- ============================================================== -->
				<!-- End Comment -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Messages -->
				<!-- ============================================================== -->
				<!--  <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-email"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu mailbox scale-up-left" aria-labelledby="2">
                                <ul>
                                    <li>
                                        <div class="drop-title">You have 4 new messages</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            Message
                                            <a href="#">
                                                <div class="user-img"> <img src="resources/horizontal/assets/images/users/1.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                            </a>
                                            Message
                                            <a href="#">
                                                <div class="user-img"> <img src="resources/horizontal/assets/images/users/2.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                            </a>
                                            Message
                                            <a href="#">
                                                <div class="user-img"> <img src="resources/horizontal/assets/images/users/3.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                            </a>
                                            Message
                                            <a href="#">
                                                <div class="user-img"> <img src="resources/horizontal/assets/images/users/4.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>See all e-Mails</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li> -->
				<!-- ============================================================== -->
				<!-- End Messages -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Messages -->
				<!-- ============================================================== -->
				<%--  <li class="nav-item dropdown mega-dropdown"> <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-view-grid"></i></a>
                            <div class="dropdown-menu scale-up-left">
                                <ul class="mega-dropdown-menu row">
                                    <li class="col-lg-3 col-xlg-2 m-b-30">
                                        <h4 class="m-b-20">CAROUSEL</h4>
                                        <!-- CAROUSEL -->
                                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner" role="listbox">
                                                <div class="carousel-item active">
                                                    <div class="container"> <img class="d-block img-fluid" src="resources/horizontal/assets/images/big/img1.jpg" alt="First slide"></div>
                                                </div>
                                                <div class="carousel-item">
                                                    <div class="container"><img class="d-block img-fluid" src="resources/horizontal/assets/images/big/img2.jpg" alt="Second slide"></div>
                                                </div>
                                                <div class="carousel-item">
                                                    <div class="container"><img class="d-block img-fluid" src="resources/horizontal/assets/images/big/img3.jpg" alt="Third slide"></div>
                                                </div>
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                                        </div>
                                        <!-- End CAROUSEL -->
                                    </li>
                                    <li class="col-lg-3 m-b-30">
                                        <h4 class="m-b-20">ACCORDION</h4>
                                        <!-- Accordian -->
                                        <div id="accordion" class="nav-accordion" role="tablist" aria-multiselectable="true">
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingOne">
                                                    <h5 class="mb-0">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                  Collapsible Group Item #1
                                                </a>
                                              </h5> </div>
                                                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high. </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingTwo">
                                                    <h5 class="mb-0">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                  Collapsible Group Item #2
                                                </a>
                                              </h5> </div>
                                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                  Collapsible Group Item #3
                                                </a>
                                              </h5> </div>
                                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-lg-3  m-b-30">
                                        <h4 class="m-b-20">CONTACT US</h4>
                                        <!-- Contact -->
                                        <form>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputname1" placeholder="Enter Name"> </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Enter email"> </div>
                                            <div class="form-group">
                                                <textarea class="form-control" id="exampleTextarea" rows="3" placeholder="Message"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-info">Submit</button>
                                        </form>
                                    </li>
                                    <li class="col-lg-3 col-xlg-4 m-b-30">
                                        <h4 class="m-b-20">List style</h4>
                                        <!-- List style -->
                                        <ul class="list-style-none">
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> You can give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Another Give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Forth link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Another fifth link</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </li> --%>
				<!-- ============================================================== -->
				<!-- End Messages -->
				<!-- ============================================================== -->
			</ul>
			<!-- ============================================================== -->
			<!-- User profile and search -->
			<!-- ============================================================== -->
			<ul class="navbar-nav my-lg-0">
				<!-- ============================================================== -->
				<!-- Search -->
				<!-- ============================================================== -->
				<%--  <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li> --%>
				<!-- ============================================================== -->
				<!-- Language -->
				<!-- ============================================================== -->
				<!--  <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="flag-icon flag-icon-us"></i></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up"> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-in"></i> India</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> China</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-de"></i> Dutch</a> </div>
                        </li> -->
				<!-- ============================================================== -->
				<!-- Profile -->
				<!-- ============================================================== -->
				<li class="topname">${name}</li>
				<%-- <li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle text-muted waves-effect waves-dark"
					href="" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"><img
						src="resources/horizontal/assets/images/users/1u.png" alt="user"
						class="profile-pic" /></a>
					<div class="dropdown-menu dropdown-menu-right scale-up">
						<ul class="dropdown-user">
							<li>
								<div class="dw-user-box">
									<div class="u-img">
										<img src="resources/horizontal/assets/images/users/1u.png"
											alt="user">
									</div>
									<div class="u-text">
										<p class="text-muted">${status}</p>
									</div>
								</div>
							</li>
							<li role="separator" class="divider"></li>
							<li><a href="newpassword"><i class="ti-user"></i>Change
									Password</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="<c:url value="/logout"/>"> <i
									class="fa fa-power-off"></i> Logout
							</a></li>
						</ul>
					</div></li> --%>
			</ul>
		</div>
		</nav> </header>
		<!-- ============================================================== -->
		<!-- End Topbar header -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Left Sidebar - style you can find in sidebar.scss  -->
		<!-- ============================================================== -->
		<aside class="left-sidebar"> <!-- Sidebar scroll-->
		<div class="scroll-sidebar">
			<!-- Sidebar navigation-->
			<nav class="sidebar-nav">
			<ul id="sidebarnav">
				<li class="nav-small-cap">PERSONAL</li>
				<li><a class="has-arrow waves-effect waves-dark"
					href="opendashboard" aria-expanded="false"> <i
						class="mdi mdi-gauge"></i>Dashboard
				</a></li>
				<li><a class="has-arrow waves-effect waves-dark"
					href="transaction" aria-expanded="false"> <i
						class="mdi mdi-wallet-travel"></i>Wallet Transactions
				</a></li>
				<li><a class="has-arrow waves-effect waves-dark"
					href="token_p_history" aria-expanded="false"> <i
						class="mdi mdi-history"></i>Token Purchase History
				</a></li>
				<li><a class="has-arrow waves-effect waves-dark"
					href="purchase" aria-expanded="false"> <i
						class="mdi mdi-bullseye"></i>Purchase Token
				</a></li>
				<li><a class="has-arrow waves-effect waves-dark"
					href="icoopenpage" aria-expanded="false"> <i
						class="mdi mdi-bullseye"></i>ICO <span class="blink">live</span>
				</a></li>
				<li><a class="has-arrow waves-effect waves-dark"
					href="newpassword" aria-expanded="false"> <i
						class="mdi mdi-face-profile"></i>Profile
				</a></li>
				<!-- <li><a class="has-arrow waves-effect waves-dark" href="#"
					aria-expanded="false"> <i class="mdi mdi-login-variant"></i>
						Logout
				</a></li> -->
				<li><a class="has-arrow waves-effect waves-dark"
					href="<c:url value="/logout"/>" aria-expanded="false"> <i
						class="mdi mdi-login-variant"></i> Logout
				</a></li>


			</ul>
			</nav>
			<!-- End Sidebar navigation -->
		</div>
		<!-- End Sidebar scroll--> </aside>
		<!-- ============================================================== -->
		<!-- End Left Sidebar - style you can find in sidebar.scss  -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->
		<div class="page-wrapper">
			<div class="container">
				<!-- ============================================================== -->
				<!-- Start Page Content -->
				<!-- ============================================================== -->
				<!-- Row -->
				<div class="row">
					<!-- Column -->
					<div class="col-lg-4 col-xlg-3 col-md-5">
						<div class="card">
							<div class="card-body">
								<center class="m-t-30">
									<img src="resources/horizontal/assets/images/users/5.jpg"
										class="img-circle" width="150" />
									<h4 class="card-title m-t-10">${firstname} ${lastname}</h4>
									<h6 class="card-subtitle">${emailid}</h6>
									<!-- <div class="row text-center justify-content-md-center">
										<div class="col-4">
											<a href="javascript:void(0)" class="link"><i
												class="icon-people"></i> <font class="font-medium">254</font></a>
										</div>
										<div class="col-4">
											<a href="javascript:void(0)" class="link"><i
												class="icon-picture"></i> <font class="font-medium">54</font></a>
										</div>
									</div> -->
								</center>
							</div>
							
							<!-- <div class="card-body">
								<small class="text-muted">Email address </small> <small
									class="text-muted p-t-30 db">Social Profile</small> <br />
								<button class="btn btn-circle btn-secondary">
									<i class="fa fa-facebook"></i>
								</button>
								<button class="btn btn-circle btn-secondary">
									<i class="fa fa-twitter"></i>
								</button>
								<button class="btn btn-circle btn-secondary">
									<i class="fa fa-youtube"></i>
								</button>
							</div> -->
						</div>
					</div>
					<!-- Column -->
					<!-- Column -->
					<div class="col-lg-8 col-xlg-9 col-md-7">
						<div class="card">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs profile-tab" role="tablist">
								<li class="nav-item"><a class="nav-link active"
									data-toggle="tab" href="#profile" role="tab">Profile</a></li>
								<li class="nav-item"><a class="nav-link" data-toggle="tab"
									href="#settings" role="tab">Settings</a></li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div class="tab-pane active" id="profile" role="tabpanel">
									<div class="card-body">
										<form class="form-horizontal form-material">
											<div class="form-group">
												<label class="col-md-12">First Name</label>
												<div class="col-md-12">
													<input type="text"
														value="${firstname}" readonly="true" class="form-control form-control-line">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-12">Last Name</label>
												<div class="col-md-12">
													<input type="text" 
														value="${lastname}" readonly="true" class="form-control form-control-line">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-12">Contact Number</label>
												<div class="col-md-12">
													<input type="text" 
														value="${phone}" readonly="true" class="form-control form-control-line">
												</div>
											</div>
											<div class="form-group">
												<label for="example-email" class="col-md-12">Email</label>
												<div class="col-md-12">
													<input type="email" readonly="true"  value="${emailid}"
														class="form-control form-control-line"
														name="example-email" id="example-email">
												</div>
											</div>
											<!-- <div class="form-group">
												<label class="col-md-12">Password</label>
												<div class="col-md-12">
													<input type="password" value="password"
														class="form-control form-control-line">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-12">Phone No</label>
												<div class="col-md-12">
													<input type="text" placeholder="123 456 7890"
														class="form-control form-control-line">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-12">Message</label>
												<div class="col-md-12">
													<textarea rows="5" class="form-control form-control-line"></textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-12">Select Country</label>
												<div class="col-sm-12">
													<select class="form-control form-control-line">
														<option>London</option>
														<option>India</option>
														<option>Usa</option>
														<option>Canada</option>
														<option>Thailand</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-12">
													<button class="btn btn-success">Update Profile</button>
												</div>
											</div> -->
										</form>
									</div>
								</div>
								<div class="tab-pane" id="settings" role="tabpanel">
									<div class="card-body">
										<form:form class="form-horizontal form-material"
											name="changepassword" action="passwordchange" method="post"
											modelAttribute="changepassword"
											onsubmit='return checkLoginForm(this);'>


											<div class="form-group">
												<!-- <label for="example-email" class="col-md-12">EmailID</label> -->
												<div class="col-md-12">
													<form:input path="emailID"
														class="form-control form-control-line" type="hidden"
														id="email" name="email" value="${emailid}"
														placeholder="Email ID" required="" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-12">Old Password</label>
												<div class="col-md-12">
													<form:input path="old_password"
														class="form-control form-control-line" type="password"
														id="old_password" name="password"
														placeholder="old password" required="" />
												</div>
											</div>
											<div class="form-group">
												<label for="example-email" class="col-md-12">New
													Password</label>
												<div class="col-md-12">
													<form:input path="new_password"
														class="form-control form-control-line" type="password"
														id="password-field" name="password"
														placeholder="new password" required="" />
													<span toggle="#password-field"
														class="fa fa-fw fa-eye field-icon toggle-password"></span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-12">Confirm Password.</label>
												<div class="col-md-12">
													<form:input path="conform_password"
														class="form-control form-control-line" type="password"
														id="password-field1" name="password"
														placeholder="confirm password" required="" />
													<span toggle="#password-field1"
														class="fa fa-fw fa-eye field-icon toggle-password"></span>
												</div>
											</div>
											<div class="form-group">
												<div class=" col-sm-12 text-center">
													<button type="submit" href="" class="btn btn-success">Update
														Profile</button>
												</div>
											</div>
										</form:form>
										<%--  <form class="form-horizontal form-material">
                                            <div class="form-group">
                                                <label class="col-md-12">Current Password</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="Johnathan Doe" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">New Password</label>
                                                <div class="col-md-12">
                                                    <input type="email" placeholder="johnathan@admin.com" class="form-control form-control-line" name="example-email" id="example-email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Confirm Password.</label>
                                                <div class="col-md-12">
                                                    <input type="password" value="password" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success">Update</button>
                                                </div>
                                            </div>
                                        </form> --%>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Column -->
				</div>
				<!-- Row -->
				<!-- ============================================================== -->
				<!-- End PAge Content -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Right sidebar -->
				<!-- ============================================================== -->
				<!-- .right-sidebar -->
				<div class="right-sidebar">
					<div class="slimscrollright">
						<div class="rpanel-title">
							Service Panel <span><i class="ti-close right-side-toggle"></i></span>
						</div>
						<div class="r-panel-body">
							<ul id="themecolors" class="m-t-20">
								<li><b>With Light sidebar</b></li>
								<li><a href="javascript:void(0)" data-theme="default"
									class="default-theme">1</a></li>
								<li><a href="javascript:void(0)" data-theme="green"
									class="green-theme">2</a></li>
								<li><a href="javascript:void(0)" data-theme="red"
									class="red-theme">3</a></li>
								<li><a href="javascript:void(0)" data-theme="blue"
									class="blue-theme working">4</a></li>
								<li><a href="javascript:void(0)" data-theme="purple"
									class="purple-theme">5</a></li>
								<li><a href="javascript:void(0)" data-theme="megna"
									class="megna-theme">6</a></li>
								<li class="d-block m-t-30"><b>With Dark sidebar</b></li>
								<li><a href="javascript:void(0)" data-theme="default-dark"
									class="default-dark-theme">7</a></li>
								<li><a href="javascript:void(0)" data-theme="green-dark"
									class="green-dark-theme">8</a></li>
								<li><a href="javascript:void(0)" data-theme="red-dark"
									class="red-dark-theme">9</a></li>
								<li><a href="javascript:void(0)" data-theme="blue-dark"
									class="blue-dark-theme">10</a></li>
								<li><a href="javascript:void(0)" data-theme="purple-dark"
									class="purple-dark-theme">11</a></li>
								<li><a href="javascript:void(0)" data-theme="megna-dark"
									class="megna-dark-theme ">12</a></li>
							</ul>
							<ul class="m-t-20 chatonline">
								<li><b>Chat option</b></li>
								<li><a href="javascript:void(0)"><img
										src="resources/horizontal/assets/images/users/1.jpg"
										alt="user-img" class="img-circle"> <span>Varun
											Dhavan <small class="text-success">online</small>
									</span></a></li>
								<li><a href="javascript:void(0)"><img
										src="resources/horizontal/assets/images/users/2.jpg"
										alt="user-img" class="img-circle"> <span>Genelia
											Deshmukh <small class="text-warning">Away</small>
									</span></a></li>
								<li><a href="javascript:void(0)"><img
										src="resources/horizontal/assets/images/users/3.jpg"
										alt="user-img" class="img-circle"> <span>Ritesh
											Deshmukh <small class="text-danger">Busy</small>
									</span></a></li>
								<li><a href="javascript:void(0)"><img
										src="resources/horizontal/assets/images/users/4.jpg"
										alt="user-img" class="img-circle"> <span>Arijit
											Sinh <small class="text-muted">Offline</small>
									</span></a></li>
								<li><a href="javascript:void(0)"><img
										src="resources/horizontal/assets/images/users/5.jpg"
										alt="user-img" class="img-circle"> <span>Govinda
											Star <small class="text-success">online</small>
									</span></a></li>
								<li><a href="javascript:void(0)"><img
										src="resources/horizontal/assets/images/users/6.jpg"
										alt="user-img" class="img-circle"> <span>John
											Abraham<small class="text-success">online</small>
									</span></a></li>
								<li><a href="javascript:void(0)"><img
										src="resources/horizontal/assets/images/users/7.jpg"
										alt="user-img" class="img-circle"> <span>Hritik
											Roshan<small class="text-success">online</small>
									</span></a></li>
								<li><a href="javascript:void(0)"><img
										src="resources/horizontal/assets/images/users/8.jpg"
										alt="user-img" class="img-circle"> <span>Pwandeep
											rajan <small class="text-success">online</small>
									</span></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- End Right sidebar -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- End Container fluid  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- footer -->
			<!-- ============================================================== -->
			<footer class="footer">© 2018 Oristo Coin</footer>
			<!-- ============================================================== -->
			<!-- End footer -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Wrapper -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- All Jquery -->
	<!-- ============================================================== -->
	<script src="resources/horizontal/assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap tether Core JavaScript -->
	<script
		src="resources/horizontal/assets/plugins/bootstrap/js/popper.min.js"></script>
	<script
		src="resources/horizontal/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- slimscrollbar scrollbar JavaScript -->
	<script src="resources/horizontal/js/jquery.slimscroll.js"></script>
	<!--Wave Effects -->
	<script src="resources/horizontal/js/waves.js"></script>
	<!--Menu sidebar -->
	<script src="resources/horizontal/js/sidebarmenu.js"></script>
	<!--stickey kit -->
	<script
		src="resources/horizontal/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
	<script
		src="resources/horizontal/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!--Custom JavaScript -->
	<script src="resources/horizontal/js/custom.min.js"></script>
	<!-- ============================================================== -->
	<!-- Style switcher -->
	<!-- ============================================================== -->
	<script
		src="resources/horizontal/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
	<script>
		$(".toggle-password").click(function() {

			$(this).toggleClass("fa-eye fa-eye-slash");
			var input = $($(this).attr("toggle"));
			if (input.attr("type") == "password") {
				input.attr("type", "text");
			} else {
				input.attr("type", "password");
			}
		});
	</script>
</body>

</html>
