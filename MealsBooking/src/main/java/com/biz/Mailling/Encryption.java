package com.biz.Mailling;

import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class Encryption {
	
	static String getkey;// = "aesEncryptionKey";// 128 bit key ,we can make 256 12345678123456781234567812345678
	private static final String initVector = "encryptionIntVec";
	static String SALT2 = "deliciously salty";
	
	public Encryption(String key) {
		
		this.getkey = key;
	}
	public static String encrypt(String value) {
		try {
				IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
				byte[] key = (SALT2 + getkey ).getBytes("UTF-8");
			
			    MessageDigest sha = MessageDigest.getInstance("SHA-1");
			    key = sha.digest(key);
			    key = Arrays.copyOf(key, 16); // use only first 128 bit
			    SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
			    
				Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
				cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, iv);

			byte[] encrypted = cipher.doFinal(value.getBytes());
			return DatatypeConverter.printBase64Binary(encrypted);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	

}
