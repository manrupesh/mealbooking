<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>


<div class="menu-container">
	<sec:authorize access="hasAuthority('AUTH_USER')">
		<a href="userdashboard">Home</a>
		| <a href="getproductlist"> Product List </a>
		| <a href="showcart"> My Cart </a> |
	</sec:authorize>
	<sec:authorize access="hasAuthority('AUTH_ADMIN')">
		<a href="admindashboard">Home</a>
		| <a href="admindashboard"> Product List </a>
		<a href="${pageContext.request.contextPath}/orderList"> Order List
		</a>
     |<a href="viewuploadproduct?product_id=0"> Create Product </a>
         |
	</sec:authorize>
</div>