<%@page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
<link rel="stylesheet" type="text/css" href="resources/mycss/styles.css">
<script src="resources/myJS/bootstrap.min.js"></script>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>
<script src="resources/myJS/jquery-ui.js"></script>
<script src="resources/myJS/custom.js"></script>
<script src="resources/myJS/jquery.mark.min.js"></script>
<script src="resources/myJS/jquery.calculation.js"></script>
<script src="resources/myJS/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery.dataTables.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- <script
	src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.5/angular.min.js"></script> -->
<script type="text/javascript">
	var app = angular.module('myapp', []);
	app.controller('myappcontroller', function($scope, $http, $compile) {
		$scope.product_id = 0;
		$scope.addtocart = function(id) {
			var bvalue = id;
			$.ajax({
				type : 'POST',
				url : "user/addtomycart",
				data : {
					product_id : bvalue
				},
				success : function(data) {
					location.reload(true);
					alert(data);

				}
			});
		}
		$scope.ratting = function(star, product_id) {
			var product_id = product_id;
			var star = star;
			$.ajax({
				type : 'POST',
				url : "user/ratting",
				data : {
					product_id : product_id,
					star : star
				},
				success : function(data) {
					location.reload(true);
					alert(data);

				}
			});
		}

		$scope.favourite = function(product_id) {
			var product_id = product_id;
			$.ajax({
				type : 'POST',
				url : "user/savefavourite",
				data : {
					product_id : product_id,
				},
				success : function(data) {
					location.reload(true);
				}
			});
		}
	});
</script>
<style>
mark {
	padding: 0px;
	background-color: #f1c40f;
}

.checked {
	color: orange;
}
</style>

</head>
<body ng-app="myapp" ng-controller="myappcontroller">
	<jsp:include page="_header.jsp" />
	<jsp:include page="_menu.jsp" />

	<fmt:setLocale value="en_US" scope="session" />
<img alt="" src="resources/theme1/assets/images/emailler/Logo.png">
	<h2>Product List</h2>
	
	<h2>Veg Product</h2>
	<c:forEach var="prodInfo" items="${product}">
		<c:if test="${prodInfo.category == 'veg'}">
			<div class="product-preview-container">
				<ul>
					<li><img class="product-image"
						src="data:image/png;base64,${prodInfo.img}" /></li>
					<li>product_id: ${prodInfo.product_id}</li>
					<li>Name: ${prodInfo.name}</li>
					<li>Price: Rs.
						${prodInfo.price}&nbsp;&nbsp;Discount=${prodInfo.discount_per}%</li>
					<li>
						<button ng-click="addtocart(${prodInfo.product_id})">add
							to Cart</button>||<a href="review?product_id=${prodInfo.product_id}"><button
								ng-click="review(${prodInfo.product_id})">view review</button></a>
					</li>

					<c:if test="${prodInfo.total_star < 1}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(3,${prodInfo.product_id})"
							class="fa fa-star"></span> <span
							ng-click="ratting(4,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(5,${prodInfo.product_id})"
							class="fa fa-star"></span></li>
					</c:if>
					<c:if test="${prodInfo.total_star == 1}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(3,${prodInfo.product_id})"
							class="fa fa-star"></span> <span
							ng-click="ratting(4,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(5,${prodInfo.product_id})"
							class="fa fa-star"></span></li>
					</c:if>
					<c:if test="${prodInfo.total_star == 2}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(3,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(4,${prodInfo.product_id})"
							class="fa fa-star"></span> <span
							ng-click="ratting(5,${prodInfo.product_id})" class="fa fa-star"></span>
						</li>
					</c:if>
					<c:if test="${prodInfo.total_star == 3}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(3,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(4,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(5,${prodInfo.product_id})"
							class="fa fa-star"></span></li>
					</c:if>
					<c:if test="${prodInfo.total_star == 4}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(3,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(4,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(5,${prodInfo.product_id})" class="fa fa-star"></span>
						</li>
					</c:if>
					<c:if test="${prodInfo.total_star == 5}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(3,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(4,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(5,${prodInfo.product_id})"
							class="fa fa-star checked"></span></li>

					</c:if>
					<c:if test="${prodInfo.favourit =='false'}">
						<li><button ng-click="favourite(${prodInfo.product_id})">
								<i class="heart fa fa-heart-o"></i>
							</button></li>
					</c:if>
					<c:if test="${prodInfo.favourit =='true'}">
						<li><button ng-click="favourite(${prodInfo.product_id})">
								<i class="heart fa fa-heart checked"></i>
							</button></li>
					</c:if>
					<!-- <li><i class="heart fa fa-heart checked"></i></li> -->
					<sec:authorize access="hasAuthority('AUTH_ADMIN')">
						<li><a style="color: red;"
							href="viewuploadproduct?product_id=${prodInfo.product_id}">
								Edit Product</a></li>
					</sec:authorize>
				</ul>
			</div>
		</c:if>
	</c:forEach>
	<br />
	<h2>Non-Veg Product</h2>
	<c:forEach var="prodInfo" items="${product}">
		<c:if test="${prodInfo.category == 'non veg'}">
			<div class="product-preview-container">
				<ul>
					<li><img class="product-image"
						src="data:image/png;base64,${prodInfo.img}" /></li>
					<li>product_id: ${prodInfo.product_id}</li>
					<li>Name: ${prodInfo.name}</li>
					<li>Price: Rs.
						${prodInfo.price}&nbsp;&nbsp;Discount=${prodInfo.discount_per}%</li>
					<li>
						<button ng-click="addtocart(${prodInfo.product_id})">add
							to Cart</button>||<a href="review?product_id=${prodInfo.product_id}"><button
								ng-click="review(${prodInfo.product_id})">view review</button></a>
					</li>

					<c:if test="${prodInfo.total_star < 1}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(3,${prodInfo.product_id})"
							class="fa fa-star"></span> <span
							ng-click="ratting(4,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(5,${prodInfo.product_id})"
							class="fa fa-star"></span></li>
					</c:if>
					<c:if test="${prodInfo.total_star == 1}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(3,${prodInfo.product_id})"
							class="fa fa-star"></span> <span
							ng-click="ratting(4,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(5,${prodInfo.product_id})"
							class="fa fa-star"></span></li>
					</c:if>
					<c:if test="${prodInfo.total_star == 2}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(3,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(4,${prodInfo.product_id})"
							class="fa fa-star"></span> <span
							ng-click="ratting(5,${prodInfo.product_id})" class="fa fa-star"></span>
						</li>
					</c:if>
					<c:if test="${prodInfo.total_star == 3}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(3,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(4,${prodInfo.product_id})" class="fa fa-star"></span>
							<span ng-click="ratting(5,${prodInfo.product_id})"
							class="fa fa-star"></span></li>
					</c:if>
					<c:if test="${prodInfo.total_star == 4}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(3,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(4,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(5,${prodInfo.product_id})" class="fa fa-star"></span>
						</li>
					</c:if>
					<c:if test="${prodInfo.total_star == 5}">
						<li><span ng-click="ratting(1,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(2,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(3,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(4,${prodInfo.product_id})"
							class="fa fa-star checked"></span> <span
							ng-click="ratting(5,${prodInfo.product_id})"
							class="fa fa-star checked"></span></li>

					</c:if>
					<c:if test="${prodInfo.favourit =='false'}">
						<li><button ng-click="favourite(${prodInfo.product_id})">
								<i class="heart fa fa-heart-o"></i>
							</button></li>
					</c:if>
					<c:if test="${prodInfo.favourit =='true'}">
						<li><button ng-click="favourite(${prodInfo.product_id})">
								<i class="heart fa fa-heart checked"></i>
							</button></li>
					</c:if>
					<!-- <li><i class="heart fa fa-heart checked"></i></li> -->
					<sec:authorize access="hasAuthority('AUTH_ADMIN')">
						<li><a style="color: red;"
							href="viewuploadproduct?product_id=${prodInfo.product_id}">
								Edit Product</a></li>
					</sec:authorize>
				</ul>
			</div>
		</c:if>
	</c:forEach>

	<c:if test="${paginationProducts.totalPages > 1}">
		<div class="page-navigator">
			<c:forEach items="${paginationProducts.navigationPages}" var="page">
				<c:if test="${page != -1 }">
					<a href="productList?page=${page}" class="nav-item">${page}</a>
				</c:if>
				<c:if test="${page == -1 }">
					<span class="nav-item"> ... </span>
				</c:if>
			</c:forEach>

		</div>
	</c:if>

	<jsp:include page="_footer.jsp" />

	<a class="has-arrow waves-effect waves-dark"
		href="<c:url value="/logout"/>" aria-expanded="false"> <i
		class="mdi mdi-login-variant"></i> Logout
	</a>



</body>
</html>