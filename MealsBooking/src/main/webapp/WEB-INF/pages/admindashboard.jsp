<%@page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
<link rel="stylesheet" type="text/css" href="resources/mycss/styles.css">
<script src="resources/myJS/bootstrap.min.js"></script>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>
<script src="resources/myJS/jquery-ui.js"></script>
<script src="resources/myJS/custom.js"></script>
<script src="resources/myJS/jquery.mark.min.js"></script>
<script src="resources/myJS/jquery.calculation.js"></script>
<script src="resources/myJS/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery.dataTables.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.5/angular.min.js"></script>
<script type="text/javascript">
	var app = angular.module('myapp', []);
	app.controller('myappcontroller', function($scope, $http, $compile) {

	});
</script>
<style>
mark {
	padding: 0px;
	background-color: #f1c40f;
}
</style>
</head>
<body ng-app="myapp" ng-controller="myappcontroller">
	<jsp:include page="_header.jsp" />
	<jsp:include page="_menu.jsp" />

	

	<div class="page-title">Product List</div>


	<c:forEach var="prodInfo" items="${product}">
		<div class="product-preview-container">
			<ul>
				<li><img class="product-image"
					src="data:image/png;base64,${prodInfo.img}" /></li>
				<li>product_id: ${prodInfo.product_id}</li>
				<li>Name: ${prodInfo.name}</li>
				<li>Price: Rs.
					${prodInfo.price}&nbsp;&nbsp;Discount=${prodInfo.discount_per}%</li>
				<li><a
					href="${pageContext.request.contextPath}/buyProduct?product_id=${prodInfo.product_id}">
						Buy Now</a></li>
				<!-- For Manager edit Product -->
				<sec:authorize access="hasAuthority('AUTH_ADMIN')">
					<li><a style="color: red;"
						href="viewuploadproduct?product_id=${prodInfo.product_id}">
							Edit Product</a></li>
				</sec:authorize>
			</ul>
		</div>

	</c:forEach>
	<br />

2222222222222222222222<img alt="" src="resources/theme1/assets/images/emailler/Logo.png">
	<c:if test="${paginationProducts.totalPages > 1}">
		<div class="page-navigator">
			<c:forEach items="${paginationProducts.navigationPages}" var="page">
				<c:if test="${page != -1 }">
					<a href="productList?page=${page}" class="nav-item">${page}</a>
				</c:if>
				<c:if test="${page == -1 }">
					<span class="nav-item"> ... </span>
				</c:if>
			</c:forEach>

		</div>
	</c:if>

	<jsp:include page="_footer.jsp" />

	<%-- <table class="table">
		<thead>
			<tr>
				<th>name</th>
				<th>Date</th>
				<th>Amount</th>
				<th>To Address</th>
				<th>Transaction ID</th>
			</tr>
		</thead>
		<c:forEach var="product" items="${product}">
			<tbody>
				<tr>
					<td>${product.name}</td>
					<td align='center'><img alt='img'
						style='height: 50%; width: 50%;'
						src="data:image/png;base64,${product.img}" /></td>
				</tr>
			</tbody>
		</c:forEach>
	</table> --%>

	<a class="has-arrow waves-effect waves-dark"
		href="<c:url value="/logout"/>" aria-expanded="false"> <i
		class="mdi mdi-login-variant"></i> Logout
	</a>



</body>
</html>