<%@page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
<link rel="stylesheet" type="text/css" href="resources/mycss/styles.css">
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('input[type="file"]').change(function(e) {
			var fileName = e.target.files[0].name;
		});
		$('input[type=file]').change(function() {
		});
	});
</script>
</head>
<body>
	<h2>How to insert image in database using Spring MVC</h2>
	<form:form action="admin/saveproduct?${_csrf.parameterName}=${_csrf.token}"
		enctype="multipart/form-data" modelAttribute="fileBucket"
		method="post">
		<%-- productName:<form:input type="text" path="productName" name="aert" />
	productDescription:<form:input type="text" path="productDescription"
			name="dsryt" />
    productType:<form:input type="text" path="productType" name="age" />
    availability:<form:input type="text" path="availability"
			name="agdse" />
    halfFull:<form:input type="text" path="halfFull" name="dsfg" />
	file:<form:input type="file" path="file" name="dfg" />
		<input type="submit" value="Submit"> --%>
		<table style="text-align: left;">
			<c:if test="${fileBucket.product_id > 0}">
				<tr>
					<td>Product ID *</td>
					<td><font color="red">${fileBucket.product_id}</font></td>
					<form:hidden path="product_id" />
				</tr>
			</c:if>
			<tr>
				<td>Name *</td>
				<td><form:input path="name" /></td>
			</tr>
			<tr>
				<td>Description *</td>
				<td><form:input path="description" /></td>
			</tr>
			<tr>
				<td>Category *</td>
				<td><form:select path="category">
						<form:option value="veg">VEG</form:option>
						<form:option value="non veg">NON VEG</form:option>
					</form:select></td>
			</tr>
			<tr>
				<td>Product Type *</td>
				<td><form:select path="product_type">
						<form:option value="hotdishes">HOT DISHES</form:option>
						<form:option value="dessert">DESSERT</form:option>
						<form:option value="drinks">DRINKS</form:option>
						<form:option value="other">Other</form:option>
					</form:select></td>
			</tr>
			<tr>
				<td>Availability *</td>
				<td><form:select path="availability">
						<form:option value="available">Available</form:option>
						<form:option value="unavailable">UnAvailable</form:option>
					</form:select></td>
			</tr>
			<tr>
				<td>Product_size *</td>
				<td><form:select path="product_size">
						<form:option value="half">HALF</form:option>
						<form:option value="full">FULL</form:option>
					</form:select></td>
			</tr>
			<tr>
				<td>Discount Percentage *</td>
				<td><form:input path="discount_per" /></td>
			</tr>
			<tr>
				<td>Discount Start Date *</td>
				<td><form:input type="datetime-local" id="start"
						path="startDate" value="2019-01-01T00:00" /></td>
			</tr>
			<tr>
				<td>Discount Start Date *</td>
				<td><form:input type="datetime-local" id="end" path="endDate"
						value="2019-01-01T00:00" /></td>
			</tr>
			<tr>
				<td>Price *</td>
				<td><form:input path="price" /></td>
			</tr>
			<tr>
				<td>Image</td>
				<td><li><img class="product-image"
						src="data:image/png;base64,${fileBucket.img}" /></li></td>
			</tr>
			<tr>
				<td>Upload Image</td>
				<td><form:input type="file" path="file" /></td>
				<td></td>
			</tr>


			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="Submit" /> <input type="reset"
					value="Reset" /></td>
			</tr>
		</table>
	</form:form>
	<p>${msg}</p>

</body>
</html>