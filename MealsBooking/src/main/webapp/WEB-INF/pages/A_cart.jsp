<%@page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
<link rel="stylesheet" type="text/css" href="resources/mycss/styles.css">
<script src="resources/myJS/bootstrap.min.js"></script>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>
<script src="resources/myJS/jquery-ui.js"></script>
<script src="resources/myJS/custom.js"></script>
<script src="resources/myJS/jquery.mark.min.js"></script>
<script src="resources/myJS/jquery.calculation.js"></script>
<script src="resources/myJS/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery.dataTables.css">
<!-- <script
	src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.5/angular.min.js"></script> -->
<script type="text/javascript">
	var app = angular.module('myapp', []);
	app.controller('myappcontroller', function($scope, $http, $compile) {
		$scope.product_id = 0;
		$scope.rmvtocart = function(id) {
			var bvalue = id;
			$.ajax({
				type : 'POST',
				url : "user/rmvtocart",
				data : {
					product_id : bvalue
				},
				success : function(data) {
					location.reload(true);
					alert(data);

				}
			});
		}
		$scope.rmvqty = function(id) {
			var bvalue = id;
			$.ajax({
				type : 'POST',
				url : "user/rmvqty",
				data : {
					product_id : bvalue
				},
				success : function(data) {
					location.reload(true);
					//alert(data);

				}
			});
		}
		$scope.addqty = function(id) {
			var bvalue = id;
			$.ajax({
				type : 'POST',
				url : "user/addqty",
				data : {
					product_id : bvalue
				},
				success : function(data) {
					location.reload(true);
					//alert(data);

				}
			});
		}
	});
</script>
<style>
mark {
	padding: 0px;
	background-color: #f1c40f;
}

.quantity {
	float: left;
	padding: 20px 0;
	width: 100%;
}
</style>

</head>
<body ng-app="myapp" ng-controller="myappcontroller">
	<jsp:include page="_header.jsp" />
	<jsp:include page="_menu.jsp" />

	<fmt:setLocale value="en_US" scope="session" />

	<div class="page-title">Product List</div>
	<img alt="" src="resources/theme1/assets/images/emailler/Logo.png">
	<c:forEach var="prodInfo" items="${product}">
		<div class="product-preview-container">
			<ul>
				<li><img class="product-image"
					src="data:image/png;base64,${prodInfo.img}" /></li>
				<li>product_id: ${prodInfo.product_id}</li>
				<li>Name: ${prodInfo.name}</li>
				<li>Price: Rs.
					${prodInfo.price}&nbsp;&nbsp;Discount=${prodInfo.discount_per}%</li>
				<li>
					<button ng-click="rmvtocart(${prodInfo.product_id})">remove
						to Cart</button>
				</li>
				<li>
					<div class="quantity">
						<input type="button" ng-click="rmvqty(${prodInfo.product_id})"
							value="-" class="minus"> <input type="text"
							name="quantity" value="${prodInfo.quantity}" title="Qty"
							class="qty" size="4"> <input type="button"
							ng-click="addqty(${prodInfo.product_id})" value="+" class="plus">
					</div>

				</li>
			</ul>
		</div>
	</c:forEach>
	</br> total=${gettotal.total}
	</br> discount=${gettotal.discounttotal}
	</br> cgst=${gettotal.cgst}
	</br> sgst=${gettotal.sgst}
	</br> gstfinaltotal=${gettotal.gstfinaltotal}
	</br>
	<button>Add Address ToProcede</button>
	<br />


	<c:if test="${paginationProducts.totalPages > 1}">
		<div class="page-navigator">
			<c:forEach items="${paginationProducts.navigationPages}" var="page">
				<c:if test="${page != -1 }">
					<a href="productList?page=${page}" class="nav-item">${page}</a>
				</c:if>
				<c:if test="${page == -1 }">
					<span class="nav-item"> ... </span>
				</c:if>
			</c:forEach>

		</div>
	</c:if>

	<jsp:include page="_footer.jsp" />

	<a class="has-arrow waves-effect waves-dark"
		href="<c:url value="/logout"/>" aria-expanded="false"> <i
		class="mdi mdi-login-variant"></i> Logout
	</a>



</body>
</html>