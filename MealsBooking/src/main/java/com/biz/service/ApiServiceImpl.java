package com.biz.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biz.dao.ApiDAO;
import com.biz.model.Student;


@Service
@Transactional
public class ApiServiceImpl implements ApiService{
	
	@Autowired
	private ApiDAO apiDAO;

	@Override
	public String postsave(Student student) {
		return apiDAO.postsave(student);
	}

}
