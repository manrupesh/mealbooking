<%@page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>

<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery.dataTables.css">
<style type="text/css">
</style>
<!-- <script
	src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.5/angular.min.js"></script> -->
<script type="text/javascript">
	function filter(element) {
		var value = $(element).val();
		value = value.toLowerCase();
		if (value == "") {
			$(".page.current").trigger("click");
			//$( "" ).trigger( "click" );
			return;
		}

		$("#fromList li").each(function() {
			var curtext = $(this).text();
			curtext = curtext.toLowerCase();
			if (curtext.search(value) > -1) {
				$(this).show();

			} else {
				$(this).hide();
			}
		});
	}

	var app = angular.module('myapp', []);
	app.controller('myappcontroller', function($scope, $http, $compile) {
		$http({
			method : 'POST',
			url : 'getCartCount'
		}).then(function successCallback(response) {
			$scope.size = response.data;
		}, function errorCallback(response) {
			console.log(response.statusText);
		});

		$scope.myVar = 'all';

		$scope.product_id = 0;
		$scope.addtocart = function(id) {
			var bvalue = id;
			$.ajax({
				type : 'POST',
				url : "addtomycart",
				data : {
					product_id : bvalue
				},
				success : function(data) {
					//alert(data)
					if (data == "userdashboard") {
						window.location.href = "login";
					} else {
						if (data != "Already in your cart!") {
							$http({
								method : 'POST',
								url : 'getCartCount'
							}).then(function successCallback(response) {
								$scope.size = response.data;
							}, function errorCallback(response) {
								console.log(response.statusText);
							});
						}
						//alert(data);
					}
				}
			});
		}
		$scope.ratting = function(star, product_id) {
			var product_id = product_id;
			var star = star;
			$.ajax({
				type : 'POST',
				url : "ratting",
				data : {
					product_id : product_id,
					star : star
				},
				success : function(data) {
					if (data == "userdashboard") {
						window.location.href = "login";
					} else {
						if (data != "Already in your cart!") {
							location.reload(true);
						}
						alert(data);
					}

				}
			});
		}

		$scope.favourite = function(product_id) {
			var product_id = product_id;
			$.ajax({
				type : 'POST',
				url : "savefavourite",
				data : {
					product_id : product_id,
				},
				success : function(data) {
					if (data == "userdashboard") {
						window.location.href = "login";
					} else {
						if (data != "Already in your cart!") {
							location.reload(true);
						}
						alert(data);
					}
				}
			});
		}

	});
</script>
<style>
mark {
	padding: 0px;
	background-color: #f1c40f;
}

.checked {
	color: orange;
}
</style>
<meta charset="UTF-8">
<title>Restaurant</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="robots" content="index, follow" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<!--=============== css  ===============-->
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/reset.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/plugins.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/style.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/color.css">
<!--=============== favicons ===============-->
<link rel="shortcut icon" href="resources/restorant/images/favicon.ico">
</head>
<body ng-app="myapp" ng-controller="myappcontroller">
	<div class="loader">
		<img src="resources/restorant/images/loader.png" alt="">
	</div>
	<!--================= main start ================-->
	<div id="main">
		<!--=============== header ===============-->
		<header>
		<div class="header-inner">
			<div class="container">
				<!--navigation social links-->
				<div class="nav-social">
					<ul>
						<li><a href="" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-instagram"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-tumblr"></i></a></li>
					</ul>
				</div>
				<!--logo-->
				<div class="logo-holder">
					<a href="userdashboard"> <img
						src="resources/restorant/images/logo.png" class="logo-vis" alt="">
						<img src="resources/restorant/images/logo1.png"
						class=" logo-notvis" alt="">
					</a>
				</div>
				<!--Navigation -->
				<sec:authorize access="hasAuthority('AUTH_USER')">
					<div class="subnav">
						<a href="showcart"><i class="fa fa-shopping-cart"></i>( <span>{{size}}</span>
							)</a>
					</div>
				</sec:authorize>
				<div class="nav-holder">
					<nav>
					<ul>
						<li><a href="userdashboard" class="act-link">Home</a></li>
						<li><a href="viewmenulist">Menu</a></li>
						<li><a href="viewgallery">Gallery</a></li>
						<!-- <li><a href="viewreservation">Reservation</a></li> -->
						<li><a href="getproductlist">Shop</a></li>
						<li><a href="viewcontact">Contact</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a href="viewmyorders">My Orders</a></li>
						</sec:authorize>
						<li class="hideli"><a href="userRegistration">Sign Up</a></li>
						<li class="hideli"><a href="login">Sign In</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a class="has-arrow waves-effect waves-dark"
								href="<c:url value="/logout"/>" aria-expanded="false"> <i
									class="mdi mdi-login-variant"></i> Logout
							</a></li>
							<script type="text/javascript">
								$(".hideli").hide();
							</script>
						</sec:authorize>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		</header>
		<!--header end-->
		<!--=============== wrapper ===============-->
		<div id="wrapper">
			<!--=============== Hero content ===============-->
			<div class="content full-height hero-content">
				<div class="slideshow-container"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);">
					<!-- slideshow -->
					<div class="slides-container">
						<!-- 1 -->
						<div class="bg"
							style="background-image: url(resources/restorant/images/bg/28.jpg)"></div>
						<!-- 2 -->
						<div class="bg"
							style="background-image: url(resources/restorant/images/bg/1.jpg)"></div>
						<!-- 3 -->
						<div class="bg"
							style="background-image: url(resources/restorant/images/bg/8.jpg)"></div>
					</div>
				</div>
				<div class="hero-title-holder">
					<div class="overlay"></div>
					<div class="hero-title">
						<div class="hero-decor b-dec">
							<div class="half-circle"></div>
						</div>
						<!-- <div class="separator color-separator"></div> -->
						<h3>Welcome to Restaurant</h3>
						<h4>Lorem Ipsum is simply dummy text of the printing and
							typesetting industry.</h4>
					</div>
				</div>

			</div>
			<!--hero end-->
			<div class="content">
				<!--=============== About ===============-->
				<section class="about-section" id="sec1">
				<div class="container">
					<div class="row">
						<!--about text-->
						<div class="col-md-6">
							<div class="section-title">
								<div class="as-heading-wrapper">
									<div class="as-img-behind">
										<img src="resources/restorant/images/bg/about.png" alt=""
											title="">
									</div>
									<div class="as-heading-title">
										<h3 class="as-big-title-heading">ABOUT US</h3>
									</div>
								</div>
								<h4 class="decor-title">Our story</h4>
								<div class="separator color-separator"></div>
							</div>
							<p>The History of Kitchens and Cooks gives further intimation
								on Mr Boulanger usual menu, stating confidently that "Boulanger
								served salted poultry and fresh eggs, all presented without a
								tablecloth on small marble tables". Numerous commentators have
								also referred to the supposed restaurant owner's eccentric habit
								of touting for custom outside his establishment, dressed in
								aristocratic fashion and brandishing a sword</p>
							<p>Numerous commentators have also referred to the supposed
								restaurant owner's eccentric habit of touting for custom outside
								his establishment, dressed in aristocratic fashion and
								brandishing a sword</p>
							<!-- <a href="menu.html" class="link ajax">Discover our menu</a> -->
						</div>
						<!-- about images-->
						<div class="col-md-6">
							<div class="single-slider-holder">
								<div class="customNavigation">
									<a class="next-slide transition"><i
										class="fa fa-angle-right"></i></a> <a
										class="prev-slide transition"><i class="fa fa-angle-left"></i></a>
								</div>
								<div class="single-slider owl-carousel">
									<!-- 1 -->
									<div class="item">
										<img src="resources/restorant/images/bg/15.jpg"
											class="respimg" alt="">
									</div>
									<!-- 2 -->
									<div class="item">
										<img src="resources/restorant/images/bg/14.jpg"
											class="respimg" alt="">
									</div>
									<!-- 3 -->
									<div class="item">
										<img src="resources/restorant/images/bg/13.jpg"
											class="respimg" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</section>
				<!--=============== Opening Hours ===============-->
				<section class="parallax-section">
				<div class="media-container video-parallax"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);">
					<div class="bg mob-bg"
						style="background-image: url(resources/restorant/images/bg/18.jpg)"></div>
					<div class="video-container">
						<img src="resources/restorant/images/bg/opening.jpg"
							class="respimg" alt="">

					</div>
				</div>
				<div class="overlay"></div>
				<div class="container">
					<h2>Opening Hours</h2>
					<h3>Call For Reservations</h3>
					<div class="bold-separator">
						<span></span>
					</div>
					<div class="work-time">
						<div class="row">
							<div class="col-md-6">
								<h3>Sunday to Tuesday</h3>
								<div class="hours">
									09:00<br> 22:00
								</div>
							</div>
							<div class="col-md-6">
								<h3>Friday to Saturday</h3>
								<div class="hours">
									11:00<br> 19:00
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="big-number">
								<a href="">+(91) 9874563210</a>
							</div>
						</div>
					</div>
				</div>
				</section>
				<!--section end-->
				<!--=============== About 2 ===============-->
				<section class="about-section"> <!-- triangle decoration-->
				<div class="triangle-decor"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="single-slider-holder">
								<div class="customNavigation">
									<a class="next-slide transition"><i
										class="fa fa-angle-right"></i></a> <a
										class="prev-slide transition"><i class="fa fa-angle-left"></i></a>
								</div>
								<div class="single-slider owl-carousel">
									<!-- 1 -->
									<div class="item">
										<img src="resources/restorant/images/slider/4.jpg"
											class="respimg" alt="">
									</div>
									<!-- 2 -->
									<div class="item">
										<img src="resources/restorant/images/slider/2.jpg"
											class="respimg" alt="">
									</div>
									<!-- 3 -->
									<div class="item">
										<img src="resources/restorant/images/slider/1.jpg"
											class="respimg" alt="">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="section-title">
								<div class="as-heading-wrapper">
									<div class="as-img-behind">
										<img src="resources/restorant/images/bg/restro.png" alt=""
											title="">
									</div>
									<div class="as-heading-title">
										<h3 class="as-big-title-heading">Our restorans</h3>
									</div>
								</div>
								<h4 class="decor-title">Natoque penatibus</h4>
								<div class="separator color-separator"></div>
							</div>
							<p>The History of Kitchens and Cooks gives further intimation
								on Mr Boulanger usual menu, stating confidently that "Boulanger
								served salted poultry and fresh eggs, all presented without a
								tablecloth on small marble tables". Numerous commentators have
								also referred to the supposed restaurant owner's eccentric habit
								of touting for custom outside his establishment, dressed in
								aristocratic fashion and brandishing a sword</p>
							<p>Restaurant owner's eccentric habit of touting for custom
								outside his establishment, dressed in aristocratic fashion and
								brandishing a sword</p>
							<a href="" class="link">View gallery</a>
						</div>
					</div>
				</div>
				</section>
				<!--section end-->
				<!--=============== Weekly Deals ===============-->
				<section class="parallax-section">
				<div class="bg bg-parallax"
					style="background-image: url(resources/restorant/images/bg/8.jpg)"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);"></div>
				<div class="overlay"></div>
				<div class="container">
					<h2>Weekly Deals</h2>
					<h3>Investigationes demonstraverunt lectores</h3>
					<div class="bold-separator">
						<span></span>
					</div>
					<div class="row">
						<div class="col-md-12">
							<!-- 1 -->
							<div class="promotion-item">
								<div class="promotion-title">
									<h4>Monday</h4>
									<span>( Labore et dolore )</span>
								</div>
								<div class="promotion-details">
									<div class="promotion-desc">Oatmeal Apples</div>
									<div class="promotion-dot"></div>
									<div class="promotion-prices">
										<span class="promotion-price">$34</span><span
											class="promotion-discount">$26</span>
									</div>
								</div>
							</div>
						</div>
						<!-- 2 -->
						<div class="col-md-12">
							<div class="promotion-item">
								<div class="promotion-title">
									<h4>Tuesday</h4>
									<span> ( Semonstraverunt )</span>
								</div>
								<div class="promotion-details">
									<div class="promotion-desc">Soft shell crab</div>
									<div class="promotion-dot"></div>
									<div class="promotion-prices">
										<span class="promotion-price">$130</span><span
											class="promotion-discount">$90</span>
									</div>
								</div>
							</div>
						</div>
						<!-- 3 -->
						<div class="col-md-12">
							<div class="promotion-item">
								<div class="promotion-title">
									<h4>Wednesday</h4>
									<span> ( Incididunt ut labore )</span>
								</div>
								<div class="promotion-details">
									<div class="promotion-desc">Miso chicken</div>
									<div class="promotion-dot"></div>
									<div class="promotion-prices">
										<span class="promotion-price">$30</span><span
											class="promotion-discount">$20</span>
									</div>
								</div>
							</div>
						</div>
						<!-- 4 -->
						<div class="col-md-12">
							<div class="promotion-item">
								<div class="promotion-title">
									<h4>Thursday</h4>
									<span> ( Adipisicing elit )</span>
								</div>
								<div class="promotion-details">
									<div class="promotion-desc">Fish pie</div>
									<div class="promotion-dot"></div>
									<div class="promotion-prices">
										<span class="promotion-price">$61</span><span
											class="promotion-discount">$44</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<a href="" class="align-link link">Online Reservation</a>
				</div>
				</section>
				<!--section end-->
				<!--=============== team ===============-->
				<section>
				<div class="triangle-decor">
					<svg x="0px" y="0px" width="100%" height="15px"> <defs>
					<pattern id="bottom-divider" x="0" y="0" width="15" height="15"
						patternUnits="userSpaceOnUse"> <path fill-rule="evenodd"
						clip-rule="evenodd" fill="#fff"
						d="M7.504-0.008l7.504,7.504L7.504,15L0,7.496L7.504-0.008z"></path></pattern></defs>
					<rect x="0" y="0" width="100%" height="15"
						fill="url(#bottom-divider)"></rect></svg>
				</div>
				<div class="container">
					<div class="section-title">
						<div class="as-heading-wrapper">
							<div class="as-img-behind">
								<img src="resources/restorant/images/bg/shop.png" alt=""
									title="">
							</div>
							<div class="as-heading-title">
								<h3 class="as-big-title-heading">SHOP INFORMATION</h3>
							</div>
						</div>
						<h4 class="decor-title">Our story</h4>
						<div class="separator color-separator"></div>
					</div>
					<div class="inner">
						<p>Numerous commentators have also referred to the supposed
							restaurant owner's eccentric habit of touting for custom outside
							his establishment, dressed in aristocratic fashion and
							brandishing a sword</p>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
					<select name="orderby" class="orderby" required ng-model="myVar">
						<option value="all" selected="selected">All</option>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<option value="favourite">My Favourite</option>
						</sec:authorize>
						<option value="veg">Veg</option>
						<option value="non-veg">Non-Veg</option>
					</select>
					<div class="paginationList" ng-if="myVar === 'all'">
						<!-- <input id="myInput" type="text" placeholder="Search.."> -->
						<input type='text' id='txtList' onkeyup="filter(this)" />
						<ul id="fromList" class="products paginationList">
							<div class="container11">
								<c:forEach var="prodInfo" items="${product}">
									<li class="product-cat-mains listItem single-item"><a
										href="review?product_id=${prodInfo.product_id}"
										ng-click="review(${prodInfo.product_id})"><img
											src="data:image/png;base64,${prodInfo.img}" alt=""
											class="product-image1"></a>
										<h4 class="product-title">
											<a href="review?product_id=${prodInfo.product_id}"
												ng-click="review(${prodInfo.product_id})">${prodInfo.name}</a>
										</h4>
										<ul class="product-cats">
											<li><a href=""
												ng-click="addtocart(${prodInfo.product_id})">Add to cart</a></li>
											<li><a href="review?product_id=${prodInfo.product_id}"
												ng-click="review(${prodInfo.product_id})">view review</a></li>
										</ul>
										<div class="product-price">
											<c:if
												test="${prodInfo.price>(prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100)}">
												<span class="promotion-price">Rs.&nbsp;${prodInfo.price}</span>
											</c:if>
											<span class="promotion-discount">Rs.&nbsp;${prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100}</span>
											<a href="">Rs.&nbsp;${prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100}</a>
										</div></li>
								</c:forEach>

							</div>
							<script src="resources/myJS/pagination.js"></script>
						</ul>
					</div>
					<div class="paginationList" ng-if="myVar === 'favourite'">
						<ul class="products paginationList">
							<div class="container113">
								<c:forEach var="prodInfo" items="${product}">
									<c:if test="${prodInfo.favourit =='true'}">
										<li class="product-cat-mains listItem single-item3"><a
											href="review?product_id=${prodInfo.product_id}"
											ng-click="review(${prodInfo.product_id})"><img
												src="data:image/png;base64,${prodInfo.img}" alt=""
												class="product-image1"></a>
											<h4 class="product-title">
												<a href="review?product_id=${prodInfo.product_id}"
													ng-click="review(${prodInfo.product_id})">${prodInfo.name}</a>
											</h4>
											<ul class="product-cats">
												<li><a href=""
													ng-click="addtocart(${prodInfo.product_id})">Add to
														cart</a></li>
												<li><a href="review?product_id=${prodInfo.product_id}"
													ng-click="review(${prodInfo.product_id})">view review</a></li>
											</ul>
											<div class="product-price">
												<c:if
													test="${prodInfo.price>(prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100)}">
													<span class="promotion-price">Rs.&nbsp;${prodInfo.price}</span>
												</c:if>
												<span class="promotion-discount">Rs.&nbsp;${prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100}</span>
												<a href="">Rs.&nbsp;${prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100}</a>
											</div></li>
									</c:if>
								</c:forEach>
							</div>
							<script src="resources/myJS/fevpagination.js"></script>
						</ul>
					</div>
					<div class="paginationList" ng-if="myVar === 'veg'">
						<ul class="products paginationList">
							<div class="container112">
								<c:forEach var="prodInfo" items="${product}">
									<c:if test="${prodInfo.category == 'veg'}">
										<li class="product-cat-mains listItem single-item2"><a
											href="review?product_id=${prodInfo.product_id}"
											ng-click="review(${prodInfo.product_id})"><img
												src="data:image/png;base64,${prodInfo.img}" alt=""
												class="product-image1"></a>
											<h4 class="product-title">
												<a href="review?product_id=${prodInfo.product_id}"
													ng-click="review(${prodInfo.product_id})">${prodInfo.name}</a>
											</h4>
											<ul class="product-cats">
												<li><a href=""
													ng-click="addtocart(${prodInfo.product_id})">Add to
														cart</a></li>
												<li><a href="review?product_id=${prodInfo.product_id}"
													ng-click="review(${prodInfo.product_id})">view review</a></li>
											</ul>
											<div class="product-price">
												<c:if
													test="${prodInfo.price>(prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100)}">
													<span class="promotion-price">Rs.&nbsp;${prodInfo.price}</span>
												</c:if>
												<span class="promotion-discount">Rs.&nbsp;${prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100}</span>
												<a href="">Rs.&nbsp;${prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100}</a>
											</div></li>
									</c:if>
								</c:forEach>
							</div>
							<script src="resources/myJS/vegpagination.js"></script>
						</ul>
					</div>
					<div class="paginationList" ng-if="myVar === 'non-veg'">
						<ul class="products paginationList">
							<div class="container114">
								<c:forEach var="prodInfo" items="${product}">
									<c:if test="${prodInfo.category == 'non veg'}">
										<li class="product-cat-mains listItem single-item4"><a
											href="review?product_id=${prodInfo.product_id}"
											ng-click="review(${prodInfo.product_id})"><img
												src="data:image/png;base64,${prodInfo.img}" alt=""
												class="product-image1"></a>
											<h4 class="product-title">
												<a href="review?product_id=${prodInfo.product_id}"
													ng-click="review(${prodInfo.product_id})">${prodInfo.name}</a>
											</h4>
											<ul class="product-cats">
												<li><a href=""
													ng-click="addtocart(${prodInfo.product_id})">Add to
														cart</a></li>
												<li><a href="review?product_id=${prodInfo.product_id}"
													ng-click="review(${prodInfo.product_id})">view review</a></li>
											</ul>
											<div class="product-price">
												<c:if
													test="${prodInfo.price>(prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100)}">
													<span class="promotion-price">Rs.&nbsp;${prodInfo.price}</span>
												</c:if>
												<span class="promotion-discount">Rs.&nbsp;${prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100}</span>
												<a href="">Rs.&nbsp;${prodInfo.price-(prodInfo.price*prodInfo.discount_per)/100}</a>
											</div></li>
									</c:if>
								</c:forEach>
							</div>
							<script src="resources/myJS/nonvegpagination.js"></script>
						</ul>
					</div>
					<!--pagination-->
					<!-- <div id="pagination-container">
						<p class='paginacaoCursor' id="beforePagination"><</p>
						<p class='paginacaoCursor' id="afterPagination">></p>
					</div> -->


				</div>
				</section>
				<!--section end-->
				<!--=============== testimonials ===============-->
				<section class="parallax-section">
				<div class="bg bg-parallax"
					style="background-image: url(resources/restorant/images/bg/1.jpg)"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);"></div>
				<div class="overlay"></div>
				<div class="container">
					<h2>Testimonials</h2>
					<h3>What said about us</h3>
					<div class="bold-separator">
						<span></span>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="testimonials-holder">
								<div class="customNavigation">
									<a class="prev-slide transition"><i
										class="fa fa-long-arrow-left"></i></a> <a
										class="next-slide transition"><i
										class="fa fa-long-arrow-right"></i></a>
								</div>
								<div class="testimonials-slider owl-carousel">
									<div class="item">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<p>" Numerous commentators have also referred to the
											supposed restaurant owner's eccentric habit of touting for
											custom outside his establishment, dressed in aristocratic
											fashion and brandishing a sword "</p>
										<h4>
											<a href="" target="_blank">Jone Doe</a>
										</h4>
									</div>
									<div class="item">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star-half"></i></li>
										</ul>
										<p>" Numerous commentators have also referred to the
											supposed restaurant owner's eccentric habit of touting for
											custom outside his establishment, dressed in aristocratic
											fashion and brandishing a sword "</p>
										<h4>
											<a href="" target="_blank">Jone Doe</a>
										</h4>
									</div>
									<div class="item">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<p>" Numerous commentators have also referred to the
											supposed restaurant owner's eccentric habit of touting for
											custom outside his establishment, dressed in aristocratic
											fashion and brandishing a sword "</p>
										<h4>
											<a href="" target="_blank">Jone Doe</a>
										</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="section-icon">
						<i class="fa fa-quote-left"></i>
					</div>
				</div>
				</section>
				<!--section end-->
			</div>
			<!--content end-->
			<!--=============== footer ===============-->
			<footer>
			<div class="footer-inner">
				<div class="container">
					<div class="row">
						<!--tiwtter-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Our twitter</h4>
								<div class="twitter-holder">
									<div class="twitts">
										<div class="twitter-feed">
											<div id="twitter-feed"></div>
										</div>
									</div>
									<div class="customNavigation">
										<a class="prev-slide transition"><i
											class="fa fa-long-arrow-left"></i></a> <a
											class="twit-link transition" href="" target="_blank"><i
											class="fa fa-twitter"></i></a> <a class="next-slide transition"><i
											class="fa fa-long-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<!--footer social links-->
						<div class="col-md-4">
							<div class="footer-social">
								<h3>Find us</h3>
								<ul>
									<li><a href="" target="_blank"><i
											class="fa fa-facebook"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-twitter"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-instagram"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-pinterest"></i></a></li>
									<li><a href="" target="_blank"><i class="fa fa-tumblr"></i></a></li>
								</ul>
							</div>
						</div>
						<!--subscribe form-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Newsletter</h4>
								<div class="subcribe-form">
									<form id="subscribe">
										<input class="enteremail" name="email" id="subscribe-email"
											placeholder="Your email address.." spellcheck="false"
											type="text">
										<button type="submit" id="subscribe-button"
											class="subscribe-button">
											<i class="fa fa-envelope"></i>
										</button>
										<label for="subscribe-email" class="subscribe-message"></label>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
					<!--footer contacts links -->
					<ul class="footer-contacts">
						<li><a href="">+(91) 9874563210</a></li>
						<li><a href="">27th Brooklyn New York, NY 10065</a></li>
						<li><a href="">yourmail@domain.com</a></li>
					</ul>
				</div>
			</div>
			<!--to top / privacy policy-->
			<div class="to-top-holder">
				<div class="container">
					<p>
						<span> &#169; Restaurant 2019 . </span> All rights reserved.
					</p>
					<div class="to-top">
						<span>Back To Top </span><i class="fa fa-angle-double-up"></i>
					</div>
				</div>
			</div>
			</footer>
			<!--footer end -->
		</div>
		<!-- wrapper end -->
	</div>
	<!-- Main end -->
	<!--=============== google map ===============-->
	<script type="text/javascript"
		src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<!--=============== scripts  ===============-->
	<script type="text/javascript"
		src="resources/restorant/js/jquery.min.js"></script>
	<script type="text/javascript" src="resources/restorant/js/plugins.js"></script>
	<script type="text/javascript" src="resources/restorant/js/scripts.js"></script>
</body>

</html>