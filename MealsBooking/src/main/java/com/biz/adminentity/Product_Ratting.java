package com.biz.adminentity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Product_Ratting_list")
public class Product_Ratting {
	
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name = "ratting_id")
	private int ratting_id;
	private int uid;
	private int star;
	private Character status;
	private Character carry;
	private Character deleted;
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	Product product_id = new Product();

	public int getRatting_id() {
		return ratting_id;
	}

	public void setRatting_id(int ratting_id) {
		this.ratting_id = ratting_id;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public int getStar() {
		return star;
	}

	public void setStar(int star) {
		this.star = star;
	}

	public Product getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Product product_id) {
		this.product_id = product_id;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public Character getCarry() {
		return carry;
	}

	public void setCarry(Character carry) {
		this.carry = carry;
	}

	public Character getDeleted() {
		return deleted;
	}

	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}

	
}
