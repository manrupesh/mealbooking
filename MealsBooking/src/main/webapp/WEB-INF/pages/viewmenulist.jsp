<%@page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
<script src="resources/myJS/bootstrap.min.js"></script>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>
<script src="resources/myJS/jquery-ui.js"></script>
<script src="resources/myJS/custom.js"></script>
<script src="resources/myJS/jquery.mark.min.js"></script>
<script src="resources/myJS/jquery.calculation.js"></script>
<script src="resources/myJS/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="resources/mycss/jquery.dataTables.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- <script
	src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.5/angular.min.js"></script> -->
<script type="text/javascript">
	var app = angular.module('myapp', []);
	app.controller('myappcontroller', function($scope, $http, $compile) {
		
		$http({
			method : 'POST',
			url : 'getCartCount'
		}).then(function successCallback(response) {
			$scope.size = response.data;
		}, function errorCallback(response) {
			console.log(response.statusText);
		});

		$scope.myVar = 'all';

		$scope.product_id = 0;
		$scope.addtocart = function(id) {
			var bvalue = id;
			$.ajax({
				type : 'POST',
				url : "addtomycart",
				data : {
					product_id : bvalue
				},
				success : function(data) {
					if (data == "userdashboard") {
						window.location.href = "login";
					} else {
						if (data != "Already in your cart!") {
							$http({
								method : 'POST',
								url : 'getCartCount'
							}).then(function successCallback(response) {
								$scope.size = response.data;
							}, function errorCallback(response) {
								console.log(response.statusText);
							});
						}
						alert(data);
					}
				}
			});
		}
		$scope.ratting = function(star, product_id) {
			var product_id = product_id;
			var star = star;
			$.ajax({
				type : 'POST',
				url : "ratting",
				data : {
					product_id : product_id,
					star : star
				},
				success : function(data) {
					if (data == "userdashboard") {
						window.location.href = "login";
					} else {
						if (data != "Already in your cart!") {
							location.reload(true);
						}
						alert(data);
					}

				}
			});
		}

		$scope.favourite = function(product_id) {
			var product_id = product_id;
			$.ajax({
				type : 'POST',
				url : "savefavourite",
				data : {
					product_id : product_id,
				},
				success : function(data) {
					if (data == "userdashboard") {
						window.location.href = "login";
					} else {
						if (data != "Already in your cart!") {
							location.reload(true);
						}
						alert(data);
					}
				}
			});
		}
	});
</script>
<style>
mark {
	padding: 0px;
	background-color: #f1c40f;
}

.checked {
	color: orange;
}
</style>
<meta charset="UTF-8">
<title>Lambert - Responsive Pub / Restaurant Template</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="robots" content="index, follow" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<!--=============== css  ===============-->
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/reset.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/plugins.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/style.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/color.css">
<!--=============== favicons ===============-->
<link rel="shortcut icon" href="resources/restorant/images/favicon.ico">
</head>
<body ng-app="myapp" ng-controller="myappcontroller">
	<div class="loader">
		<img src="resources/restorant/images/loader.png" alt="">
	</div>
	<!--================= main start ================-->
	<div id="main">
		<!--=============== header ===============-->
		<header>
		<div class="header-inner">
			<div class="container">
				<!--navigation social links-->
				<div class="nav-social">
					<ul>
						<li><a href="" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-instagram"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-tumblr"></i></a></li>
					</ul>
				</div>
				<!--logo-->
				<div class="logo-holder">
					<a href="index.html"> <img
						src="resources/restorant/images/logo.png" class="respimg logo-vis"
						alt=""> <img src="resources/restorant/images/logo2.png"
						class="respimg logo-notvis" alt="">
					</a>
				</div>
				<!--Navigation -->
				<sec:authorize access="hasAuthority('AUTH_USER')">
					<div class="subnav">
						<a href="showcart"><i class="fa fa-shopping-cart"></i>( <span>{{size}}</span>
							)</a>
					</div>
				</sec:authorize>
				<div class="nav-holder">
					<nav>
					<ul>
						<li><a href="userdashboard">Home</a></li>
						<li><a href="viewmenulist" class="act-link">Menu</a></li>
						<li><a href="viewgallery">Gallery</a></li>
						<li><a href="getproductlist">Shop</a></li>
						<li><a href="viewcontact">Contact</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a href="viewmyorders">My Orders</a></li>
						</sec:authorize>
						<li class="hideli"><a href="userRegistration">Sign Up</a></li>
						<li class="hideli"><a href="login">Sign In</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a class="has-arrow waves-effect waves-dark"
								href="<c:url value="/logout"/>" aria-expanded="false"> <i
									class="mdi mdi-login-variant"></i> Logout
							</a></li>
							<script type="text/javascript">
								$(".hideli").hide();
							</script>
						</sec:authorize>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		</header>
		<!--header end-->
		<!--=============== wrapper ===============-->
		<div id="wrapper">
			<div class="content">
				<section class="parallax-section header-section">
				<div class="bg bg-parallax"
					style="background-image: url(resources/restorant/images/bg/14.jpg)"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);"></div>
				<div class="overlay"></div>
				<div class="container">
					<h2>Hot dishes</h2>
					<h3>Pellentesque habitant morbi</h3>
				</div>
				</section>
				<section>
				<div class="triangle-decor"></div>
				<div class="menu-bg lbd"
					style="background-image: url(resources/restorant/images/menu/1.png)"
					data-top-bottom="transform: translateX(200px);"
					data-bottom-top="transform: translateX(-200px);"></div>
				<div class="container">
					<div class="separator color-separator"></div>
					<div class="menu-holder">
						<div class="row">
							<c:forEach var="prodInfo" items="${product}">
								<c:if test="${prodInfo.product_type == 'hotdishes'}">
									<div class="col-md-6">
										<!--menu item-->
										<div class="menu-item">
											<div class="menu-item-details">
												<div class="menu-item-desc">
													<a href="data:image/png;base64,${prodInfo.img}"
														class="image-popup">${prodInfo.name}</a>
												</div>
												<div class="menu-item-dot"></div>
												<div class="menu-item-prices">
													<div class="menu-item-price">Rs. ${prodInfo.price}</div>
												</div>
											</div>
											<p>${prodInfo.description}</p>
										</div>
										<!--menu item end-->
									</div>
								</c:if>
							</c:forEach>
						</div>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
				</div>
				</section>
				<section class="parallax-section">
				<div class="bg bg-parallax"
					style="background-image: url(resources/restorant/images/bg/17.jpg)"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);"></div>
				<div class="overlay"></div>
				<div class="container">
					<h2>Dessert</h2>
					<h3>Aenean ultricies mi vitae est</h3>
					<div class="bold-separator">
						<span></span>
					</div>
				</div>
				</section>
				<section>
				<div class="triangle-decor"></div>
				<div class="menu-bg rbd"
					style="background-image: url(resources/restorant/images/menu/2.png)"
					data-top-bottom="transform: translateX(-200px);"
					data-bottom-top="transform: translateX(200px);"></div>
				<div class="container">
					<div class="separator color-separator"></div>
					<div class="menu-holder">
						<div class="row">
							<c:forEach var="prodInfo" items="${product}">
								<c:if test="${prodInfo.product_type == 'dessert'}">
									<div class="col-md-6">
										<!--menu item-->
										<div class="menu-item">
											<div class="menu-item-details">
												<div class="menu-item-desc">
													<a href="data:image/png;base64,${prodInfo.img}"
														class="image-popup">${prodInfo.name}</a>
												</div>
												<div class="menu-item-dot"></div>
												<div class="menu-item-prices">
													<div class="menu-item-price">Rs. ${prodInfo.price}</div>
												</div>
											</div>
											<p>${prodInfo.description}</p>
										</div>
										<!--menu item end-->
									</div>
								</c:if>
							</c:forEach>
						</div>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
				</div>
				</section>
				<section class="parallax-section">
				<div class="bg bg-parallax"
					style="background-image: url(resources/restorant/images/bg/20.jpg)"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);"></div>
				<div class="overlay"></div>
				<div class="container">
					<h2>Drinks</h2>
					<h3>Donec eu libero</h3>
					<div class="bold-separator">
						<span></span>
					</div>
				</div>
				</section>
				<section>
				<div class="triangle-decor"></div>
				<div class="menu-bg lbd"
					style="background-image: url(resources/restorant/images/menu/3.png)"
					data-top-bottom="transform: translateX(200px);"
					data-bottom-top="transform: translateX(-200px);"></div>
				<div class="container">
					<div class="separator color-separator"></div>
					<div class="menu-holder">
						<div class="row">
							<c:forEach var="prodInfo" items="${product}">
								<c:if test="${prodInfo.product_type == 'drinks'}">
									<div class="col-md-6">
										<!--menu item-->
										<div class="menu-item">
											<div class="menu-item-details">
												<div class="menu-item-desc">
													<a href="data:image/png;base64,${prodInfo.img}"
														class="image-popup">${prodInfo.name}</a>
												</div>
												<div class="menu-item-dot"></div>
												<div class="menu-item-prices">
													<div class="menu-item-price">Rs. ${prodInfo.price}</div>
												</div>
											</div>
											<p>${prodInfo.description}</p>
										</div>
										<!--menu item end-->
									</div>
								</c:if>
							</c:forEach>
						</div>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
				</div>
				</section>
			</div>
			<!--content end-->
			<!--=============== footer ===============-->
			<footer>
			<div class="footer-inner">
				<div class="container">
					<div class="row">
						<!--tiwtter-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Our twitter</h4>
								<div class="twitter-holder">
									<div class="twitts">
										<div class="twitter-feed">
											<div id="twitter-feed"></div>
										</div>
									</div>
									<div class="customNavigation">
										<a class="prev-slide transition"><i
											class="fa fa-long-arrow-left"></i></a> <a
											class="twit-link transition"
											href="https://twitter.com/katokli3mmm" target="_blank"><i
											class="fa fa-twitter"></i></a> <a class="next-slide transition"><i
											class="fa fa-long-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<!--footer social links-->
						<div class="col-md-4">
							<div class="footer-social">
								<h3>Find us</h3>
								<ul>
									<li><a href="" target="_blank"><i
											class="fa fa-facebook"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-twitter"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-instagram"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-pinterest"></i></a></li>
									<li><a href="" target="_blank"><i class="fa fa-tumblr"></i></a></li>
								</ul>
							</div>
						</div>
						<!--subscribe form-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Newsletter</h4>
								<div class="subcribe-form">
									<form id="subscribe">
										<input class="enteremail" name="email" id="subscribe-email"
											placeholder="Your email address.." spellcheck="false"
											type="text">
										<button type="submit" id="subscribe-button"
											class="subscribe-button">
											<i class="fa fa-envelope"></i>
										</button>
										<label for="subscribe-email" class="subscribe-message"></label>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
					<!--footer contacts links -->
					<ul class="footer-contacts">
						<li><a href="">+7(111)123456789</a></li>
						<li><a href="">27th Brooklyn New York, NY 10065</a></li>
						<li><a href="">yourmail@domain.com</a></li>
					</ul>
				</div>
			</div>
			<!--to top / privacy policy-->
			<div class="to-top-holder">
				<div class="container">
					<p>
						<span> &#169; Lambert 2015 . </span> All rights reserved.
					</p>
					<div class="to-top">
						<span>Back To Top </span><i class="fa fa-angle-double-up"></i>
					</div>
				</div>
			</div>
			</footer>
			<!--footer end -->
		</div>
		<!-- wrapper end -->
	</div>
	<!-- Main end -->
	<!--=============== google map ===============-->
	<script type="text/javascript"
		src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<!--=============== scripts  ===============-->
	<script type="text/javascript"
		src="resources/restorant/js/jquery.min.js"></script>
	<script type="text/javascript" src="resources/restorant/js/plugins.js"></script>
	<script type="text/javascript" src="resources/restorant/js/scripts.js"></script>
</body>

<!-- Mirrored from lambert.kwst.net/site/menu.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 01 Feb 2019 10:44:01 GMT -->
</html>