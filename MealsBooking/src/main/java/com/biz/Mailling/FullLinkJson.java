package com.biz.Mailling;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class FullLinkJson {
	
	public static String send(String encryvalue) {
		 String jsonout="";
		try {
			
			URL url = new URL(encryvalue);
			URLConnection conn = url.openConnection();

			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				jsonout+=line;
			}
			rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonout;
	
	}

}
