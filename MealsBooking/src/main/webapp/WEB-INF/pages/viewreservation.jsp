<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>

<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/reset.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/plugins.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/style.css">
<link type="text/css" rel="stylesheet"
	href="resources/restorant/css/color.css">
<!--=============== favicons ===============-->
<link rel="shortcut icon" href="resources/restorant/images/favicon.ico">

<script src="resources/myJS/bootstrap.min.js"></script>
<script src="resources/myJS/jquery-1.8.2.min.js"></script>
<script src="resources/myJS/angular.min.js"></script>
<script src="resources/myJS/jquery-ui.js"></script>
<script src="resources/myJS/custom.js"></script>
<script src="resources/myJS/jquery.mark.min.js"></script>
<script src="resources/myJS/jquery.calculation.js"></script>
<script src="resources/myJS/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	var sheight = window.screen.availHeight - 183;
	var swidth = window.screen.availWidth - 800;

	$(document).ready(function() {

	});
	var app = angular.module('myapp', []);
	app.controller('mycontroller', function($scope, $http, $window) {
	});

	function checkLoginForm(form) {

		usn = document.getElementById("firstName");
		lastName = document.getElementById("lastName");
		email = document.getElementById("email");
		pwd = document.getElementById("password-field1");
		cpwd = document.getElementById("password-field2");
		var terms = document.getElementById("checkbox-signup");

		if (pwd.value != cpwd.value) {
			$("#phone").val(mobnumber);
			alert("Enter Password and conform password is same");
			cpwd.focus();
			return false;
		}
		if (usn.value == "") {
			$("#phone").val(mobnumber);
			alert("Enter First Name");
			usn.focus();
			return false;
		}
		if (lastName.value == "") {
			$("#phone").val(mobnumber);
			alert("Enter Last Name");
			lastName.focus();
			return false;
		}
		if (email.value == "") {
			$("#phone").val(mobnumber);
			alert("Enter Email Name");
			email.focus();
			return false;
		}

		var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
		if (!re.test(pwd.value)) {
			$("#phone").val(mobnumber);
			alert("Password > 8 characters it must contain at least one number (0-9),at least one lowercase letter (a-z),at least one special character, at least one uppercase letter (A-Z)");
			pwd.focus();
			return false;
		}
		if (!terms.checked) {
			$("#phone").val(mobnumber);
			alert('Please signify your agreement with our terms.');
			terms.focus();
			return false;
		}
		var phoneno = /^\d{10}$/;
		if (mobnumber.match(phoneno)) {
			return true;
		} else {
			$("#phone").val(mobnumber);
			alert("check your mob m=number");
			return false;
		}
		return true;
	}
</script>
</head>

<body ng-app="myapp" ng-controller="myappcontroller">

	<div class="loader">
		<img src="resources/restorant/images/loader.png" alt="">
	</div>
	<!--================= main start ================-->
	<div id="main">
		<!--=============== header ===============-->
		<header>
		<div class="header-inner">
			<div class="container">
				<!--navigation social links-->
				<div class="nav-social">
					<ul>
						<li><a href="" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-instagram"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="" target="_blank"><i class="fa fa-tumblr"></i></a></li>
					</ul>
				</div>
				<!--logo-->
				<div class="logo-holder">
					<a href="index.html"> <img
						src="resources/restorant/images/logo.png" class="respimg logo-vis"
						alt=""> <img src="resources/restorant/images/logo2.png"
						class="respimg logo-notvis" alt="">
					</a>
				</div>
				<!--Navigation -->
				<sec:authorize access="hasAuthority('AUTH_USER')">
					<div class="subnav">
						<a href="showcart"><i class="fa fa-shopping-cart"></i>( <span>${cartsize}</span>
							)</a>
					</div>
				</sec:authorize>
				<div class="nav-holder">
					<nav>
					<ul>
						<li><a href="userdashboard">Home</a></li>
						<li><a href="viewmenulist">Menu</a></li>
						<li><a href="viewgallery">Gallery</a></li>
						<li><a href="getproductlist">Shop</a></li>
						<li><a href="viewcontact">Contact</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a href="viewmyorders">My Orders</a></li>
						</sec:authorize>
						<li class="hideli"><a href="userRegistration">Sign Up</a></li>
						<li class="hideli"><a href="login">Sign In</a></li>
						<sec:authorize access="hasAuthority('AUTH_USER')">
							<li><a class="has-arrow waves-effect waves-dark"
								href="<c:url value="/logout"/>" aria-expanded="false"> <i
									class="mdi mdi-login-variant"></i> Logout
							</a></li>
							<script type="text/javascript">
								$(".hideli").hide();
							</script>
						</sec:authorize>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		</header>
		<!--header end-->
		<!--=============== wrapper ===============-->
		<div id="wrapper">
			<div class="content">
				<!--=============== parallax section  ===============-->
				<section class="parallax-section header-section">
				<div class="bg bg-parallax"
					style="background-image: url(resources/restorant/images/bg/33.jpg)"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);"></div>
				<div class="overlay"></div>
				<div class="container">
					<h2>Make online reservation</h2>
					<h3>Booking a table online is easy</h3>
				</div>
				</section>
				<!--section end-->
				<!--=============== reservation ===============-->
				<section>
				<div class="triangle-decor"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">
								<h4>Reervation info</h4>
								<div class="separator color-separator"></div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="inner">
										<p>Numerous commentators have also referred to the
											supposed restaurant owner's eccentric habit of touting for
											custom outside his establishment, dressed in aristocratic
											fashion and brandishing a sword</p>
									</div>
								</div>
							</div>
							<div class="bold-separator">
								<span></span>
							</div>
							<div class="reservation-form-holder">
								<div class="reservation-form">
									<div id="message"></div>

									<font color="red" size="4">${status}</font>
									<form:form class="form-horizontal form-material"
										id="reservation-form" name="signIn"
										action="user/savetablebookdetail" method="post"
										modelAttribute="tablebookdetail"
										onsubmit='return checkLoginForm(this);'>
										<div class="row">
											<div class="col-md-6">
												<h3>Book a table</h3>
												<!--date-->
												<form:input path="bookdate" placeholder="Date"
													class="myInput" id="resdate" type="date" data-lang="en"
													data-years="2015-2016" data-format="YYYY-MM-DD"
													data-sundayfirst="false" />
												<!--time-->
												<form:select path="booktime" id="restime"
													class="form-control">
													<form:option value="5:00am">5:00 am</form:option>
													<form:option value="5:30am">5:30 am</form:option>
													<form:option value="6:00am">6:00 am</form:option>
													<form:option value="6:30am">6:30 am</form:option>
													<form:option selected="selected" value="7:00am">7:00 am</form:option>
													<form:option value="7:30am">7:30 am</form:option>
													<form:option value="8:00am">8:00 am</form:option>
													<form:option value="8:30am">8:30 am</form:option>
													<form:option value="9:00am">9:00 am</form:option>
													<form:option value="9:30am">9:30 am</form:option>
													<form:option value="10:00am">10:00 am</form:option>
													<form:option value="10:30am">10:30 am</form:option>
													<form:option value="11:00am">11:00 am</form:option>
													<form:option value="11:30am">11:30 am</form:option>
													<form:option value="12:00pm">12:00 pm</form:option>
													<form:option value="12:30pm">12:30 pm</form:option>
													<form:option value="1:00pm">1:00 pm</form:option>
													<form:option value="1:30pm">1:30 pm</form:option>
													<form:option value="2:00pm">2:00 pm</form:option>
													<form:option value="2:30pm">2:30 pm</form:option>
													<form:option value="3:00pm">3:00 pm</form:option>
													<form:option value="3:30pm">3:30 pm</form:option>
													<form:option value="4:00pm">4:00 pm</form:option>
													<form:option value="4:30pm">4:30 pm</form:option>
													<form:option value="5:00pm">5:00 pm</form:option>
													<form:option value="5:30pm">5:30 pm</form:option>
													<form:option value="6:00pm">6:00 pm</form:option>
													<form:option value="6:30pm">6:30 pm</form:option>
													<form:option value="7:00pm">7:00 pm</form:option>
													<form:option value="7:30pm">7:30 pm</form:option>
													<form:option value="8:00pm">8:00 pm</form:option>
													<form:option value="8:30pm">8:30 pm</form:option>
													<form:option value="9:00pm">9:00 pm</form:option>
													<form:option value="9:30pm">9:30 pm</form:option>
													<form:option value="10:00pm">10:00 pm</form:option>
													<form:option value="10:30pm">10:30 pm</form:option>
													<form:option value="11:00pm">11:00 pm</form:option>
													<form:option value="11:30pm">11:30 pm</form:option>
												</form:select>
												<form:input path="address" type="text" id="name"
													placeholder="Address" />
												<form:select id="numperson" path="persons"
													class="form-control">
													<form:option value="1">1 Person</form:option>
													<form:option value="2">2 People</form:option>
													<form:option value="3">3 People</form:option>
													<form:option value="4">4 People</form:option>
													<form:option value="5">5 People</form:option>
													<form:option value="6">6 People</form:option>
												</form:select>
											</div>
											<div class="col-md-6">
												<h3>Contact Details</h3>
												<!--name-->
												<form:input path="username" type="text" id="name"
													placeholder="Name" />
												<!--mail-->
												<form:input path="emailID" type="text" id="email"
													placeholder="E-mail" />
												<!--phone-->
												<form:input path="mobno" type="text" id="phone"
													placeholder="Phone" />
												<!--message-->
												<form:textarea path="message" id="comments"
													placeholder="Message"></form:textarea>
											</div>
										</div>
										<button type="submit" id="submit-res">Make a
											reservation</button>
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
				</section>
				<!--section end-->
				<!--=============== Opening Hours ===============-->
				<section class="parallax-section" id="sec2">
				<div class="media-container video-parallax"
					data-top-bottom="transform: translateY(300px);"
					data-bottom-top="transform: translateY(-300px);">
					<div class="bg mob-bg"
						style="background-image: url(resources/restorant/images/bg/22.jpg)"></div>
					<div class="video-container">
						<video autoplay loop muted class="bgvid"> <source
							src="resources/restorant/video/1.mp4" type="video/mp4"></video>
					</div>
				</div>
				<div class="overlay"></div>
				<div class="container">
					<h2>Opening Hours</h2>
					<h3>Call For Reservations</h3>
					<div class="bold-separator">
						<span></span>
					</div>
					<div class="work-time">
						<div class="row">
							<div class="col-md-6">
								<h3>Sunday to Tuesday</h3>
								<div class="hours">
									09:00<br> 22:00
								</div>
							</div>
							<div class="col-md-6">
								<h3>Friday to Saturday</h3>
								<div class="hours">
									11:00<br> 19:00
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="big-number">
								<a href="#">+7(111)123456789</a>
							</div>
						</div>
					</div>
				</div>
				</section>
			</div>
			<!--content end-->
			<!--=============== footer ===============-->
			<footer>
			<div class="footer-inner">
				<div class="container">
					<div class="row">
						<!--tiwtter-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Our twitter</h4>
								<div class="twitter-holder">
									<div class="twitts">
										<div class="twitter-feed">
											<div id="twitter-feed"></div>
										</div>
									</div>
									<div class="customNavigation">
										<a class="prev-slide transition"><i
											class="fa fa-long-arrow-left"></i></a> <a
											class="twit-link transition"
											href="https://twitter.com/katokli3mmm" target="_blank"><i
											class="fa fa-twitter"></i></a> <a class="next-slide transition"><i
											class="fa fa-long-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<!--footer social links-->
						<div class="col-md-4">
							<div class="footer-social">
								<h3>Find us</h3>
								<ul>
									<li><a href="" target="_blank"><i
											class="fa fa-facebook"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-twitter"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-instagram"></i></a></li>
									<li><a href="" target="_blank"><i
											class="fa fa-pinterest"></i></a></li>
									<li><a href="" target="_blank"><i class="fa fa-tumblr"></i></a></li>
								</ul>
							</div>
						</div>
						<!--subscribe form-->
						<div class="col-md-4">
							<div class="footer-info">
								<h4>Newsletter</h4>
								<div class="subcribe-form">
									<form id="subscribe">
										<input class="enteremail" name="email" id="subscribe-email"
											placeholder="Your email address.." spellcheck="false"
											type="text">
										<button type="submit" id="subscribe-button"
											class="subscribe-button">
											<i class="fa fa-envelope"></i>
										</button>
										<label for="subscribe-email" class="subscribe-message"></label>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="bold-separator">
						<span></span>
					</div>
					<!--footer contacts links -->
					<ul class="footer-contacts">
						<li><a href="">+7(111)123456789</a></li>
						<li><a href="">27th Brooklyn New York, NY 10065</a></li>
						<li><a href="">yourmail@domain.com</a></li>
					</ul>
				</div>
			</div>
			<!--to top / privacy policy-->
			<div class="to-top-holder">
				<div class="container">
					<p>
						<span> &#169; Lambert 2015 . </span> All rights reserved.
					</p>
					<div class="to-top">
						<span>Back To Top </span><i class="fa fa-angle-double-up"></i>
					</div>
				</div>
			</div>
			</footer>
			<!--footer end -->
		</div>
		<!-- wrapper end -->
	</div>
	<!-- Main end -->
	<!--=============== google map ===============-->
	<!-- <script type="text/javascript"
		src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
	<!--=============== scripts  ===============-->
	<!-- <script type="text/javascript"
		src="resources/restorant/js/jquery.min.js"></script> -->
	<script type="text/javascript" src="resources/restorant/js/plugins.js"></script>
	<script type="text/javascript" src="resources/restorant/js/scripts.js"></script>
</body>