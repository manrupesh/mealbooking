package com.biz.service;

import java.util.List;

import com.biz.adminentity.Order_Details;
import com.biz.adminentity.Orders;
import com.biz.adminentity.Product;
import com.biz.adminentity.Product_Reviews;
import com.biz.adminentity.Table_Book_Details;
import com.biz.adminmodel.GetProductCartTotal;
import com.biz.adminmodel.Reviews;

public interface UserService {

	public Product findProduct(int product_id);

	public String addtocart(int product_id);

	public List<Product> getcart();

	public String rmvtocart(int product_id);

	public String ratting(int product_id, int star);

	public void updateratting(int product_id);

	public void savereview(Reviews review);

	public List<Product_Reviews> getreviewslist(int product_id);

	public String savefavourite(int product_id);

	public String getfovourite(int product_id);

	public String rmvqty(int product_id);

	public String addqty(int product_id);

	public GetProductCartTotal gettotal();

	public int savestudentdetails(Table_Book_Details tablebookdetail);

	public boolean addorder(Orders orders);

	public List<Orders> getmyorders();

	public List<Order_Details> getmyorderdetails(int order_id);


}
