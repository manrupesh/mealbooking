package com.biz.dao;

import java.util.ArrayList;

import com.biz.adminentity.Admin;
import com.biz.model.ChangePassword;
import com.biz.model.Login;
import com.biz.model.Student;
import com.biz.model.User;

public interface WalletDAO {
	public User addUser(User user);

	public int editpassword(User user);

	public User getUserdata(User user);

	public int passwordchange(ChangePassword changepassword);

	public boolean checkUser(Login login, String uuid);

	public void updateUser(String emailID, String otp);

	public void checkUserActive(String uuid);

	public User getUserdata1(User user);

	//public void updateipdetails(String logs, String emailid);

	
	//  public int saveproduct(Product product);
	 /* 
	 * public int savestudentdetails(Student student);
	 * 
	 * public ArrayList<Student> getstudent();
	 * 
	 * public ArrayList<Product> getproduct();
	 */

	public Admin getUserdata2(Admin user);

	public Admin getUserdata12(Admin user);

	public void updateAdmin(String emailID, String otp);

}
